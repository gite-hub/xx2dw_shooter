#include "unicode32.h"
#include "win32.h"
#include "string_fmt.h"

std::wstring Charset::toUnicode(bool utf8, const char* format, ...) {
	va_list args;
	va_start(args, format);
	auto ret = cc2d::format(format, args);
	va_end(args);
	int length = MultiByteToWideChar(utf8 ? CP_UTF8 : CP_ACP, 0, ret.c_str(), ret.length(), NULL, 0);
	auto wchars = new wchar_t[length + 1];
	MultiByteToWideChar(utf8 ? CP_UTF8 : CP_ACP, 0, ret.c_str(), ret.length(), wchars, length);
	wchars[length] = 0;
	auto wstr = std::wstring(wchars, length);
	delete[] wchars;
	return wstr;
}


wchar_t Charset::toUnicode(const char* single_char, bool utf8) {
    int length = MultiByteToWideChar(utf8 ? CP_UTF8 : CP_ACP, 0, single_char, strlen(single_char), NULL, 0);
    wchar_t wchars[2]; // = new wchar_t[length + 1];
    MultiByteToWideChar(utf8 ? CP_UTF8 : CP_ACP, 0, single_char, strlen(single_char), wchars, length);
    wchars[length] = 0;
    return wchars[0];
}


std::string Charset::toAnsi(bool utf8, const std::wstring& wstr) {
    int length = WideCharToMultiByte(utf8 ? CP_UTF8 : CP_ACP, 0, wstr.c_str(), wstr.length(), NULL, 0, nullptr, nullptr);
    char* chars = new char[length + 1];
    WideCharToMultiByte(utf8 ? CP_UTF8 : CP_ACP, 0, wstr.c_str(), wstr.length(), chars, length, nullptr, nullptr);
    chars[length] = 0;
    auto str = std::string(chars, length);
    delete[] chars;
    return str;
}
