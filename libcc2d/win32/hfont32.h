#pragma once
#include "win32.h"
#include "cxx_stl.h"
#include "cxx_t.h"

class HFontManager {
public:
	using word_t = wchar_t;
	HFontManager();
	~HFontManager();

	void init(HWND hwnd);

	static HFONT__* createFont(int height);

	struct Cache {
		HDC__* dc = nullptr;
		HFONT__* font = nullptr;
	};
	Cache& getCache(int font_size);

	bool toData(int font_size, word_t word, int& width, int& height, uchar* datas = nullptr);

private:
	HDC _hwnd_dc;
	BITMAPINFO* _bitmap_info;
	HBRUSH _brush;
	std::unordered_map<int, Cache> _caches;
};

extern HFontManager g_hfont;
