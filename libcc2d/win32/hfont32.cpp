#include "hfont32.h"

HFontManager g_hfont;


HFontManager::HFontManager() {
	_hwnd_dc = nullptr;
	_bitmap_info = nullptr;
	_brush = nullptr;
}

HFontManager::~HFontManager() {
	if (_bitmap_info != nullptr) {
		free(_bitmap_info);
	}
}


void HFontManager::init(HWND hwnd) {
	_hwnd_dc = GetDC(hwnd);
	_bitmap_info = (BITMAPINFO*)malloc(sizeof(BITMAPINFO) + sizeof(RGBQUAD) * 4);
	_brush = CreateSolidBrush(RGB(0xFF, 0xFF, 0xFF));
}


HFONT HFontManager::createFont(int height) {
	return CreateFont(
		height, 0,           // 高宽 正值指定字符单元格的高度；负值指定字符的基线到字符顶部的高度
		0, 0,                // 显示的角度
		FW_THIN,           // 字体的磅数  700粗体 
		FALSE, FALSE, FALSE, // 斜体 下划线 删除线
        GB2312_CHARSET,      // 字符集
		OUT_CHARACTER_PRECIS,// 输出精度
        CLIP_DEFAULT_PRECIS, // 裁剪精度 CLIP_STROKE_PRECIS
		DEFAULT_QUALITY,     // 逻辑字体与输出设备的实际字体之间的精度 CLEARTYPE_QUALITY ANTIALIASED_QUALITY
        FIXED_PITCH | MONO_FONT | FF_MODERN,           // 字体间距
		L"");
}



HFontManager::Cache& HFontManager::getCache(int font_size) {
	const auto& it = _caches.find(font_size);
	if (it != _caches.end()) {
		return it->second;
	}
	auto& cache = _caches[font_size];
    //  (HFONT)GetStockObject(DEFAULT_GUI_FONT);
    cache.font = createFont(font_size);

	cache.dc = CreateCompatibleDC(_hwnd_dc);
	SelectObject(cache.dc, cache.font);

	auto _pen = CreatePen(PS_SOLID, 1, 0xFFFFFF);
	auto _oldpen = SelectObject(cache.dc, _pen);


	// SetMapMode(cache.dc, MM_TEXT);
    // 文本混合模式
    // SetBkMode(cache.dc, TRANSPARENT);
	SetTextColor(cache.dc, 0xFFFFFF);
	SetBkColor(cache.dc, 0x000000);

	return cache;
}


bool HFontManager::toData(int font_size, word_t word, int& width, int& height, uchar* datas) {

	auto dc = getCache(font_size).dc;

	word_t words[2] = { word, L'\0' };

	RECT rect;
	rect.left = 0;
	rect.top = 0;
	{
		SIZE size;
		GetTextExtentPoint32(dc, words, 1, &size);
		rect.right = size.cx;
		rect.bottom = size.cy;
	}
	DrawText(dc, words, -1, &rect, DT_CALCRECT | DT_LEFT | DT_TOP);// DT_WORDBREAK| DT_EDITCONTROL);
	width = rect.right;
	height = rect.bottom;
	if (datas == nullptr) {
		return false;
	}
	HBITMAP hbitmap = CreateCompatibleBitmap(_hwnd_dc, width, height);
	HBITMAP old_hbmp = (HBITMAP)SelectObject(dc, hbitmap);
#if 1
	memset(_bitmap_info, 0, sizeof(BITMAPINFO));
 	_bitmap_info->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	GetDIBits(dc, hbitmap, 0, 0, nullptr, _bitmap_info, DIB_RGB_COLORS);

	FillRect(dc, &rect, _brush);
	// 这里再画的时候不能DT_CALCRECT了
	DrawText(dc, words, -1, &rect, DT_LEFT | DT_TOP);

    // int bit = _bitmap_info->bmiHeader.biBitCount / 8;
	static uint* bmps = new uint[128 * 128];
	// auto bmps = new uchar[_bitmap_info->bmiHeader.biSizeImage];
	memset(bmps, 0, _bitmap_info->bmiHeader.biSizeImage);
	GetDIBits(dc, hbitmap, 0, _bitmap_info->bmiHeader.biHeight, bmps, _bitmap_info, DIB_RGB_COLORS);

	width = rect.right;
	height = rect.bottom;
	int pitch = width; // cc2d::pitch<uchar>(width);
	// uchar* datas = new uchar[pitch * height];
	memset(datas, 0, pitch * height * sizeof(*datas));

    uint temp;
	for (int i = 0, j, ih = height - 1, iw = _bitmap_info->bmiHeader.biWidth; i < height; i += 1, ih -= 1) {
		for (j = 0; j < width; ++j) {
            temp = bmps[ih * iw + j];
			if (temp/* >= 0x007FFFFF*/) {
				datas[i * pitch + j] = temp * 255.0f / 0xFFFFFF;
			}
		}
	}
	// delete[] bmps;

#else
	int pitch = cc2d::pitch<uchar>(width);
	int size = pitch * height;
	_bitmap_info->bmiHeader.biBitCount = 8;
	_bitmap_info->bmiHeader.biWidth = pitch;
	_bitmap_info->bmiHeader.biHeight = -height;
	_bitmap_info->bmiHeader.biSizeImage = pitch * height;

	auto datas = new uchar[size];
	memset(datas, 0, size);

	rect.right = pitch;
	FillRect(dc, &rect, _brush);
	DrawText(dc, words, 1, &rect, DT_LEFT | DT_TOP);
	GetDIBits(dc, hbitmap, 0, 0, nullptr, _bitmap_info, DIB_RGB_COLORS);
	GetDIBits(dc, hbitmap, 0, height, datas, _bitmap_info, DIB_RGB_COLORS);
#endif

	// SelectObject(g_DC2, oldFont);
	SelectObject(dc, old_hbmp);
	DeleteObject(hbitmap);
	return true;
}
