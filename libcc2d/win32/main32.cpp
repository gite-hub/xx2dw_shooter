#include "win32.h"
#include "main.h"
#include "glad_inc.h"
#include "extern2d.h"
#include "input.h"
#include "charset.h"
#include "hfont32.h"
#include "iofile.h"
#include "logging.h"

#ifdef CC_USE_EGL
#pragma comment(lib, "3rd/libEGL.dll.lib")
#pragma comment(lib, "3rd/libGLESv2.dll.lib")
#else
#if defined(_DEBUG)
#pragma comment(lib, "3rd/glfw3_mt")
// #pragma comment(lib, "opencv_core4100_mtd")
// #pragma comment(lib, "opencv_imgproc4100_mtd")
// #pragma comment(lib, "ittnotify_mtd")
#else
#pragma comment(lib, "3rd/glfw3")
// #pragma comment(lib, "opencv_core4100_mt")
// #pragma comment(lib, "opencv_imgproc4100_mt")
#endif
#endif

#pragma comment(lib, "3rd/bass/win32/bass")
#pragma comment(lib, "3rd/turbojpeg-static")


#ifdef CC_USE_EGL
static EGLDisplay display = nullptr;
static EGLSurface surface = nullptr;
static EGLContext context = nullptr;
#else
GLFWwindow* hwindow = nullptr;
#endif


static LRESULT CALLBACK  WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    switch (uMsg) {
    case WM_CLOSE:
        // 		if (g_pMainState->m_bInit) {
        // 			g_uiClose.OnOff(!g_uiClose.m_NeedShow);
        // 			return 1;
        // 		}
    case WM_PAINT:
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;
    case WM_LBUTTONDOWN: // 按键精灵也经过这里
        // ccc_box("按下");
        break;
    }
    return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

static void resizeWindow(HWND hwnd, int width, int height) {

    RECT wind_rect, client_rect;
    GetWindowRect(hwnd, &wind_rect);
    GetClientRect(hwnd, &client_rect);

    width = (wind_rect.right - (client_rect.right - width)) - wind_rect.left;
    height = (wind_rect.bottom - (client_rect.bottom - height)) - wind_rect.top;

    MoveWindow(hwnd, wind_rect.left, wind_rect.top, width, height, true);
}

int WINAPI WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nShowCmd) {
#ifdef CC_USE_EGL 

    WNDCLASSEX wclass;
    wclass.style = CS_DBLCLKS | CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
    wclass.cbClsExtra = 0;
    wclass.cbSize = sizeof(wclass);
    wclass.cbWndExtra = 0;
    wclass.hbrBackground = 0;
    wclass.hCursor = LoadCursor(0, IDC_ARROW);
    wclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(101/*IDI_ICON1*/));
    wclass.hIconSm = wclass.hIcon;
    wclass.lpszClassName = L"cc2d";
    wclass.hInstance = hInstance;
    wclass.lpfnWndProc = WindowProc;
    wclass.lpszMenuName = 0;
    wclass.style = CS_CLASSDC;
    //注册窗口
    if (!RegisterClassEx(&wclass)) {
        return false;
    }
    int width = 960;
    int height = 600;
    long style = WS_BORDER | WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_VISIBLE;
    auto hwnd = CreateWindow(wclass.lpszClassName, L"标题", style,
        (GetSystemMetrics(SM_CXSCREEN) - width) / 2, (GetSystemMetrics(SM_CYSCREEN) - height) / 2, width, height,
        nullptr, nullptr, hInstance, nullptr);

    if (hwnd == nullptr) {
        return false;
    }

    resizeWindow(hwnd, width, height);
    ShowWindow(hwnd, SW_SHOW);
    UpdateWindow(hwnd);
    CoInitialize(nullptr);


    display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    EGLint major;
    EGLint minor;
    eglInitialize(display, &major, &minor);
    eglBindAPI(EGL_OPENGL_ES_API);
#if 0
    EGLint attribs[] = {
        EGL_RED_SIZE, 8,
        EGL_GREEN_SIZE, 8,
        EGL_BLUE_SIZE, 8,
        // EGL_ALPHA_SIZE, 8,
        EGL_DEPTH_SIZE, 24,
        // EGL_STENCIL_SIZE, 8,
        EGL_RENDERABLE_TYPE, EGL_WINDOW_BIT | EGL_OPENGL_ES2_BIT,
        EGL_SURFACE_TYPE, EGL_WINDOW_BIT | EGL_OPENGL_ES2_BIT,
        EGL_NONE
    };
    EGLConfig config;
    EGLint numConfigs(0);
    eglChooseConfig(display, attribs, &config, 1, &numConfigs);

    EGLint format(0);
    eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format);
#endif

    EGLConfig* eglConfigs;
    int eglNumConfigs;
    eglGetConfigs(display, NULL, 0, &eglNumConfigs);

#if 0
    eglConfigs = new EGLConfig[eglNumConfigs];

    for (int i = 0; i < eglNumConfigs; i++) {
        CC_LOG("Config %d", i); // << i << "\n";
        CC_LOG("Supported APIs");
        int eglRenderable;
        eglGetConfigAttrib(display, eglConfigs[i], EGL_RENDERABLE_TYPE, &eglRenderable);
        if (eglRenderable & EGL_OPENGL_ES_BIT)  CC_LOG("OPENGL ES");// << "\n";
        if (eglRenderable & EGL_OPENGL_ES2_BIT)  CC_LOG("OPENGL ES2");//  << "\n";
        if (eglRenderable & EGL_OPENVG_BIT)  CC_LOG("OPENVG");//  << "\n";
        if (eglRenderable & EGL_OPENGL_BIT)  CC_LOG("OPENGL");//  << "\n";
    }
#endif
    EGLint attr[] = {
        EGL_BLUE_SIZE, 8,
        EGL_GREEN_SIZE, 8,
        EGL_RED_SIZE, 8,
        EGL_DEPTH_SIZE, 24,
        EGL_RENDERABLE_TYPE, EGL_WINDOW_BIT | EGL_OPENGL_ES2_BIT,
        EGL_SURFACE_TYPE, EGL_WINDOW_BIT | EGL_OPENGL_ES2_BIT,
        EGL_NONE
    };

    EGLConfig* eglConfig = new EGLConfig;
    int elgNumConfig;
    if (!eglChooseConfig(display, attr, eglConfig, sizeof(eglConfig), &eglNumConfigs)) {
        CC_LOG("Could not get valid egl configuration!");// << endl;
        return false;
    }

    EGLConfig config = *eglConfig;

    surface = eglCreateWindowSurface(display, config, hwnd, NULL);
    if (surface == EGL_NO_SURFACE) {
        // http://docs.neomades.com/en/current/GenericAPI/reference/javax/microedition/khronos/egl/EGL10.html
        // public static final int EGL_BAD_CONFIG
        // EGL error code indicating 'bad config'.
        // Constant Value : 12293 (0x00003005)
        CC_LOG("Could not create EGL surface : %d", eglGetError());
        return false;
}

    EGLint contextAttributes[] = {
        EGL_CONTEXT_CLIENT_VERSION, 2,
        EGL_NONE, EGL_NONE,
    };
    context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttributes);
    if (context == EGL_NO_CONTEXT) {
        CC_LOG("Could not create egl context : %d", eglGetError());
        return false;
    }

    CC_LOG("EGL Version = %s", eglQueryString(display, EGL_VERSION));
    CC_LOG("EGL Vendor = %s", eglQueryString(display, EGL_VENDOR));
    CC_LOG("EGL Client APIs : %s", eglQueryString(display, EGL_CLIENT_APIS));
    CC_LOG("EGL Extensions :");
    std::stringstream sstr;
    sstr << eglQueryString(display, EGL_EXTENSIONS);
    std::string str;
    while (sstr >> str) {
        CC_LOG("%s", str.c_str());
    }



    if (eglMakeCurrent(display, surface, surface, context) == EGL_FALSE) {
        return false;
    }

    int egl_width, egl_height;
    eglQuerySurface(display, surface, EGL_WIDTH, &egl_width);
    eglQuerySurface(display, surface, EGL_HEIGHT, &egl_height);

    g_hfont.init(hwnd);

    IFile::setAssetManager(nullptr, "assets/");

    if (!cc2d::mainInit(width, height)) {
        return 0;
    }


    LARGE_INTEGER large;
    QueryPerformanceFrequency(&large);
    LONGLONG freq = large.QuadPart;
    double ifreq = 1.0 / freq;

    QueryPerformanceCounter(&large);
    LONGLONG counter = large.QuadPart, temp;

    MSG msg;
    ZeroMemory(&msg, sizeof(MSG));
    while (true) {
        if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
            if (msg.message == WM_QUIT) {
                break;
            }
            TranslateMessage(&msg);
            DispatchMessage(&msg);
            continue;
        }
        QueryPerformanceCounter(&large);
        temp = large.QuadPart - counter;
        if (temp >= freq / 60) {
            counter = large.QuadPart;
            // glfwPollEvents();
            glClearColor(0.1f, 0.0f, 0.0f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT/* | GL_DEPTH_BUFFER_BIT*/);
            
            cc2d::mainLoop(temp * ifreq);

//             // 绘制三角形
//             static const GLfloat vertices[] = {
//                 0.0f,  0.5f, 0.0f,
//                -0.5f, -0.5f, 0.0f,
//                 0.5f, -0.5f, 0.0f,
//             };
//             static const GLfloat colors[] = {
//                 1.0f, 0.0f, 0.0f, 1.0f,
//                 0.0f, 1.0f, 0.0f, 1.0f,
//                 0.0f, 0.0f, 1.0f, 1.0f,
//             };
// 
//             glEnableVertexAttribArray(0);
//             glEnableVertexAttribArray(1);
//             glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, vertices);
//             glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, colors);
// 
//             glDrawArrays(GL_TRIANGLES, 0, 3);

            eglSwapBuffers(display, surface);
        } else {
            Sleep(temp * 1000 / freq);
        }
    }
    eglDestroyContext(display, context);
    eglDestroySurface(display, surface);
    eglTerminate(display);

    DestroyWindow(hwnd);
#else
    if (!glfwInit()) {
        return 0;
    }

    glfwDefaultWindowHints();
    glfwWindowHint(GLFW_DEPTH_BITS, 0);
    glfwWindowHint(GLFW_STENCIL_BITS, 0);

    int width = 1280;
    int height = 800;

    // std::string utf8_str = (char*)u8"Open Graphics Library 开放式图形库";
    // std::string bom_str = "Open Graphics Library 开放式图形库";

    hwindow = glfwCreateWindow(width, height, "Open Graphics Library 开放式图形库", nullptr, nullptr);
    if (!hwindow) {
        return 0;
    }
    glfwMakeContextCurrent(hwindow);
    // GLFW_LOCK_KEY_MODS
    glfwSetInputMode(hwindow, GLFW_CURSOR_NORMAL, GLFW_TRUE);
    // 	while (auto e = glGetError()) {
    // 		CC_LOG_ERROR("glGetError() == %d", (int)e);
    // 	};
    if (!gladLoadGL((GLADloadfunc)glfwGetProcAddress)) {
        return 0;
    }

    // no v-sync by default
    glfwSwapInterval(0);

    glEnable(GL_PRIMITIVE_RESTART);
    glPrimitiveRestartIndex(65535);
    glPointSize(5);
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

    auto hwnd = glfwGetWin32Window(hwindow);
    g_hfont.init(hwnd);

    IFile::setAssetManager(nullptr, "assets/");

    if (!cc2d::mainInit(width, height)) {
        return 0;
    }

    // 设置自定义窗口过程函数
    // SetWindowLongPtr(hwnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(WindowProc));
    glfwSetMouseButtonCallback(hwindow, [](GLFWwindow* /*window*/, int button, int action, int) {
        if (button == GLFW_MOUSE_BUTTON_LEFT || button == GLFW_MOUSE_BUTTON_RIGHT) {
            static std::unordered_map<int, INPUT_ACTION_TYPE> input_table = {
                { GLFW_PRESS, INPUT_PRESS },
                { GLFW_RELEASE, INPUT_RELEASE },
                { GLFW_REPEAT, INPUT_REPEAT },
            };
            const auto& it = input_table.find(action);
            if (it != input_table.end()) {
                g_input.acquire(button == GLFW_MOUSE_BUTTON_LEFT ? MOUSE_LEFT : MOUSE_RIGHT, it->second);
            }
        }
    });
    glfwSetCursorPosCallback(hwindow, [](GLFWwindow*, double x, double y) {
        g_input.acquire(x, y);
        });
    //	glfwSetScrollCallback(hwnidow, GLFWEventHandler::onGLFWMouseScrollCallback);
    glfwSetCharCallback(hwindow, [](GLFWwindow*, unsigned int charCode) {
        g_input.acquire(charCode);
        });
    glfwSetKeyCallback(hwindow, [](GLFWwindow* /*window*/, int key, int /*scancode*/, int action, int /*mods*/) {
        const auto& table = cc2d::getKeyCodeTable();
        const auto& it = table.find(key);
        if (it != table.end()) {
            static std::unordered_map<int, INPUT_ACTION_TYPE> input_table = {
                { GLFW_PRESS, INPUT_PRESS },
                { GLFW_RELEASE, INPUT_RELEASE },
                { GLFW_REPEAT, INPUT_REPEAT },
            };
            const auto& it2 = input_table.find(action);
            if (it2 != input_table.end()) {
                g_input.acquire(it->second, it2->second);
            }
        }
    });
    // 	glfwSetWindowPosCallback(hwnidow, GLFWEventHandler::onGLFWWindowPosCallback);
    // 	glfwSetWindowSizeCallback(hwnidow, GLFWEventHandler::onGLFWWindowSizeCallback);
    // 	glfwSetWindowIconifyCallback(hwnidow, GLFWEventHandler::onGLFWWindowIconifyCallback);
    // 	glfwSetWindowFocusCallback(hwnidow, GLFWEventHandler::onGLFWWindowFocusCallback);
    glfwSetWindowCloseCallback(hwindow, [](GLFWwindow*) {
        // glfwDestroyWindow(hwindow);
    });

    LARGE_INTEGER nLast;
    LARGE_INTEGER nNow;
    QueryPerformanceCounter(&nLast);

    LONGLONG interval = 0LL;
    LONG waitMS = 0L;

    LARGE_INTEGER freq;
    QueryPerformanceFrequency(&freq);

    LARGE_INTEGER _animationInterval;
    _animationInterval.QuadPart = (LONGLONG)(freq.QuadPart / 60);

    // MSG msg;
    // ZeroMemory(&msg, sizeof(MSG));
    while (!glfwWindowShouldClose(hwindow)) {
#if 0
        if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
            if (msg.message == WM_QUIT) {
                break;
            }
            TranslateMessage(&msg);
            DispatchMessage(&msg);
            continue;
}
#endif
        QueryPerformanceCounter(&nNow);
        interval = nNow.QuadPart - nLast.QuadPart;
        if (interval >= _animationInterval.QuadPart) {
            nLast.QuadPart = nNow.QuadPart;
            glfwPollEvents();
            // CC_LOG("gl_PollEvents");
            glClear(GL_COLOR_BUFFER_BIT);
            cc2d::mainLoop(interval * 0.0000001f);
            glfwSwapBuffers(hwindow);
            // CC_LOG("gl_SwapBuffers");
        } else {
            waitMS = static_cast<LONG>((_animationInterval.QuadPart - interval) * 1000LL / freq.QuadPart - 1L);
            if (waitMS > 1L) {
                Sleep(waitMS);
            }
        }
    }
#endif
    return 0;
}
