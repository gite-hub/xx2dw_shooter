#pragma once
#include <xstring>

namespace Charset {
	std::wstring toUnicode(bool utf8, const char* format, ...);
    wchar_t toUnicode(const char* single_char, bool utf8);
    std::string toAnsi(bool utf8, const std::wstring& wstr);
};
