#pragma once
#include "cxx_stl.h"
#include "gl_inc.h"

enum VERTEX_SHADER_TYPE {
	VERTEX_SHADER_DEFAULT,
	VERTEX_SHADER_COUNT,
};


// D3DCAPS9 caps;
// d3dDevice->GetDeviceCaps(&caps);
// caps.MaxSimultaneousTextures 表示支持的最大纹理单元数量

// GLint maxTextureUnits;
// glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &maxTextureUnits);

class GLManager {
public:
	GLManager();
	void initGL();
	static gluid_t loadShader(uint/*GLenum*/ type, const char* content);
	void initShaders();
	void initPrograms();
	void initUniformAndAttri();
	void initVAO();
    void initFBO();
    void initFBOVAO();

	void init(int viewport_x, int viewport_y, int viewport_width, int viewport_height);

	bool linkShader(SHADER_TYPE shader_type);
	bool activeTexture0(gluid_t texture);
	bool activeTexture1(gluid_t texture);
    bool activeTexture2(gluid_t texture);

    bool bindTexture(gluid_t texture, bool palette);

	void render(SHADER_TYPE shader_type, const VertexInstance& instance, gluid_t texture);
    void render(const VertexInstance& instance, int width, int height, gluid_t texture, gluid_t texture_palette, gluid_t texture_alpha);
	void render();

private:
	std::array<gluid_t, VERTEX_SHADER_COUNT> _vertex_shaders;
	std::array<gluid_t, SHADER_COUNT> _fragment_shaders;

	gluid_t _program;

	gluid_t _uniform_texture;
    gluid_t _uniform_texture_palette;
    gluid_t _uniform_texture_alpha;

	gluid_t _attri_xyuv0;
	gluid_t _attri_xyuv1;
	gluid_t _attri_xyuv2;
	gluid_t _attri_xyuv3;
	gluid_t _attri_rgba;

	gluid_t _vao;
	gluid_t _vbo;

    gluid_t _fbo;
    gluid_t _fbo_texture;

    // 没用到
    gluid_t _fbo_vao;
    gluid_t _fbo_vbo;
    gluid_t _fbo_attri_xy;
    gluid_t _fbo_attri_uv;


	VERTEX_SHADER_TYPE _vertex_shader_type;
	SHADER_TYPE _fragment_shader_type;
	SHADER_TYPE _shader_type;

	gluid_t _binding_texture;
    gluid_t _binding_texture_palette;

	std::array <VertexInstance, 16384> _baths;
	int _bath_count;
public:
	static const gluid_t kInvalid;
};
extern GLManager gl_manager;


