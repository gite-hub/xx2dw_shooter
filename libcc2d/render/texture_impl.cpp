#include "texture_impl.h"
#include "glad_inc.h"
#include "extern2d.h"
#include "gl_manager.h"

void TextureImpl::toFormat(PIXEL_BIT pixel_bit, InnerFormat& inner_fmt, Format& fmt, Mask& mask) {
	inner_fmt = GL_RGBA8;
	fmt = GL_RGBA;
	mask = GL_UNSIGNED_BYTE;
	switch (pixel_bit) {
	case PIXEL_8:
		inner_fmt = GL_ALPHA;
		fmt = GL_ALPHA;
		break;
	case PIXEL_16:
		// GL_RG8UIGL_RG_INTEGER 搞不了
		// GL_LUMINANCE_ALPHA; 采样模糊
		inner_fmt = GL_RGBA4;
		fmt = GL_RGBA;
		mask = GL_UNSIGNED_SHORT_4_4_4_4;
		break;
	case PIXEL_24:
		inner_fmt = GL_RGB8;
		fmt = GL_RGB;
		break;
	case PIXEL_32:
	default:
		break;
	}
}


ITexture* TextureImpl::generate(int count) {
	ITexture* texture = new ITexture[count];
	glGenTextures(count, texture);
	return texture;
}


ITexture TextureImpl::generate() {
	ITexture texture;
	glGenTextures(1, &texture);
	return texture;
}


void TextureImpl::load(ITexture texture, PIXEL_BIT pixel_bit, int pitched_width, int height, const void* datas) {
	GLint inner_fmt;
	GLenum fmt;
	GLenum mask;
	toFormat(pixel_bit, inner_fmt, fmt, mask);
    gl_manager.bindTexture(texture, false);
	glTexImage2D(GL_TEXTURE_2D, 0, inner_fmt, pitched_width, height, 0, fmt, mask, datas);
	// 去掉的话 字体A8全1 R8全0
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    // GL_LINEAR_MIPMAP_LINEAR 三线性过滤
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, g_integer_scale ? GL_NEAREST : GL_LINEAR_MIPMAP_LINEAR);
	// 不设置的话，图块之间有缝
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
#if 0
	// 一维纹理安卓闪退
	CC_LOG("GL_TEXTURE_1D %d", texture);
	glBindTexture(GL_TEXTURE_1D, texture);
	CC_LOG("glBindTexture");
	// internalformat 指定纹理中颜色分量的数量。 
	// 必须为 1、2、3 或 4 或以下符号常量之一：
	// GL_ALPHA、GL_ALPHA4、GL_ALPHA8、GL_ALPHA12、GL_ALPHA16、
	// GL_LUMINANCE、GL_LUMINANCE4、GL_LUMINANCE8、GL_LUMINANCE12、GL_LUMINANCE16、
	// GL_LUMINANCE_ALPHA、GL_LUMINANCE4_ALPHA4、GL_LUMINANCE6_ALPHA2、GL_LUMINANCE8_ALPHA8、
	// GL_LUMINANCE12_ALPHA4、GL_LUMINANCE12_ALPHA12、GL_LUMINANCE16_ALPHA16、
	// GL_INTENSITY、GL_INTENSITY4、GL_INTENSITY8、GL_INTENSITY12、GL_INTENSITY16、
	// GL_RGB、GL_R3_G3_B2、GL_RGB4、GL_RGB5、GL_RGB8、GL_RGB10、GL_RGB12、GL_RGB16、
	// GL_RGBA、GL_RGBA2、GL_RGBA4、GL_RGB5_A1、GL_RGBA8、GL_RGB10_A2、GL_RGBA12 或 GL_RGBA16
	// border 边框的宽度。 必须为 0 或 1。
	glTexImage1D(GL_TEXTURE_1D, 0, inner_fmt, width, 0, fmt, GL_UNSIGNED_BYTE, datas);
	CC_LOG("glTexImage1D  %d  %d", width, height);
	glGenerateMipmap(GL_TEXTURE_1D);
#endif
}


ITexture TextureImpl::create(PIXEL_BIT pixel_bit, int pitched_width, int height, const void* datas) {
	auto texture = generate();
	load(texture, pixel_bit, pitched_width, height, datas);
	return texture;
}


void TextureImpl::free(ITexture texture) {
	glDeleteTextures(1, &texture);
}

void TextureImpl::free(int count, ITexture* texture) {
	glDeleteTextures(count, texture);
	delete[] texture;
}

void TextureImpl::update(ITexture texture, PIXEL_BIT pixel_bit, int x, int y, int width, int height, const void* pitched_datas) {
	GLint inner_fmt;
	GLenum fmt;
	GLenum mask;
	toFormat(pixel_bit, inner_fmt, fmt, mask);
    gl_manager.bindTexture(texture, false);
	glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, width, height, fmt, mask, pitched_datas);
}


static VertexInstance instance;
void TextureImpl::render(ITexture texture, SHADER_TYPE shader_type, color_t color, 
	float world_x, float world_y, int texture_width, int texture_height, 
	int rect_x, int rect_y, int rect_width, int rect_height, 
	float render_width, float render_height) {

	cc2d::toXYUV(world_x, world_y, texture_width, texture_height,
		rect_x, rect_y, rect_width, rect_height,
		render_width, render_height, instance.xyuv);
	instance.rgba = COLOR::to32(color);
	gl_manager.render(shader_type, instance, texture);
}


void TextureImpl::render(ITexture texture, SHADER_TYPE shader_type, color_t color, 
	float world_x, float world_y, int texture_width, int texture_height, float render_width, float render_height) {

	cc2d::toXYUV(world_x, world_y, texture_width, texture_height,
		render_width, render_height, instance.xyuv);
	instance.rgba = COLOR::to32(color);
	gl_manager.render(shader_type, instance, texture);
}
