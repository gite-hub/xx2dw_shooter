#pragma once
#include "gl_xyuv.h"

// GLuint;
using gluid_t = unsigned int;
static constexpr int kGLquad4 = 4;
static constexpr int kGLinvalid = -1;

enum SHADER_TYPE {
	SHADER_DEFAULT,
	SHADER_COLOR_7F,
	SHADER_A8,
	SHADER_COUNT,
};


struct VertexInstance {
    VertexXYUV xyuv[kGLquad4];
    rgb32_t rgba;
};
