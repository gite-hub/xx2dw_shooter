#include "gl_manager.h"
#include "glad_inc.h"
#include "logging.h"
#include "extern2d.h"

GLManager gl_manager;
const gluid_t GLManager::kInvalid = kGLinvalid;
//////////////////////////////////////////////////////////////////////////
GLManager::GLManager() {
    _vertex_shaders.fill(kInvalid);
    _fragment_shaders.fill(kInvalid);

    _program = kInvalid;
    _uniform_texture = kInvalid;
    _uniform_texture_palette = kInvalid;
    _uniform_texture_alpha = kInvalid;

    _attri_xyuv0 = kInvalid;
    _attri_xyuv1 = kInvalid;
    _attri_xyuv2 = kInvalid;
    _attri_xyuv3 = kInvalid;
    _attri_rgba = kInvalid;

    _vao = kInvalid;
    _vbo = kInvalid;

    _fbo = kInvalid;
    _fbo_texture = kInvalid;
    _fbo_vao = kInvalid;
    _fbo_vbo = kInvalid;
    _fbo_attri_xy = kInvalid;
    _fbo_attri_uv = kInvalid;

    _vertex_shader_type = VERTEX_SHADER_COUNT;
	_fragment_shader_type = SHADER_COUNT;
	_shader_type = SHADER_COUNT;

    _binding_texture = kInvalid;
    _binding_texture_palette = kInvalid;

    _bath_count = 0;


}

void GLManager::initGL() {

    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    // glEnable(GL_DEPTH_TEST);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBlendEquation(GL_FUNC_ADD);

    // glEnable(GL_TEXTURE_1D);
}


gluid_t GLManager::loadShader(GLenum type, const char* content) {
    auto shader = glCreateShader(type);
    glShaderSource(shader, 1, &content, nullptr);
    glCompileShader(shader);
    return shader;
}

void GLManager::initShaders() {
    // vec2(_rect.x + _0_1.x * _rect.z, _rect.y + _rect.w - _0_1.y * _rect.w) / vec2(textureSize(_texture, 0))
    auto content = R"(
#version 320 es
layout(location = 0) in vec4 _xyuv0;
layout(location = 1) in vec4 _xyuv1;
layout(location = 2) in vec4 _xyuv2;
layout(location = 3) in vec4 _xyuv3;
layout(location = 4) in vec4 _rgba;
out vec2 texture_coord;
out vec4 _color;
void main() {
    vec4 xyuv[4] = vec4[4](_xyuv0, _xyuv1, _xyuv2, _xyuv3);
    _color = xyuv[gl_VertexID];
    gl_Position = vec4(_color.xy, 0, 1);
    texture_coord = _color.zw;
    _color = _rgba;
})";
    auto shader = loadShader(GL_VERTEX_SHADER, content);
    _vertex_shaders[VERTEX_SHADER_DEFAULT] = shader;
    CC_LOG("gl_load_vertex_shader_default %d", shader);

    // 浮点数精度相关: highp mediump 必须写不然android不显示
    // texture(_texture, texture_coord / vec2(textureSize(_texture, 0)));
    // vec4( (c.r + 0.00001f) * vColorplus, (c.g + 0.00001f) * vColorplus, (c.b + 0.00001f) * vColorplus, c.a );
    content = R"(
#version 320 es
precision mediump float;
layout(location = 0) uniform sampler2D _texture;
in vec2 texture_coord;
in vec4 _color;
out vec4 out_color;
void main() {
    vec4 c = texture(_texture, texture_coord);
    out_color = _color * c;
})";

    shader = loadShader(GL_FRAGMENT_SHADER, content);
    _fragment_shaders[SHADER_DEFAULT] = shader;
    CC_LOG("gl_load_fragment_shader_default %d", shader);

    content = R"(
#version 320 es
precision mediump float;
layout(location = 0) uniform sampler2D _texture;
layout(location = 1) uniform sampler2D _texture_palette;
layout(location = 2) uniform sampler2D _texture_alpha;
in vec2 texture_coord;
in vec4 _color;
out vec4 out_color;
void main() {
    vec4 ci = texture(_texture, texture_coord);
    vec4 c = texture(_texture_palette, vec2(ci.a, 0));
    vec4 ca = texture(_texture_alpha, texture_coord);
    out_color = vec4(c.r + (_color.r - 0.5), c.g + (_color.g - 0.5), c.b + (_color.b - 0.5), ca.a * _color.a);
})";

    shader = loadShader(GL_FRAGMENT_SHADER, content);
    _fragment_shaders[SHADER_COLOR_7F] = shader;
    CC_LOG("gl_load_fragment_shader_dye %d", shader);

    content = R"(
#version 320 es
precision mediump float;
layout(location = 0) uniform sampler2D _texture;
in vec2 texture_coord;
in vec4 _color;
out vec4 out_color;
void main() {
    vec4 c = texture(_texture, texture_coord);
    out_color = vec4(_color.r, _color.g, _color.b, c.a);
})";

    shader = loadShader(GL_FRAGMENT_SHADER, content);
    _fragment_shaders[SHADER_A8] = shader;
    CC_LOG("gl_load_fragment_shader_A8 %d", shader);
}


void GLManager::initPrograms() {
	_program = glCreateProgram();
	glAttachShader(_program, _vertex_shaders[VERTEX_SHADER_DEFAULT]);
	CC_LOG("glCreateProgram %d", _program);
#if 0
    auto program = new GLProgram(SHADER_DEFAULT);
    program->init(_vertex_shaders[VERTEX_SHADER_DEFAULT], _fragment_shaders[SHADER_DEFAULT]);
    _programs[SHADER_DEFAULT] = program;

	program = new GLProgram(SHADER_DYE);
	program->init(_vertex_shaders[VERTEX_SHADER_DEFAULT], _fragment_shaders[SHADER_DYE]);
	_programs[SHADER_DYE] = program;

	program = new GLProgram(SHADER_A8);
	program->init(_vertex_shaders[VERTEX_SHADER_DEFAULT], _fragment_shaders[SHADER_A8]);
	_programs[SHADER_A8] = program;
#endif
}


void GLManager::initUniformAndAttri() {
#
	_uniform_texture = 0; // glGetUniformLocation(_program, "_texture");
    _uniform_texture_palette = 1;
    _uniform_texture_alpha = 2;
	// CC_LOG("gl_GetUniformLocation %d %d", _uniform_texture, _uniform_texture_dye);
	// 	少写一个逗号,找半天
// 	if (auto e = glGetError(); e != GL_NO_ERROR) {
// 		CC_LOG("glGetError() == %d\n", e);
// 	}
	_attri_xyuv0 = 0; // glGetAttribLocation(_program, "_xyuv0");
	_attri_xyuv1 = 1; // glGetAttribLocation(_program, "_xyuv1");
	_attri_xyuv2 = 2; // glGetAttribLocation(_program, "_xyuv2");
	_attri_xyuv3 = 3; // glGetAttribLocation(_program, "_xyuv3");
	_attri_rgba = 4; // glGetAttribLocation(_program, "_rgba");
	// 	CC_LOG("gl GetAttribLocation xyuv4? %d %d %d %d %d", _attri_xyuv0, _attri_xyuv1, _attri_xyuv2, _attri_xyuv3, _attri_rgba);
	if (auto e = glGetError(); e != GL_NO_ERROR) {
		CC_LOG("glGetError() == %d\n", e);
	}
    _fbo_attri_xy = 0;
    _fbo_attri_uv = 1;
}

void GLManager::initVAO() {
	glGenVertexArrays(1, &_vao);
	// 绑定/选择 VAO
	glBindVertexArray(_vao);
	CC_LOG("glBindVertexArray %d", _vao);
	glGenBuffers(1, &_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, _vbo);

	gluid_t attri_xyuv[] = { _attri_xyuv0, _attri_xyuv1, _attri_xyuv2, _attri_xyuv3 };
	for (int i = 0; i < kGLquad4; i++) {
		glVertexAttribPointer(attri_xyuv[i], 4, GL_FLOAT, GL_FALSE, sizeof(VertexInstance), (void*)(offsetof(VertexInstance, xyuv) + i * sizeof(VertexXYUV)));
		glVertexAttribDivisor(attri_xyuv[i], 1);
		glEnableVertexAttribArray(attri_xyuv[i]);
	}
	CC_LOG("gl_vbo_xyuv4");

	glVertexAttribPointer(_attri_rgba, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexInstance), (GLvoid*)offsetof(VertexInstance, rgba));
	glVertexAttribDivisor(_attri_rgba, 1);
	glEnableVertexAttribArray(_attri_rgba);
	CC_LOG("gl_vbo_rgba");

	// 先绑定VAO，然后绑定VBO和EBO，再链接顶点属性指针即可。
	// 解绑顺序是：VBO(注意要先链接顶点属性指针)、VAO、EBO
	// glBindBuffer(GL_ARRAY_BUFFER, (GLint)0);
	// glBindVertexArray(0);
}


void GLManager::initFBO() {
    glGenFramebuffers(1, &_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, _fbo);

    glGenTextures(1, &_fbo_texture);
    glBindTexture(GL_TEXTURE_2D, _fbo_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 32, 32, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, g_integer_scale ? GL_NEAREST : GL_LINEAR_MIPMAP_LINEAR);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _fbo_texture, 0);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        CC_LOG("FBO is not complete!");
    }
}

void GLManager::initFBOVAO() {
    glGenVertexArrays(1, &_fbo_vao);
    // 绑定/选择 VAO
    glBindVertexArray(_fbo_vao);

    glGenBuffers(1, &_fbo_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, _fbo_vbo);

    float e = 0.5f;
    static const GLfloat xy4[] = {
        -e, -e,
        -e, e,
        e, e,
        e, -e,
    };
    glVertexAttribPointer(_fbo_attri_xy, 2, GL_FLOAT, GL_FALSE, 0, xy4);
    glEnableVertexAttribArray(_fbo_attri_xy);

    static const GLfloat uv4[] = {
        0, 0,
        0, 1,
        1, 1,
        1, 0
    };
    glVertexAttribPointer(_fbo_attri_uv, 2, GL_FLOAT, GL_FALSE, 0, uv4);
    glEnableVertexAttribArray(_fbo_attri_uv);

    glBindBuffer(GL_ARRAY_BUFFER, (GLint)0);
    glBindVertexArray(0);
}


void GLManager::init(int viewport_x, int viewport_y, int viewport_width, int viewport_height) {
	initGL();
	initShaders();
	initPrograms();
	initUniformAndAttri();
	initVAO();
	glViewport(viewport_x, viewport_y, viewport_width, viewport_height);
}


bool GLManager::linkShader(SHADER_TYPE shader_type) {

	VERTEX_SHADER_TYPE vertex_shader_type = VERTEX_SHADER_DEFAULT;
	SHADER_TYPE fragment_shader_type = SHADER_DEFAULT;
	switch (shader_type) {
	case SHADER_COLOR_7F:
		fragment_shader_type = SHADER_COLOR_7F;
		break;
	case SHADER_A8:
		fragment_shader_type = SHADER_A8;
		break;
	case SHADER_COUNT:
	case SHADER_DEFAULT:
	default:
		break;
	}

	bool changed = false;
	if (_vertex_shader_type != vertex_shader_type) {
		changed = true;
		if (_vertex_shader_type != VERTEX_SHADER_COUNT) {
			glDetachShader(_program, _vertex_shaders[_vertex_shader_type]);
		}
		glAttachShader(_program, _vertex_shaders[vertex_shader_type]);
		_vertex_shader_type = vertex_shader_type;
	}
	if (_fragment_shader_type != fragment_shader_type) {
		changed = true;
		if (_fragment_shader_type != SHADER_COUNT) {
			glDetachShader(_program, _fragment_shaders[_fragment_shader_type]);
		}
		glAttachShader(_program, _fragment_shaders[fragment_shader_type]);
        if (auto e = glGetError(); e != GL_NO_ERROR) {
            // CC_LOG("glGetError() == %d\n", e);
        }
		_fragment_shader_type = fragment_shader_type;
	}
	if (changed) {
		// Vec2 忘了联合体
		// RGBA 倒反天罡
		// linkShader();
		// initUniformAndAttri();
		// initVAO();
		// 
		// 不解绑就一直连体(不行，合批会崩溃)
        // glBindVertexArray(0);
		// glBindVertexArray(_vao);
		// glBindBuffer(GL_ARRAY_BUFFER, vbo);
		// 要重新映射数据
		glLinkProgram(_program);
		glUseProgram(_program);
	}
	// CC_LOG("gl_linkShader %d %d %d", _program, vertex_shader, fragment_shader);
	return changed;
}

bool GLManager::activeTexture0(gluid_t texture) {
	glActiveTexture(GL_TEXTURE0);
    if (bindTexture(texture, false)) {
        glUniform1i(_uniform_texture, 0);
        return true;
    }
	return false;
}

bool GLManager::activeTexture1(gluid_t texture) {
    glActiveTexture(GL_TEXTURE1);
    if (bindTexture(texture, true)) {
        glUniform1i(_uniform_texture_palette, 0);
        return true;
    }
    return false;
}

bool GLManager::activeTexture2(gluid_t texture) {
    glActiveTexture(GL_TEXTURE2);
    if (bindTexture(texture, false)) {
        glUniform1i(_uniform_texture_alpha, 0);
        return true;
    }
    return false;
}

bool GLManager::bindTexture(gluid_t texture, bool palette) {
    if (texture != _binding_texture) {
        glBindTexture(GL_TEXTURE_2D, texture);
        _binding_texture = texture;
        if (palette) {
            _binding_texture_palette = texture;
        }
        return true;
    }
    return false;
}


void GLManager::render(SHADER_TYPE shader_type, const VertexInstance& instance, gluid_t texture) {
    
    if (texture != _binding_texture || _shader_type != shader_type || _bath_count == _baths.size() - 1) {
        render();
    }
	linkShader(shader_type);
	_shader_type = shader_type;
	activeTexture0(texture);
	memcpy(&_baths[_bath_count], &instance, sizeof(instance));
	_bath_count += 1;
}


void GLManager::render(const VertexInstance& instance, int width, int height, gluid_t texture, gluid_t texture_palette, gluid_t texture_alpha) {
    if (texture != _binding_texture || texture_palette != _binding_texture_palette || _shader_type != SHADER_COLOR_7F || _bath_count == _baths.size() - 1) {
        render();
    }
    linkShader(SHADER_COLOR_7F);
    _shader_type = SHADER_COLOR_7F;

    glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
    glViewport(0, 0, width, height);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    render();
}


void GLManager::render() {
	if (_bath_count == 0 || _binding_texture == kInvalid) {
		return;
	}
	glBufferData(GL_ARRAY_BUFFER, sizeof(_baths[0]) * _bath_count, &_baths[0], GL_STATIC_DRAW);

	// 当vertex buffer中的顶点会被多个图元频繁使用时，DrawElements比DrawArrays更为高效
	glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, kGLquad4, _bath_count);

	// GLuint indexes[] = {0, 1, 2, 3};
	// 或者
	// glDrawElementsInstanced(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_INT, indexes, 4);
	// 或者
	// glDrawElements的最后一个论点可以是两件不同的事情：
	// 如果没有绑定到 GL_ELEMENT_ARRAY_BUFFER 的索引缓冲区，则它是指向存储索引的位置的指针。
	// 如果有绑定到 GL_ELEMENT_ARRAY_BUFFER 的索引缓冲区，则该数组是一个以字节为单位的偏移量()。
	// glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_INT, indexes);

	// 通过一个额外的参数GLint baseInstance指定按照baseInstace作为偏移从buffer中取出instance属性的数据
	// glDrawArraysBaseInstance

	// 扩展DrawElements，当根据indices作为索引从vertex buffer中读取数据时，有时希望允许一定数目顶点的偏移
	// 例如当vertex buffer中存着动画的多帧，需要按照一定的偏移取每一帧的数据进行渲染。
	// 该偏移通过一个额外的参数GLint basevertex 指定
	// glDrawElementsBaseVertex(mode, count, type, indicesPtr, baseVertex)

    // _drawing_program = nullptr;
	// _binding_texture = GLManager::kInvalid;
	// _binding_texture_dye = GLManager::kInvalid;
	_bath_count = 0;
}

