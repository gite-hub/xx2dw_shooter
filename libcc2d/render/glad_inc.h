#pragma once
#include "platform.h"

#if defined(CC_ANDROID)
#define CC_USE_EGL
#else
#endif

#if defined(CC_USE_EGL)
#include <EGL/egl.h>
#include <GLES3/gl3.h>
#else
#define GLAD_GLES2
#include "glad/gl.h"
#define GLFW_INCLUDE_ES3
#include "glfw/glfw3.h"
#define GLFW_EXPOSE_NATIVE_WIN32
#include "GLFW/glfw3native.h"
#endif
