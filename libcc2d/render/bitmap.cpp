#include "bitmap.h"
#include "cxx_func.h"
#include "texture_impl.h"

Bitmap::Bitmap() {
	_texture = kInvalidTexture;
}

Bitmap::~Bitmap() {
	if (isValid()) {
		TextureImpl::free(_texture);
	}
}

bool Bitmap::isValid() const {
	return _texture != kInvalidTexture;
}

bool Bitmap::load(PIXEL_BIT bitmap_type, int width, int height, const void* pitch_datas) {
	int pitch = cc2d::pitch(width, cc2d::getByte(bitmap_type));
	if (isValid()) {
		TextureImpl::load(_texture, bitmap_type, pitch, height, pitch_datas);
	} else {
		_texture = TextureImpl::create(bitmap_type, pitch, height, pitch_datas);
	}
	_bitmap_type = bitmap_type;
	this->width = width;
	this->height = height;
	return true;
}

bool Bitmap::update(int x, int y, int width, int height, const void* datas) {
	TextureImpl::update(_texture, _bitmap_type, x, y, width, height, datas);
	return true;
}


ITexture Bitmap::getTexture() {
	return _texture;
}

void Bitmap::render(float world_x, float world_y, color_t color) const {
	TextureImpl::render(_texture, SHADER_DEFAULT, color, world_x, world_y, width, height, width, height);
}
