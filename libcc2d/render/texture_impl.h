#pragma once
#include "texture.h"
#include "cxx_enum.h"
#include "colorgb.h"

class TextureImpl {
public:
	// GLint inner_fmt = GL_RGBA8;
	using InnerFormat = int;
	// GLenum fmt = GL_RGBA;
	using Format = unsigned int;
	// GLenum type = GL_UNSIGNED_BYTE;
	using Mask = unsigned int;
	static void toFormat(PIXEL_BIT pixel_bit, InnerFormat& inner_fmt, Format& fmt, Mask& mask);

	static ITexture* generate(int count);
	static ITexture generate();

	static void load(ITexture texture, PIXEL_BIT pixel_bit, int pitched_width, int height, const void* datas);

	static ITexture create(PIXEL_BIT pixel_bit, int pitched_width, int height, const void* datas);

	static void free(ITexture texture);
	static void free(int count, ITexture* texture);

	static void update(ITexture texture, PIXEL_BIT pixel_bit, int x, int y, int width, int height, const void* pitched_datas);

	static void render(ITexture texture, SHADER_TYPE shader_type, color_t color,
		float world_x, float world_y, int texture_width, int texture_height,
		int rect_x, int rect_y, int rect_width, int rect_height,
		float render_width, float render_height);

	static void render(ITexture texture, SHADER_TYPE shader_type, color_t color,
		float world_x, float world_y, int texture_width, int texture_height,
		float render_width, float render_height);
};
