#pragma once
#include "texture.h"
#include "cxx_enum.h"
#include "colorgb.h"

class Bitmap {
public:
	Bitmap();

	~Bitmap();

	bool isValid() const;

	bool load(PIXEL_BIT bitmap_type, int width, int height, const void* pitch_datas);

	bool update(int x, int y, int width, int height, const void* datas);

	ITexture getTexture();

	void render(float world_x, float world_y, color_t color = COLOR::WHITE) const;
	
private:
	ITexture _texture;
	PIXEL_BIT _bitmap_type;
public:
	int width, height;
};
