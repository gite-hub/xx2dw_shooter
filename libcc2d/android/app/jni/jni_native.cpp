/****************************************************************************
****************************************************************************/
// #include "base/Director.h"
// #include "base/EventKeyboard.h"
// #include "base/EventDispatcher.h"
// #include "platform/android/GLViewImpl-android.h"

#include "jni_native.h"
#include "jni_helper.h"

#include <android/log.h>
#include "android/asset_manager_jni.h"
#include <string>

#include "main.h"
#include "utf16.h"
#include "glad_inc.h"
#include "iofile.h"
#include "input.h"


// USING_NS_AX;


// static std::string g_apkPath;

#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, "native", __VA_ARGS__)

void cc_android_app_init(JNIEnv* env) __attribute__((weak));

static const char* className = "org.cc2d.lib.ccEngine";

static EditTextCallback s_editTextCallback = nullptr;
static void* s_ctx                         = nullptr;

#define TG3_GRAVITY_EARTH (9.80665f)
bool gl_loaded = false;
bool gl_initd = false;


//const char* getApkPath()
//{
//    if (g_apkPath.empty())
//    {
//        g_apkPath = JniHelper::callStaticStringMethod(className, "getAssetsPath");
//    }
//
//    return g_apkPath.c_str();
//}


std::string getPackageNameJNI()
{
    return JniHelper::callStaticStringMethod(className, "getCocos2dxPackageName");
}

int getObbAssetFileDescriptorJNI(const char* path, int64_t* startOffset, int64_t* size)
{
    JniMethodInfo methodInfo;
    int fd = 0;

    if (JniHelper::getStaticMethodInfo(methodInfo, className, "getObbAssetFileDescriptor", "(Ljava/lang/String;)[J"))
    {
        jstring stringArg = methodInfo.env->NewStringUTF(path);
        jlongArray newArray =
            (jlongArray)methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID, stringArg);
        jsize theArrayLen = methodInfo.env->GetArrayLength(newArray);

        if (theArrayLen == 3)
        {
            jboolean copy = JNI_FALSE;
            jlong* array  = methodInfo.env->GetLongArrayElements(newArray, &copy);
            fd            = static_cast<int>(array[0]);
            *startOffset  = array[1];
            *size         = array[2];
            methodInfo.env->ReleaseLongArrayElements(newArray, array, 0);
        }

        methodInfo.env->DeleteLocalRef(methodInfo.classID);
        methodInfo.env->DeleteLocalRef(stringArg);
    }

    return fd;
}

void conversionEncodingJNI(const char* src, int byteSize, const char* fromCharset, char* dst, const char* newCharset)
{
    JniMethodInfo methodInfo;

    if (JniHelper::getStaticMethodInfo(methodInfo, className, "conversionEncoding",
                                       "([BLjava/lang/String;Ljava/lang/String;)[B"))
    {
        jbyteArray strArray = methodInfo.env->NewByteArray(byteSize);
        methodInfo.env->SetByteArrayRegion(strArray, 0, byteSize, reinterpret_cast<const jbyte*>(src));

        jstring stringArg1 = methodInfo.env->NewStringUTF(fromCharset);
        jstring stringArg2 = methodInfo.env->NewStringUTF(newCharset);

        jbyteArray newArray = (jbyteArray)methodInfo.env->CallStaticObjectMethod(
            methodInfo.classID, methodInfo.methodID, strArray, stringArg1, stringArg2);
        jsize theArrayLen = methodInfo.env->GetArrayLength(newArray);
        methodInfo.env->GetByteArrayRegion(newArray, 0, theArrayLen, (jbyte*)dst);

        methodInfo.env->DeleteLocalRef(strArray);
        methodInfo.env->DeleteLocalRef(stringArg1);
        methodInfo.env->DeleteLocalRef(stringArg2);
        methodInfo.env->DeleteLocalRef(newArray);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
}


extern "C" {


#if __ANDROID_API__ > 19
#    include <signal.h>
#    include <dlfcn.h>
typedef __sighandler_t (*bsd_signal_func_t)(int, __sighandler_t);
bsd_signal_func_t bsd_signal_func = NULL;

__sighandler_t bsd_signal(int s, __sighandler_t f) {
    if (bsd_signal_func == NULL) {
        // For now (up to Android 7.0) this is always available
        bsd_signal_func = (bsd_signal_func_t) dlsym(RTLD_DEFAULT, "bsd_signal");

        if (bsd_signal_func == NULL) {
            __android_log_assert("", "bsd_signal_wrapper", "bsd_signal symbol not found!");
        }
    }
    return bsd_signal_func(s, f);
}
#endif  // __ANDROID_API__ > 19

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *vm, void *reserved) {
    LOGD("JniHelper::setJavaVM");
    JniHelper::setJavaVM(vm);
    cc_android_app_init(JniHelper::getEnv());

    return JNI_VERSION_1_4;
}


JNIEXPORT jintArray JNICALL Java_org_cc2d_lib_ccActivity_nativeGetGLContextAttrs(JNIEnv *env, jclass) {
    LOGD("Activity::getGLContextAttrs");
    // Application::getInstance()->initGLContextAttrs();
    // GLContextAttrs _glContextAttrs = GLView::getGLContextAttrs();

    // int tmp[7] = {_glContextAttrs.redBits,           _glContextAttrs.greenBits, _glContextAttrs.blueBits,
    //               _glContextAttrs.alphaBits,         _glContextAttrs.depthBits, _glContextAttrs.stencilBits,
    //               _glContextAttrs.multisamplingCount};

    int tmp[7] = { 8, 8, 8, 8, 24, 8, 0 };
    jintArray glContextAttrsJava = env->NewIntArray(7);
    env->SetIntArrayRegion(glContextAttrsJava, 0, 7, tmp);

    return glContextAttrsJava;
}

JNIEXPORT void JNICALL
Java_org_cc2d_lib_ccRenderer_nativeOnSurfaceChanged(JNIEnv *, jclass, jint w, jint h) {
    // Application::getInstance()->applicationScreenSizeChanged(w, h);
}


JNIEXPORT void JNICALL Java_org_cc2d_lib_ccEngine_nativeSetContext(JNIEnv *env,
                                                                    jclass,
                                                                    jobject context,
                                                                    jobject assetManager) {
    LOGD("Engine::SetContext");
    JniHelper::setClassLoaderFrom(context);
    // FileUtilsAndroid::setassetmanager(AAssetManager_fromJava(env, assetManager));
    auto assets = JniHelper::callStaticStringMethod(className, "getWritablePath");
    assets += '/';
    auto manager = AAssetManager_fromJava(env, assetManager);
    IFile::setAssetManager(manager, assets);
}


JNIEXPORT void JNICALL Java_org_cc2d_lib_ccEngine_nativeSetEditTextDialogResult(JNIEnv *env,
                                                                                 jclass,
                                                                                 jbyteArray text) {
    jsize size = env->GetArrayLength(text);

    if (size > 0) {
        jbyte *data = (jbyte *) env->GetByteArrayElements(text, 0);
        char *buffer = (char *) malloc(size + 1);
        if (buffer != nullptr) {
            memcpy(buffer, data, size);
            buffer[size] = '\0';
            // pass data to edittext's delegate
            if (s_editTextCallback)
                s_editTextCallback(buffer, s_ctx);
            free(buffer);
        }
        env->ReleaseByteArrayElements(text, data, 0);
    } else {
        if (s_editTextCallback)
            s_editTextCallback("", s_ctx);
    }
}


JNIEXPORT void JNICALL
Java_org_cc2d_lib_ccEngine_nativeCall0(JNIEnv *env, jclass, jlong op, jlong param) {
    // auto operation = reinterpret_cast<AsyncOperation>(static_cast<uintptr_t>(op));
    // if (operation)
    //     operation(reinterpret_cast<void*>(static_cast<uintptr_t>(param)));
}


JNIEXPORT void JNICALL
Java_org_cc2d_lib_ccEngine_nativeRunOnGLThread(JNIEnv *env, jclass, jobject runnable) {
    using jobject_type = std::remove_pointer_t<jobject>;
    struct jobject_delete {
        void operator()(jobject_type *__ptr) const _NOEXCEPT {
            JniHelper::getEnv()->DeleteGlobalRef(__ptr);
        }
    };

    // Director::getInstance()->getScheduler()->runOnAxmolThread(
    //     [wrap = std::make_shared<std::unique_ptr<jobject_type, jobject_delete>>(env->NewGlobalRef(runnable))] {
    //         auto curEnv = JniHelper::getEnv();

    //         JniMethodInfo mi;
    //         if (JniHelper::getMethodInfo(mi, "java/lang/Runnable", "run", "()V"))
    //         {
    //             curEnv->CallVoidMethod(wrap.get()->get(), mi.methodID);
    //         }
    //     });
}



JNIEXPORT void JNICALL Java_org_cc2d_lib_ccRenderer_nativeInit(JNIEnv *, jclass, jint w, jint h) {
    LOGD("ccRenderer::Init");
    //  && gladLoaderLoadGLES2()
    if (!gl_loaded){

        gl_loaded = true;
    }
    if (gl_loaded && !gl_initd &&  cc2d::mainInit(w, h)){
        gl_initd = true;
    }
//    cc2d::mainInit(w, h);
//     GLViewImpl::loadGLES2();

//     auto director = Director::getInstance();
//     auto glView   = director->getGLView();
//     if (!glView)
//     {
//         glView = GLViewImpl::create("Android app");
//         glView->setFrameSize(w, h);
//         director->setGLView(glView);

//         Application::getInstance()->run();
//     }
//     else
//     {
//         backend::DriverBase::getInstance()->resetState();
//         Director::getInstance()->resetMatrixStack();
//         EventCustom recreatedEvent(EVENT_RENDERER_RECREATED);
//         director->getEventDispatcher()->dispatchEvent(&recreatedEvent);
//         director->setGLDefaultValues();
// #if AX_ENABLE_CACHE_TEXTURE_DATA
//         VolatileTextureMgr::reloadAllTextures();
// #endif
//     }
}

JNIEXPORT void JNICALL Java_org_cc2d_lib_ccRenderer_nativeOnContextLost(JNIEnv *, jclass) {
    LOGD("Render::OnContextLost");
// #if AX_ENABLE_RESTART_APPLICATION_ON_CONTEXT_LOST
//     auto director = Director::getInstance();
//     EventCustom recreatedEvent(EVENT_APP_RESTARTING);
//     director->getEventDispatcher()->dispatchEvent(&recreatedEvent);

//     //  Pop to root scene, replace with an empty scene, and clear all cached data before restarting
//     director->popToRootScene();
//     auto rootScene = Scene::create();
//     director->replaceScene(rootScene);
//     director->purgeCachedData();

//     JniHelper::callStaticVoidMethod("org/axmol/lib/AxmolEngine", "restartProcess");
// #endif
}


JNIEXPORT void JNICALL Java_org_cc2d_lib_ccRenderer_nativeRender(JNIEnv *, jclass) {
    std::chrono::steady_clock::time_point _lastUpdate = std::chrono::steady_clock::now(), now;
    now = std::chrono::steady_clock::now();
    static float dt;
    dt  = std::chrono::duration_cast<std::chrono::microseconds>(now - _lastUpdate).count() / 100.0f;
    _lastUpdate = now;
    glClear(GL_COLOR_BUFFER_BIT);
    cc2d::mainLoop(dt);
}

JNIEXPORT void JNICALL Java_org_cc2d_lib_ccRenderer_nativeOnPause(JNIEnv *, jclass) {
    // LOGD("Render::OnPause");
    // if (Director::getInstance()->getGLView())
    // {
    //     Application::getInstance()->applicationDidEnterBackground();
    //     EventCustom backgroundEvent(EVENT_COME_TO_BACKGROUND);
    //     Director::getInstance()->getEventDispatcher()->dispatchEvent(&backgroundEvent);
    // }
}

JNIEXPORT void JNICALL Java_org_cc2d_lib_ccRenderer_nativeOnResume(JNIEnv *, jclass) {
    // LOGD("Render::OnResume");
    // static bool firstTime = true;
    // if (Director::getInstance()->getGLView())
    // {
    //     // don't invoke at first to keep the same logic as iOS
    //     // can refer to https://github.com/cocos2d/cocos2d-x/issues/14206
    //     if (!firstTime)
    //         Application::getInstance()->applicationWillEnterForeground();

    //     EventCustom foregroundEvent(EVENT_COME_TO_FOREGROUND);
    //     Director::getInstance()->getEventDispatcher()->dispatchEvent(&foregroundEvent);

    //     firstTime = false;
    // }
}


JNIEXPORT void JNICALL
Java_org_cc2d_lib_ccRenderer_nativeTouchesBegin(JNIEnv *, jclass, jint id, jfloat x, jfloat y) {
    // LOGD("Render::TouchBegin");
    intptr_t idlong = id;
    // Director::getInstance()->getGLView()->handleTouchesBegin(1, &idlong, &x, &y);
    g_input.acquire(x, y);
    g_input.acquire(INPUT_PRESS);
}

JNIEXPORT void JNICALL
Java_org_cc2d_lib_ccRenderer_nativeTouchesEnd(JNIEnv *, jclass, jint id, jfloat x, jfloat y) {
    // LOGD("Render::TouchEnd");
    intptr_t idlong = id;
    // Director::getInstance()->getGLView()->handleTouchesEnd(1, &idlong, &x, &y);
    g_input.acquire(x, y);
    g_input.acquire(INPUT_RELEASE);
}

JNIEXPORT void JNICALL Java_org_cc2d_lib_ccRenderer_nativeTouchesMove(JNIEnv *env,
                                                                       jclass,
                                                                       jintArray ids,
                                                                       jfloatArray xs,
                                                                       jfloatArray ys) {
    int size = env->GetArrayLength(ids);
    jint id[size];
    jfloat x[size];
    jfloat y[size];

    env->GetIntArrayRegion(ids, 0, size, id);
    env->GetFloatArrayRegion(xs, 0, size, x);
    env->GetFloatArrayRegion(ys, 0, size, y);

    intptr_t idlong[size];
    for (int i = 0; i < size; i++)
        idlong[i] = id[i];

    // Director::getInstance()->getGLView()->handleTouchesMove(size, idlong, x, y);
    g_input.acquire(x[0], y[0]);
    g_input.acquire(INPUT_REPEAT);
}

JNIEXPORT void JNICALL Java_org_cc2d_lib_ccRenderer_nativeTouchesCancel(JNIEnv *env,
                                                                         jclass,
                                                                         jintArray ids,
                                                                         jfloatArray xs,
                                                                         jfloatArray ys) {
    LOGD("Render::TouchCancel");
    int size = env->GetArrayLength(ids);
    jint id[size];
    jfloat x[size];
    jfloat y[size];

    env->GetIntArrayRegion(ids, 0, size, id);
    env->GetFloatArrayRegion(xs, 0, size, x);
    env->GetFloatArrayRegion(ys, 0, size, y);

    intptr_t idlong[size];
    for (int i = 0; i < size; i++)
        idlong[i] = id[i];

    // Director::getInstance()->getGLView()->handleTouchesCancel(size, idlong, x, y);
    g_input.acquire(x[0], y[0]);
    g_input.acquire(INPUT_RELEASE);
}

#define KEYCODE_BACK 0x04
#define KEYCODE_MENU 0x52
#define KEYCODE_DPAD_UP 0x13
#define KEYCODE_DPAD_DOWN 0x14
#define KEYCODE_DPAD_LEFT 0x15
#define KEYCODE_DPAD_RIGHT 0x16
#define KEYCODE_ENTER 0x42
#define KEYCODE_PLAY 0x7e
#define KEYCODE_DPAD_CENTER 0x17

static std::unordered_map<int, int/*EventKeyboard::KeyCode*/> g_keyCodeMap = {
        // {KEYCODE_BACK, EventKeyboard::KeyCode::KEY_ESCAPE},
        // {KEYCODE_MENU, EventKeyboard::KeyCode::KEY_MENU},
        // {KEYCODE_DPAD_UP, EventKeyboard::KeyCode::KEY_DPAD_UP},
        // {KEYCODE_DPAD_DOWN, EventKeyboard::KeyCode::KEY_DPAD_DOWN},
        // {KEYCODE_DPAD_LEFT, EventKeyboard::KeyCode::KEY_DPAD_LEFT},
        // {KEYCODE_DPAD_RIGHT, EventKeyboard::KeyCode::KEY_DPAD_RIGHT},
        // {KEYCODE_ENTER, EventKeyboard::KeyCode::KEY_ENTER},
        // {KEYCODE_PLAY, EventKeyboard::KeyCode::KEY_PLAY},
        // {KEYCODE_DPAD_CENTER, EventKeyboard::KeyCode::KEY_DPAD_CENTER},

};

JNIEXPORT jboolean JNICALL Java_org_cc2d_lib_ccRenderer_nativeKeyEvent(JNIEnv *,
                                                                        jclass,
                                                                        jint keyCode,
                                                                        jboolean isPressed) {
    auto iterKeyCode = g_keyCodeMap.find(keyCode);
    if (iterKeyCode == g_keyCodeMap.end()) {
        return JNI_FALSE;
    }

    // EventKeyboard::KeyCode cocos2dKey = g_keyCodeMap.at(keyCode);
    // EventKeyboard event(cocos2dKey, isPressed);
    // Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
    g_input.acquire(keyCode);
    return JNI_TRUE;
}


JNIEXPORT void JNICALL Java_org_cc2d_lib_ccAccelerometer_onSensorChanged(JNIEnv *,
                                                                          jclass,
                                                                          jfloat x,
                                                                          jfloat y,
                                                                          jfloat z,
                                                                          jlong timeStamp) {
    // Acceleration a;
    // a.x         = -((double)x / TG3_GRAVITY_EARTH);
    // a.y         = -((double)y / TG3_GRAVITY_EARTH);
    // a.z         = -((double)z / TG3_GRAVITY_EARTH);
    // a.timestamp = (double)timeStamp / 1e9;

    // EventAcceleration event(a);
    // Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
}


JNIEXPORT void JNICALL
Java_org_cc2d_lib_ccRenderer_nativeInsertText(JNIEnv *env, jclass, jstring text) {
    std::string strValue = Charset::getStringUTFCharsJNI(env, text);
    const char *pszText = strValue.c_str();
    // IMEDispatcher::sharedDispatcher()->dispatchInsertText(pszText, strlen(pszText));
}

JNIEXPORT void JNICALL Java_org_cc2d_lib_ccRenderer_nativeDeleteBackward(JNIEnv *, jclass) {
    // IMEDispatcher::sharedDispatcher()->dispatchDeleteBackward();
}

JNIEXPORT jstring JNICALL Java_org_cc2d_lib_ccRenderer_nativeGetContentText(JNIEnv *env, jclass) {
    // auto pszText = IMEDispatcher::sharedDispatcher()->getContentText();
    // return Charset::newStringUTFJNI(env, pszText);
}


JNIEXPORT void JNICALL
Java_org_cc2d_lib_EditBoxHelper_nativeEditingDidBegin(JNIEnv *, jclass, jint index) {
    // editBoxEditingDidBegin(index);
}

JNIEXPORT void JNICALL Java_org_cc2d_lib_EditBoxHelper_nativeEditingChanged(JNIEnv *env,
                                                                             jclass,
                                                                             jint index,
                                                                             jstring text) {
    std::string textString = Charset::getStringUTFCharsJNI(env, text);
    // editBoxEditingDidChanged(index, textString);
}

JNIEXPORT void JNICALL Java_org_cc2d_lib_EditBoxHelper_nativeEditingDidEnd(JNIEnv *env,
                                                                            jclass,
                                                                            jint index,
                                                                            jstring text,
                                                                            jint action) {
    std::string textString = Charset::getStringUTFCharsJNI(env, text);
    // editBoxEditingDidEnd(index, textString, action);
}





/*
 * Class:     org_cocos2dx_lib_Cocos2dxWebViewHelper
 * Method:    shouldStartLoading
 * Signature: (ILjava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_cc2d_lib_WebViewHelper_nativeStartLoading(JNIEnv* env,
                                                                                          jclass,
                                                                                          jint index,
                                                                                          jstring jurl)
{
    // auto charUrl    = env->GetStringUTFChars(jurl, NULL);
    // std::string url = charUrl;
    // env->ReleaseStringUTFChars(jurl, charUrl);
    // return ax::ui::WebViewImpl::shouldStartLoading(index, url);
}

/*
 * Class:     org_cocos2dx_lib_Cocos2dxWebViewHelper
 * Method:    didFinishLoading
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_cc2d_lib_WebViewHelper_nativeFinishLoading(JNIEnv* env,
                                                                                    jclass,
                                                                                    jint index,
                                                                                    jstring jurl)
{
    // LOGD("didFinishLoading");
    // auto charUrl    = env->GetStringUTFChars(jurl, NULL);
    // std::string url = charUrl;
    // env->ReleaseStringUTFChars(jurl, charUrl);
    // ax::ui::WebViewImpl::didFinishLoading(index, url);
}

/*
 * Class:     org_cocos2dx_lib_Cocos2dxWebViewHelper
 * Method:    didFailLoading
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_cc2d_lib_WebViewHelper_nativeFailLoading(JNIEnv* env,
                                                                                  jclass,
                                                                                  jint index,
                                                                                  jstring jurl)
{
    // LOGD("didFailLoading");
    // auto charUrl    = env->GetStringUTFChars(jurl, NULL);
    // std::string url = charUrl;
    // env->ReleaseStringUTFChars(jurl, charUrl);
    // ax::ui::WebViewImpl::didFailLoading(index, url);
}

/*
 * Class:     org_cocos2dx_lib_Cocos2dxWebViewHelper
 * Method:    onJsCallback
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_cc2d_lib_WebViewHelper_nativeOnJsCallback(JNIEnv* env,
                                                                                jclass,
                                                                                jint index,
                                                                                jstring jmessage)
{
    // LOGD("jsCallback");
    // auto charMessage    = env->GetStringUTFChars(jmessage, NULL);
    // std::string message = charMessage;
    // env->ReleaseStringUTFChars(jmessage, charMessage);
    // ax::ui::WebViewImpl::onJsCallback(index, message);
}




JNIEXPORT void JNICALL Java_org_cc2d_lib_GameControllerAdapter_nativeControllerConnected(JNIEnv*,
                                                                                             jclass,
                                                                                             jstring deviceName,
                                                                                             jint controllerID)
{
    // AXLOG("controller id: %d connected!", controllerID);
    // ax::ControllerImpl::onConnected(ax::JniHelper::jstring2string(deviceName), controllerID);
}

JNIEXPORT void JNICALL Java_org_cc2d_lib_GameControllerAdapter_nativeControllerDisconnected(JNIEnv*,
                                                                                                jclass,
                                                                                                jstring deviceName,
                                                                                                jint controllerID)
{
    // AXLOG("controller id: %d disconnected!", controllerID);
    // ax::ControllerImpl::onDisconnected(ax::JniHelper::jstring2string(deviceName), controllerID);
}

JNIEXPORT void JNICALL Java_org_cc2d_lib_GameControllerAdapter_nativeControllerButtonEvent(JNIEnv*,
                                                                                               jclass,
                                                                                               jstring deviceName,
                                                                                               jint controllerID,
                                                                                               jint button,
                                                                                               jboolean isPressed,
                                                                                               jfloat value,
                                                                                               jboolean isAnalog)
{
    // ax::ControllerImpl::onButtonEvent(ax::JniHelper::jstring2string(deviceName), controllerID, button,
    //                                        isPressed, value, isAnalog);
}

JNIEXPORT void JNICALL Java_org_cc2d_lib_GameControllerAdapter_nativeControllerAxisEvent(JNIEnv*,
                                                                                             jclass,
                                                                                             jstring deviceName,
                                                                                             jint controllerID,
                                                                                             jint axis,
                                                                                             jfloat value,
                                                                                             jboolean isAnalog)
{
    // ax::ControllerImpl::onAxisEvent(ax::JniHelper::jstring2string(deviceName), controllerID, axis, value,
    //                                      isAnalog);
}



}



int getFontSizeAccordingHeightJni(int height)
{
    return JniHelper::callStaticIntMethod("org.cc2d.lib.BitmapHelper", "getFontSizeAccordingHeight", height);
}

std::string getStringWithEllipsisJni(const char* text, float width, float fontSize)
{
    return JniHelper::callStaticStringMethod("org.cc2d.lib.BitmapHelper", "getStringWithEllipsis", text, width, fontSize);
}


#undef LOGE
#undef LOGD