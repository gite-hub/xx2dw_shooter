#include "utf16.h"
#include "charset.h"

std::string Charset::getStringUTFCharsJNI(JNIEnv* env, jstring srcjStr, bool* ret)
{
    std::string utf8Str;
    if (srcjStr != nullptr && env != nullptr)
    {
        const unsigned short* unicodeChar = (const unsigned short*)env->GetStringChars(srcjStr, nullptr);
        size_t unicodeCharLength          = env->GetStringLength(srcjStr);
        const std::u16string unicodeStr((const char16_t*)unicodeChar, unicodeCharLength);
        bool flag = UTF16ToUTF8(unicodeStr, utf8Str);
        if (ret)
        {
            *ret = flag;
        }
        if (!flag)
        {
            utf8Str = "";
        }
        env->ReleaseStringChars(srcjStr, unicodeChar);
    }
    else
    {
        if (ret)
        {
            *ret = false;
        }
        utf8Str = "";
    }
    return utf8Str;
}

jstring Charset::newStringUTFJNI(JNIEnv* env, std::string_view utf8Str, bool* ret)
{
    std::u16string utf16Str;
    bool flag = UTF8ToUTF16(utf8Str, utf16Str);

    if (ret)
    {
        *ret = flag;
    }

    if (!flag)
    {
        utf16Str.clear();
    }
    jstring stringText = env->NewString((const jchar*)utf16Str.data(), utf16Str.length());
    return stringText;
}