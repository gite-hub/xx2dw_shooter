﻿#include "hfont_jni.h"
#include "jni_helper.h"
#include "cxx_t.h"
#include "cxx_func.h"
#include "logging.h"


static uchar* s_bitmapA8poiner = nullptr;
static int s_width, s_height;

#if defined(CC_ANDROID)
extern "C" {
JNIEXPORT void JNICALL
Java_org_cc2d_lib_BitmapHelper_nativeInitBitmapDC(JNIEnv *env, jclass, jint width, jint height,
                                                   jbyteArray pixels) {
    s_width = width;
    s_height = height;
    if (width * height && s_bitmapA8poiner != nullptr) {
        int pitch = cc2d::pitch<uchar>(width);
        int size = pitch * height;
        env->GetByteArrayRegion(pixels, 0, size, (jbyte *) s_bitmapA8poiner);
    }
}
}
#endif



static bool getBitmapJni(const std::string& char8, int font_size)
{
    JniMethodInfo methodInfo;

    // "([BLjava/lang/String;IIIIIIIIZFFFFZIIIIFZI)Z"
    if (!JniHelper::getStaticMethodInfo(methodInfo, "org.cc2d.lib.BitmapHelper",
                                        "createTextBitmapShadowStroke",
                                        "([BI)Z")){
        CC_LOG("%s %d: error to get methodInfo", __FILE__, __LINE__);
        return false;
    }


    /**create bitmap
     * this method call Cococs2dx.createBitmap()(java code) to create the bitmap, the java code
     * will call Java_org_axmol_lib_BitmapHelper_nativeInitBitmapDC() to init the width, height
     * and data.
     * use this approach to decrease the jni call number
     */
    int count = static_cast<int>(char8.length());
    jbyteArray strArray = methodInfo.env->NewByteArray(count);
    methodInfo.env->SetByteArrayRegion(strArray, 0, count, reinterpret_cast<const jbyte*>(char8.data()));
    jstring jstrFont = methodInfo.env->NewStringUTF("");

    if (!methodInfo.env->CallStaticBooleanMethod(methodInfo.classID, methodInfo.methodID, strArray, font_size))
//    if (!methodInfo.env->CallStaticBooleanMethod(
//            methodInfo.classID, methodInfo.methodID, strArray, jstrFont, font_size,
//            0xFF, 0xFF, 0xFF,
//            0xFF, 11, 0, 0, false,
//            0.0f, -0.0f,
//            0.0f, 0.0f,
//            false, 0,
//            0, 0,
//            0, 0.0f, true,
//            0))
    {
        return false;
    }

    methodInfo.env->DeleteLocalRef(strArray);
    methodInfo.env->DeleteLocalRef(jstrFont);
    methodInfo.env->DeleteLocalRef(methodInfo.classID);

    return true;
}


void setBitmapA8pointerJni(uchar* datas){
    s_bitmapA8poiner = datas;
}


bool toBitmapA8Jni(int font_size, const char* char8, int& width, int& height) {
    if (!getBitmapJni(char8, font_size)) {
        return false;
    };
    width  = s_width;
    height = s_height;
    return  true;
    // int pitch = cc2d::pitch<uchar>(width);
    // int size = pitch * height;
    // auto ptr = new uchar[size];
    // memcpy(datas, s_bitmap, pitch * height);
    // return true;
}