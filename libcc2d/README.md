# libcc2d

![](demo.png)

C++ 游戏引擎(PC+Android)

GLSL着色器渲染

## PC

glad+glfw

GDI字体

## Android

egl+gles

系统字体

参考cocos2dx的java和jni

## 功能

IO文件读写

AssetManager读取

音频播放(从内存中读取也OK)

WDF直读

染色直读

WAS/SP直读

MAP直读

UTF8<-->16<-->32

文字缓存纹理

文本解析 #表情 #颜色

