#pragma once
#include "cxx.h"
#include "vec2.h"

enum ASTAR_TYPE {
	ASTAR_UNKNOWN,
	ASTAR_OPEN,
	ASTAR_CLOSE,
	ASTAR_ROAD = 0,
	ASTAR_WALL,
};

class AstarNode {
public:
	void reset();
	int getF();
private:
	int heapi, g, h, f;
	ASTAR_TYPE type;
	ASTAR_TYPE opend;
	Vec2i parent;
	friend class AstarHeap;
	friend class cAstar;
};

class AstarHeap : public std::vector<Vec2i> {
public:
	void setNode(AstarNode* pnode, int pitch);

	AstarNode& att(int x, int y);

	AstarNode& att(const Vec2i& v);

	AstarNode& att(int i);

	int getF(int i);

	void swap(int& a, int b);

	void remove1st();

	void pushBack(const Vec2i& element);
	// when F change
	void update(int i);

	int getMinF();

protected:
	int _pitch;
	AstarNode* _pnode = nullptr;
};


class Astar {
public:
	Astar();
	void setNext(int x, int y);
	void setNext();
public:
	int next_x, next_y, next_index;
	std::vector<Vec2i> paths;
public:
	static const int kStand = -2;
	static const int kStright = -1;
	friend class cAstar;
};

class cAstar : public AstarHeap {
public:
	~cAstar();

	void reset();

	void load(int mapid, int width, int height, const uchar* datas);

	bool find(int x, int y, int tx, int ty, Vec2i& vec);

	bool find(Astar* pobj, int dx, int dy, int tx, int ty);

private:
	int _mapid = 0;
	int _width = 0;
	int _height = 0;

	AstarNode* _pnode = nullptr;
	int _nodes_size = 0;
public:

};

extern cAstar g_astar;
