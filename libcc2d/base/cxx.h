#pragma once
#include "cxx_t.h"
#include "cxx_inl.h"
#include "cxx_stl.h"

#define forr(_stl_,_i_) for(int _i_ = static_cast<int>(_stl_.size()) - 1; _i_ >= 0; --_i_)
#define forv(_stl_,_i_) for(int _i_ = 0; _i_ < static_cast<int>(_stl_.size()); ++_i_)
