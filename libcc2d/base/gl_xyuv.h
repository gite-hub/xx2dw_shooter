#pragma once
#include "cxx_pod.h"

namespace cc2d {

	void createOrthographicOffCenter(float left, float right, float bottom, float top);

	void toXYUV(float world_x, float world_y, int rect_width, int rect_height, float scale, VertexXYUV* xyuv);

	void toXYUV(float world_x, float world_y, float render_width, float render_height, VertexXYUV* xyuv);

	void toXYUV(float world_x, float world_y, int texture_width, int texture_height, float render_width, float render_height, VertexXYUV* xyuv);

	void toXYUV(float world_x, float world_y,
		int texture_width, int texture_height,
		int rect_x, int rect_y, int rect_width, int rect_height,
		float render_width, float render_height, VertexXYUV* xyuv);
}
