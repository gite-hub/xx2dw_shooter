#include "colorgb.h"

static rgb16_t c5;
static rgb32_t c8;
COLOR::COLOR() {
	inner = COLOR::WHITE.inner;
}

COLOR::COLOR(color_t c) {
	inner = c;
}

COLOR::COLOR(const COLOR& c) {
	inner = c.inner;
}

COLOR::COLOR(rgb16_t c) {
	r = rgb5to8(c.r);
	g = rgb6to8(c.g);
	b = rgb5to8(c.b);
}

COLOR::COLOR(uchar r, uchar g, uchar b, uchar a) {
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

COLOR::operator color_t() const {
	return inner;
}

COLOR::operator rgb32_t() const {
	return to32(inner);
}

COLOR::operator rgb16_t() const {
	return to16(inner);
}


rgb16_t COLOR::to16(uint color) {
	c8.inner = color;
	c5.r = c8.r >> 3;
	c5.g = c8.g >> 2;
	c5.b = c8.b >> 3;
	return c5;
}


rgb32_t COLOR::to32(color_t color) {
	c8.inner = color;
	return c8;
}


rgb32_t COLOR::to32(rgb16_t c565, uchar alpha5) {
    c8.r = rgb5to8(c565.r);
    c8.g = rgb6to8(c565.g);
    c8.b = rgb5to8(c565.b);
    c8.a = rgb5to8(alpha5);
    return c8;
}



uchar COLOR::rgb5to8(uchar five) {
	return (five == 0) ? 0 : (five << 3 | 7);
}

uchar COLOR::rgb6to8(uchar six) {
	return (six == 0) ? 0 : (six << 2 | 3);
}

const COLOR COLOR::BLACK(0x00, 0x00, 0x00);
const COLOR COLOR::RED(0xFF, 0x00, 0x00);
const COLOR COLOR::GREEN(0x00, 0xFF, 0x00);
const COLOR COLOR::BLUE(0x00, 0x00, 0xFF);
const COLOR COLOR::YELLOW(0xFF, 0xFF, 0x00);
const COLOR COLOR::WHITE(0xFF, 0xFF, 0xFF);
const COLOR COLOR::DYE(0x7F, 0x7F, 0x7F);
const COLOR COLOR::NPC(0xE0, 0xD4, 0x28);
const COLOR COLOR::OBJECT(0x6A, 0xE4, 0x7E);
