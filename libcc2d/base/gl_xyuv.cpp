#include "gl_xyuv.h"
#include "extern2d.h"
#define cc_use_matrix 0
#if cc_use_matrix
#include "mat4.h"
#include "transform_node.h"
static  Mat4 s_projection_matrix;
#endif
void cc2d::createOrthographicOffCenter(float left, float right, float bottom, float top) {
#if cc_use_matrix
	Mat4::createOrthographicOffCenter(left, right, bottom, top, -1.0f, 1.0f, &s_projection_matrix);
#endif
}


void cc2d::toXYUV(float world_x, float world_y, float render_width, float render_height, VertexXYUV* xyuv) {
#if 0
	xyuv[0].x = xyuv[1].x = world_x - g_width * 0.5f;
	xyuv[2].x = xyuv[3].x = world_x + render_width - g_width * 0.5f;
	xyuv[0].y = xyuv[2].y = g_height * 0.5f - world_y;
	xyuv[1].y = xyuv[3].y = g_height * 0.5f - world_y - render_height;

	for (int i = 0; i < kGLquad4; i += 1) {
		xyuv[i].x *= 2.0f / g_width;
		xyuv[i].y *= 2.0f / g_height;
	}
#elif 1
	xyuv[0].x = xyuv[1].x = world_x * 2.0f / g_width - 1.0f;
	xyuv[2].x = xyuv[3].x = (world_x + render_width) * 2.0f / g_width - 1.0f;
	xyuv[0].y = xyuv[2].y = 1.0f - world_y * 2.0f / g_height;
	xyuv[1].y = xyuv[3].y = 1.0f - (world_y + render_height) * 2.0f / g_height;
#endif
}

#if cc_use_matrix
void cc2d::toXYUV(float world_x, float world_y, int rect_width, int rect_height, float scale, VertexXYUV* xyuv) {
	
	static Vec2i temp[kGLquad4];
	temp[0] = { 0, 0 };
	temp[1] = { rect_width, 0 };
	temp[2] = { 0, rect_height };
	temp[3] = { rect_width, rect_height };

	static TransformNode node;
	node.setPos(world_x, world_y);
	node.setSize(rect_width, rect_height);
	node.setScale(scale);

	static Mat4 m;
	m = s_projection_matrix * node.getTransform();
	for (int i = 0, z = 0, w = 1; i < kGLquad4; i += 1) {
		// 仿射也是这几个值
		xyuv[i].x = m.m[0] * temp[i].x + m.m[4] * temp[i].y + m.m[8] * z + m.m[12] * w;
		xyuv[i].y = m.m[1] * temp[i].x + m.m[5] * temp[i].y + m.m[9] * z + m.m[13] * w;
	}
}
#endif

void cc2d::toXYUV(float world_x, float world_y, int texture_width, int texture_height, float render_width, float render_height, VertexXYUV* xyuv) {
#if cc_use_matrix
	// GL_TRIANGLE_STRIP 的顺序排列，顺序为左上、右上、左下、右下
	xyuv[0].v = xyuv[1].v = 0.0f;
	xyuv[2].v = xyuv[3].v = 1.0f;
	xyuv[0].u = xyuv[2].u = 0.0f;
	xyuv[1].u = xyuv[3].u = 1.0f;
	toXYUV(world_x, world_y, texture_width, texture_height, render_width / texture_width, xyuv);
#else
	// GL_TRIANGLE_STRIP 顺序为左下、左上、右下、右上
	xyuv[0].u = xyuv[1].u = 0.0f;
	xyuv[2].u = xyuv[3].u = 1.0f;
	xyuv[0].v = xyuv[2].v = 0.0f;
	xyuv[1].v = xyuv[3].v = 1.0f;
	toXYUV(world_x, world_y, render_width, render_height, xyuv);
#endif
}


void cc2d::toXYUV(float world_x, float world_y, 
	int texture_width, int texture_height, 
	int rect_x, int rect_y, int rect_width, int rect_height, 
	float render_width, float render_height, VertexXYUV* xyuv) {
#if cc_use_matrix
	xyuv[0].v = xyuv[1].v = rect_x * 1.0f / texture_width;
	xyuv[2].v = xyuv[3].v = (rect_x + rect_width) * 1.0f / texture_width;
	xyuv[0].u = xyuv[2].u = rect_y * 1.0f / texture_height;
	xyuv[1].u = xyuv[3].u = (rect_y + rect_height) * 1.0f / texture_height;
	toXYUV(world_x, world_y, texture_width, texture_height, render_width / texture_width, xyuv);
#else
	xyuv[0].u = xyuv[1].u = rect_x * 1.0f / texture_width;
	xyuv[2].u = xyuv[3].u = (rect_x + rect_width) * 1.0f / texture_width;
	xyuv[0].v = xyuv[2].v = rect_y * 1.0f / texture_height;
	xyuv[1].v = xyuv[3].v = (rect_y + rect_height) * 1.0f / texture_height;
	toXYUV(world_x, world_y, render_width, render_height, xyuv);
#endif
}

#undef cc_use_matrix
