#pragma once
#include "scheduler.h"

class SchedulerNode : public Ref {
public:
    SchedulerNode();
    virtual ~SchedulerNode();

    void setScheduler(Scheduler* scheduler);

    Scheduler* getScheduler() { return _scheduler; }
    const Scheduler* getScheduler() const { return _scheduler; }


    bool isScheduled(SEL_SCHEDULE selector);

    bool isScheduled(const std::string& key);

    void scheduleUpdate(void);

    void scheduleUpdateWithPriority(int priority);

    void unscheduleUpdate(void);

    void schedule(SEL_SCHEDULE selector, float interval, unsigned int repeat, float delay);

    void schedule(SEL_SCHEDULE selector, float interval);

    void scheduleOnce(SEL_SCHEDULE selector, float delay);

    void scheduleOnce(const ccSchedulerFunc& callback, float delay, const std::string& key);

    void schedule(SEL_SCHEDULE selector);

    void schedule(const ccSchedulerFunc& callback, const std::string& key);

    void schedule(const ccSchedulerFunc& callback, float interval, const std::string& key);

    void schedule(const ccSchedulerFunc& callback, float interval, unsigned int repeat, float delay, const std::string& key);

    void unschedule(SEL_SCHEDULE selector);

    void unschedule(const std::string& key);

    void unscheduleAllCallbacks();
    void resume(void);
    void pause(void);

    virtual void update(float delta) {}

protected:
    Scheduler* _scheduler = nullptr;
    bool _running = true;
    bool _isScheduled = false;
};


#define SCHEDULE_SELECTOR(_SELECTOR) static_cast<SEL_SCHEDULE>(&_SELECTOR)
