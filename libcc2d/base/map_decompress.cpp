#include "map_decompress.h"
#include "memory2d.h"
#include "libjpeg-turbo-3.0.3/turbojpeg.h"

bool toJpegTurbo(uchar* jpeg_datas, uint size, rgb24_t* datas) {
	int jpegsubsamp;
	tjhandle jpeg = tjInitDecompress();
	if (!jpeg) {
		// Error("ReadJpegAsRGB() tjInitDecompress() failed: %s", tjGetErrorStr());
		// return std::make_tuple(false, std::vector<uint8>(0), 0, 0, 0);
		return false;
	}
	int width;
	int height;
	if (tjDecompressHeader2(jpeg, jpeg_datas, size, &width, &height, &jpegsubsamp) != 0) {
		// Error("ReadJpegAsRGB() tjDecompressHeader2() failed: %s", tjGetErrorStr());
		// return std::make_tuple(false, std::vector<uint8>(0), 0, 0, 0);
		return false;
	}
	uint pitch = tjPixelSize[TJPF_RGB] * width;
	return tjDecompress2(jpeg, jpeg_datas, size, (uchar*)datas, width, pitch, height, TJPF_RGB, 0) == 0;
}


int decompressMask(void* in, void* out) {

	/*register*/ uchar* op;
	/*register*/ uchar* ip;
	/*register*/ unsigned t;
	/*register*/  uchar* m_pos;

	op = (uchar*)out;
	ip = (uchar*)in;

	if (*ip > 17) {
		t = *ip++ - 17;
		if (t < 4)
			goto match_next;
		do *op++ = *ip++; while (--t > 0);
		goto first_literal_run;
	}

	while (1) {
		t = *ip++;
		if (t >= 16) goto match;
		if (t == 0) {
			while (*ip == 0) {
				t += 255;
				ip++;
			}
			t += 15 + *ip++;
		}

		*(unsigned*)op = *(unsigned*)ip;
		op += 4; ip += 4;
		if (--t > 0) {
			if (t >= 4) {
				do {
					*(unsigned*)op = *(unsigned*)ip;
					op += 4; ip += 4; t -= 4;
				} while (t >= 4);
				if (t > 0) do *op++ = *ip++; while (--t > 0);
			} else do *op++ = *ip++; while (--t > 0);
		}

	first_literal_run:

		t = *ip++;
		if (t >= 16)
			goto match;

		m_pos = op - 0x0801;
		m_pos -= t >> 2;
		m_pos -= *ip++ << 2;

		*op++ = *m_pos++; *op++ = *m_pos++; *op++ = *m_pos;

		goto match_done;

		while (1) {
		match:
			if (t >= 64) {

				m_pos = op - 1;
				m_pos -= (t >> 2) & 7;
				m_pos -= *ip++ << 3;
				t = (t >> 5) - 1;

				goto copy_match;

			} else if (t >= 32) {
				t &= 31;
				if (t == 0) {
					while (*ip == 0) {
						t += 255;
						ip++;
					}
					t += 31 + *ip++;
				}

				m_pos = op - 1;
				m_pos -= (*(unsigned short*)ip) >> 2;
				ip += 2;
			} else if (t >= 16) {
				m_pos = op;
				m_pos -= (t & 8) << 11;
				t &= 7;
				if (t == 0) {
					while (*ip == 0) {
						t += 255;
						ip++;
					}
					t += 7 + *ip++;
				}
				m_pos -= (*(unsigned short*)ip) >> 2;
				ip += 2;
				if (m_pos == op)
					goto eof_found;
				m_pos -= 0x4000;
			} else {
				m_pos = op - 1;
				m_pos -= t >> 2;
				m_pos -= *ip++ << 2;
				*op++ = *m_pos++; *op++ = *m_pos;
				goto match_done;
			}

			if (t >= 6 && (op - m_pos) >= 4) {
				*(unsigned*)op = *(unsigned*)m_pos;
				op += 4; m_pos += 4; t -= 2;
				do {
					*(unsigned*)op = *(unsigned*)m_pos;
					op += 4; m_pos += 4; t -= 4;
				} while (t >= 4);
				if (t > 0) do *op++ = *m_pos++; while (--t > 0);
			} else {
			copy_match:
				*op++ = *m_pos++; *op++ = *m_pos++;
				do *op++ = *m_pos++; while (--t > 0);
			}

		match_done:

			t = ip[-2] & 3;
			if (t == 0)	break;

		match_next:
			do *op++ = *ip++; while (--t > 0);
			t = *ip++;
		}
	}

eof_found:
	//   if (ip != ip_end) return -1;

	return (op - (uchar*)out);
}


static inline void swapByte(ushort* value) { *value = ((*value) << 8) | ((*value) >> 8); }

int jpegHandler(uchar* buffer, int in_size, uchar* out_datas) {
	// JPEG数据处理原理
	// 1、复制D8到D9的数据到缓冲区中
	// 2、删除第3、4个字节 FFA0
	// 3、修改FFDA的长度00 09 为 00 0C
	// 4、在FFDA数据的最后添加00 3F 00
	// 5、替换FFDA到FF D9之间的FF数据为FF 00
	int length = 0;			// 临时变量，表示已读取的长度
	auto ptr = out_datas;	// 已处理数据的开始地址
	ushort ushort_value = 0;// 临时变量，表示循环的次数
	uint out_size = 0;
	// 当已读取数据的长度小于总长度时继续
	while (length < in_size && *buffer++ == 0xFF) {
		*ptr++ = 0xFF;
		length++;
		switch (*buffer) {
		case 0xD8:
			*ptr++ = 0xD8;
			*buffer++;
			length++;
			break;
		case 0xA0:
			*buffer++;
			ptr--;
			length++;
			break;
		case 0xC0:
			*ptr++ = 0xC0;
			*buffer++;
			length++;

			memcpy(&ushort_value/*, sizeof(ushort)*/, buffer, sizeof(ushort_value)); // 读取长度
			swapByte(&ushort_value); // 将长度转换为Intel顺序


			for (int i = 0; i < ushort_value; i++) {
				*ptr++ = *buffer++;
				length++;
			}

			break;
		case 0xC4:
			*ptr++ = 0xC4;
			*buffer++;
			length++;

			memcpy(&ushort_value/*, sizeof(ushort)*/, buffer, sizeof(ushort_value)); // 读取长度
			swapByte(&ushort_value); // 将长度转换为Intel顺序

			for (int i = 0; i < ushort_value; i++) {
				*ptr++ = *buffer++;
				length++;
			}
			break;
		case 0xDB:
			*ptr++ = 0xDB;
			*buffer++;
			length++;

			memcpy(&ushort_value/*, sizeof(ushort)*/, buffer, sizeof(ushort_value)); // 读取长度
			swapByte(&ushort_value); // 将长度转换为Intel顺序

			for (int i = 0; i < ushort_value; i++) {
				*ptr++ = *buffer++;
				length++;
			}
			break;
		case 0xDA:
			*ptr++ = 0xDA;
			*ptr++ = 0x00;
			*ptr++ = 0x0C;
			*buffer++;
			length++;

			memcpy(&ushort_value, /*sizeof(ushort),*/ buffer, sizeof(ushort_value)); // 读取长度
			swapByte(&ushort_value); // 将长度转换为Intel顺序
			*buffer++;
			length++;
			*buffer++;

			for (int i = 2; i < ushort_value; i++) {
				*ptr++ = *buffer++;
				length++;
			}
			*ptr++ = 0x00;
			*ptr++ = 0x3F;
			*ptr++ = 0x00;
			out_size += 1; // 这里应该是+3的，因为前面的0xFFA0没有-2，所以这里只+1。

			// 循环处理0xFFDA到0xFFD9之间所有的0xFF替换为0xFF00
			for (; length < in_size - 2;) {
				if (*buffer == 0xFF) {
					*ptr++ = 0xFF;
					*ptr++ = 0x00;
					*buffer++;
					length++;
					out_size++;
				} else {
					*ptr++ = *buffer++;
					length++;
				}
			}
			// 直接在这里写上了0xFFD9结束Jpeg图片.
			out_size--; // 这里多了一个字节，所以减去。
			*ptr--;
			*ptr-- = 0xD9;
			break;
		case 0xD9:
			// 算法问题，这里不会被执行，但结果一样。
			*ptr++ = 0xD9;
			length++;
			break;
		default:
			break;
		}
	}
	out_size += in_size;
    return out_size;
}



bool decompressToMask(int width, int height, const char* pointer, uchar* temp_memory, uchar* decodes) {
	int mask_size = *(int*)(pointer + 16);
	if (mask_size == 0) {
		return false;
	}
	// alignment 4
	int aiginw = ((width >> 2) + (width % 4 != 0)) << 2;
	uchar* compress_datas = (uchar*)pointer + 20;
	// 4 pixel per Byte, so >> 2
	int mask_index = (aiginw * height) >> 2;

	decompressMask(compress_datas, temp_memory);

	memset(decodes, 0, width * height * sizeof(*decodes));
	auto decs = decodes;
	uchar mask_value;
	for (int k = 0, i; k < height; k += 1) {
		for (i = 0; i < width; ++i, decs += 1) {
			mask_index = (k * aiginw + i) << 1;			   // bit
			mask_value = temp_memory[mask_index >> 3];		// to byte
			mask_value = mask_value >> (mask_index % 8);   // to bit
			if ((mask_value & 3) == 3) {
				*decs = 1;
			}
		}
	}
	return true;
}
