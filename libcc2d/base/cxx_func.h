#pragma once
#include "cxx_enum.h"

namespace cc2d {

	inline int pitch(int width, int byte) {
		return (((((width * byte) << 1) + 7) & 0xFFFFFFF8) >> 1) / byte;
	}
	template<typename T>
	inline int pitch(int width) { return pitch(width, sizeof(T)); }

	inline int getByte(PIXEL_BIT pixel_bit) { return pixel_bit + 1; }
}
