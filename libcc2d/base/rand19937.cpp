#include "rand19937.h"

std::mt19937& random19937() {
	static std::random_device seed_gen;
	static std::mt19937 engine(seed_gen());
	return engine;
}
