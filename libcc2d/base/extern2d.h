#pragma once

static const int kMinSolutionHeight = 400;

extern int g_width, g_height;
extern float g_scale;
extern bool g_integer_scale;
extern float g_dt;
