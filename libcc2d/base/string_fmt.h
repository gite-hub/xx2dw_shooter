#pragma once
#include <string>
#include <stdarg.h>

/** @def AX_FORMAT_PRINTF(formatPos, argPos)
 * Only certain compiler support __attribute__((format))
 *
 * @param formatPos 1-based position of format string argument.
 * @param argPos    1-based position of first format-dependent argument.
 */
#if defined(__GNUC__) && (__GNUC__ >= 4)
#    define CC_FORMAT_PRINTF(formatPos, argPos) __attribute__((__format__(printf, formatPos, argPos)))
#elif defined(__has_attribute)
#    if __has_attribute(format)
#        define CC_FORMAT_PRINTF(formatPos, argPos) __attribute__((__format__(printf, formatPos, argPos)))
#    else
#        define CC_FORMAT_PRINTF(formatPos, argPos)
#    endif  // __has_attribute(format)
#else
#    define CC_FORMAT_PRINTF(formatPos, argPos)
#endif

#if defined(_MSC_VER)
#    define CC_FORMAT_PRINTF_SIZE_T "%08lX"
#else
#    define CC_FORMAT_PRINTF_SIZE_T "%08zX"
#endif


namespace cc2d {
    std::string format(const char* fmt, ...) CC_FORMAT_PRINTF(1, 2);
    std::string format(const char* fmt, va_list ap);
}
