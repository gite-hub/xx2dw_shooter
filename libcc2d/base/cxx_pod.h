#pragma once
#include "cxx_t.h"


using char_utf8_t = unsigned long long;

// bgr --> high bit 0x(R|G|B) --> low bit
union rgb16_t { ushort inner; struct { ushort b : 5, g : 6, r : 5; }; };
// rgba --> high bit 0x(A|B|G|R) --> low bit
union rgb32_t { uint inner; struct { uchar r, g, b, a; }; };

struct rgb24_t { uchar r, g, b; };

union rgb44_t { ushort inner; struct { ushort a : 4, b : 4, g : 4, r : 4; }; };

using color_t = uint;



using was_t = unsigned int;
static constexpr was_t k0u = 0u;

using dye_t = uint;



// 在类或结构体继承时满足以下两个条件之一：
// 1、派生类中有非静态成员，且只有仅包含静态成员的基类。
// 2、基类有非静态成员，而派生类没有非静态成员。
// 其实就是派生类和基类中不允许同时出现非静态成员，因为同时有非静态成员就无法进行memcpy
struct VertexXYUV { float x, y, u, v; };
