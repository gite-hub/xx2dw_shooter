#include "direction.h"
#include "cxx.h"


void Direction::normalize(d8_t direction, int& x, int& y) {
    switch (direction) {
    case D8_RIGHT_UP:
        x = 1;
        y = -1;
        break;
    case D8_LEFT_UP:
        x = -1;
        y = -1;
        break;
    case D8_RIGHT_DOWN:
        x = 1;
        y = 1;
        break;
    case D8_LEFT_DOWN:
        x = -1;
        y = 1;
        break;
    }
}


d8_t Direction::reverse(d8_t direction) {
	static std::map<d8_t, d8_t> reverse_table;
	if (reverse_table.empty()) {
		reverse_table.emplace(D8_UP, D8_DOWN);
		reverse_table.emplace(D8_DOWN, D8_UP);
		reverse_table.emplace(D8_LEFT, D8_RIGHT);
		reverse_table.emplace(D8_RIGHT, D8_LEFT);
		reverse_table.emplace(D8_LEFT_UP, D8_RIGHT_DOWN);
		reverse_table.emplace(D8_RIGHT_DOWN, D8_LEFT_UP);
		reverse_table.emplace(D8_LEFT_DOWN, D8_RIGHT_UP);
		reverse_table.emplace(D8_RIGHT_UP, D8_LEFT_DOWN);
	}

	const auto& it = reverse_table.find(direction);
	if (it != reverse_table.end()) {
		return it->second;
	}
	return direction;
}


d8_t Direction::rotate(bool slant, d8_t direction, bool clockwise) {
	static std::vector<d8_t> direction4s, direction8s;
	if (direction4s.empty()) {
		direction4s.emplace_back(D8_RIGHT_UP);
		direction4s.emplace_back(D8_RIGHT_DOWN);
		direction4s.emplace_back(D8_LEFT_DOWN);
		direction4s.emplace_back(D8_LEFT_UP);

		direction8s.emplace_back(D8_LEFT);
		direction8s.emplace_back(D8_LEFT_UP);
		direction8s.emplace_back(D8_UP);
		direction8s.emplace_back(D8_RIGHT_UP);
		direction8s.emplace_back(D8_RIGHT);
		direction8s.emplace_back(D8_RIGHT_DOWN);
		direction8s.emplace_back(D8_DOWN);
		direction8s.emplace_back(D8_LEFT_DOWN);
	}
	const auto& directions = slant ? direction8s : direction4s;
	int index = k_1;
	forv(directions, i) {
		if (directions[i] == direction) {
			index = i;
			break;
		}
	}
	if (index == k_1) {
		return direction;
	}
	if (clockwise) {
		++index;
	} else if ((--index) < 0) {
		return directions.back();
	}
	return directions[index % directions.size()];
}


struct Degree {
	int lower, upper;
	d8_t direction1, direction2;
	Degree(int l, int u, d8_t d1, d8_t d2 = D8_NONE) : lower(l), upper(u), direction1(d1), direction2(d2) {}
};
d8_t Direction::vector(d8_t direction, float x, float y) {

	static std::vector<Degree> degrees;
	if (degrees.empty()) {
		std::vector<d8_t> directions = { D8_RIGHT, D8_RIGHT_UP, D8_UP, D8_LEFT_UP, D8_LEFT, D8_LEFT_DOWN, D8_DOWN, D8_RIGHT_DOWN };
		for (int i = 0, size = directions.size(), degree = 0; i < size; ++i) {
			// double times
			// degrees.emplace_back(Degree(-30, -15, D8_RIGHT_DOWN, D8_RIGHT));
			// degrees.emplace_back(Degree(-22.5f, 22.5f, D8_RIGHT));
			// degrees.emplace_back(Degree(15, 30, D8_RIGHT, D8_RIGHT_UP));
			// degrees.emplace_back(Degree(22.5f, 67.5f, D8_RIGHT_UP));
			degrees.emplace_back(Degree(degree - 60, degree - 30, directions[(i - 1 + size) % size], directions[i]));
			degrees.emplace_back(Degree(degree - 45, degree + 45, directions[i]));
			degree += 90;
		}
	}

	// anti 0 -- (-180)
	auto degreef = -(std::atan2(y, x) * 57.29577951f);
	if (degreef < -30) {
		degreef += 360;
	}
	int degree = degreef * 2;

	int index = k_1;
	forv(degrees, i) {
		const auto& d = degrees[i];
		if (degree < d.lower || degree >= d.upper) {
			continue;
		}
		index = i;
		if (d.direction2 != D8_NONE && (direction == d.direction1 || direction == d.direction2)) {
			return direction;
		}
	}
	return degrees[index].direction1;
}


d8_t Direction::vector(int vx, int vy) {
	if (vx < 0) {
		return (vy < 0) ? D8_LEFT_UP : D8_LEFT_DOWN;
	}
	return (vy < 0) ? D8_RIGHT_UP : D8_RIGHT_DOWN;
}
