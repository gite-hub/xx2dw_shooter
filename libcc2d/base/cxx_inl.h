#pragma once
#include <vector>

template <typename T>
inline void DELETE_PTR(T& pointer) noexcept { if (pointer != nullptr) { delete pointer; pointer = nullptr; } }

template <typename T>
inline void DELETE_ARR(T& pointer) noexcept { if (pointer != nullptr) { delete[] pointer; pointer = nullptr; } }

template <typename T>
inline void FREE_PTR(T& pointer) noexcept { if (pointer != nullptr) { free(pointer); pointer = nullptr; } }

template <typename T>
inline void RELEASE_PTR(T& pointer) { if (pointer != nullptr) { pointer->release(); pointer = nullptr; } }

template <typename T>
inline bool FOUND(const std::vector<T>& container, const T& element) { 
	return std::find(container.begin(), container.end(), element) != container.end();
}
