#include "reference_count.h"

ReferenceCount::~ReferenceCount() {

}

void ReferenceCount::retain() {
    _referenceCount += 1;
}

void ReferenceCount::release() {
    _referenceCount -= 1;
}

unsigned int ReferenceCount::getReferenceCount() const {
    return _referenceCount;
}
