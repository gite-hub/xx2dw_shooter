#include "astar.h"
#include "cxx_inl.h"

cAstar g_astar;

void AstarNode::reset() {
	heapi = k_1;
	h = k_1;
	g = k_1;
	f = k_1;
	opend = ASTAR_UNKNOWN;
	parent.x = k_1;
	parent.y = k_1;
}

int AstarNode::getF() {
	return h + g;
}


//////////////////////////////////////////////////////////////////////////
void AstarHeap::setNode(AstarNode* pnode, int pitch) {
	_pnode = pnode;
	_pitch = pitch;
}

AstarNode& AstarHeap::att(int x, int y) {
	return _pnode[y * _pitch + x];
}

AstarNode& AstarHeap::att(const Vec2i& v) {
	return _pnode[v.y * _pitch + v.x];
}

AstarNode& AstarHeap::att(int i) {
	return att(this->at(i));
}


int AstarHeap::getF(int i) {
	return this->att(i).getF();
}

void AstarHeap::swap(int& a, int b) {
	std::swap(this->at(a), this->at(b));
	// 同步保存地图中该坐标在堆中的最新位置
	this->att(a).heapi = a;
	this->att(b).heapi = b;
	a = b;
}

void AstarHeap::remove1st() {
	int size_1 = static_cast<int>(this->size()) - 1;
	if (size_1 >= 0) {
		this->att(size_1).heapi = 0;
		this->att(0).heapi = k_1;
		this->at(0) = this->at(size_1);
		this->pop_back();

		for (int index = 0, left_i, right_i, min_i; index < this->size(); ) {
			left_i = 2 * index + 1;
			right_i = 2 * index + 2;

			if (right_i > this->size()) {
				break;
			}
			min_i = left_i;
			// 有两个孩子，找出两个孩子节点中F值最低的元素
			if (right_i < this->size()) {
				if (this->getF(min_i) > this->getF(right_i)) {
					min_i = right_i;
				}
			}
			//如果当前节点的F值 大于 他孩子节点的F值，则交换
			if (this->getF(index) > this->getF(min_i)) {
				this->swap(index, min_i);
			} else {
				break;
			}
		}
	}
}



//往堆中添加新的元素（节点）
void AstarHeap::pushBack(const Vec2i& element) {
	this->emplace_back(element);
	int index = this->size() - 1, parent_i;
	this->att(index).heapi = index;
	//不断的与他的父节点比较，直到该新节点的F值大于他的父节点的F值为止 或者 该新节点到了堆首
	while (index > 0) {
		parent_i = (index - 1) / 2;
		if (this->getF(index) < this->getF(parent_i)) {
			this->swap(index, parent_i);
			continue;
		} else {
			break;
		}
	}
}

//当堆中某元素的F值发生改变时，更新该元素在堆中的位置
void AstarHeap::update(int index) {
	int curr_i = index, parent_i;
	Vec2i temp;
	//如果上面的循环没有执行，则无法判断该节点的最新F值的相对大小，
	//所以，此时需要把该节点移动到堆首删除掉，然后再在堆末尾添加该节点，最后程序再把该节点移动到堆中适当的位置处
	if (curr_i == index) {
		// 把该节点移动到堆首
		while (curr_i > 0) {
			parent_i = (curr_i - 1) / 2;
			this->swap(curr_i, parent_i);
		}
	}
	temp = this->at(0);
	remove1st();
	pushBack(temp);
}


int AstarHeap::getMinF() {
	if (!this->empty()) {
		const auto& v = this->at(0);
		return this->att(v.x, v.x).getF();
	}
	return 0;
}



//////////////////////////////////////////////////////////////////////////
Astar::Astar() {
	next_index = kStand;
}

void Astar::setNext(int x, int y) {
	next_x = x; next_y = y;
}

void Astar::setNext() {
	const auto& t = paths[next_index];
	next_x = t.x;
	next_y = t.y;
}


//////////////////////////////////////////////////////////////////////////
cAstar::~cAstar() {
	DELETE_ARR(_pnode);
}


void cAstar::reset() {

	auto pnode = _pnode;
	for (int i = _width * _height - 1; i >= 0; --i, ++pnode) {
		pnode->reset();
	}
	this->clear();
	this->setNode(_pnode, _width);
}



void cAstar::load(int mapid, int width, int height, const uchar* datas) {
	if (_mapid == mapid) {
		return;
	}
	_mapid = mapid;
	_width = width;
	_height = height;
	if (_nodes_size < width * height) {
		DELETE_ARR(_pnode);
		_pnode = new AstarNode[_nodes_size = width * height];
	}
	auto pnode = _pnode;
	auto pdata = datas;
	for (int i = _width * _height - 1; i >= 0; --i, ++pnode, ++pdata) {
		pnode->type = static_cast<ASTAR_TYPE>(*pdata);
	}
}


bool cAstar::find(int temp_x, int temp_y, int tx, int ty, Vec2i& vec) {

	Vec2i vtemp;
	vtemp.x = temp_x;
	vtemp.y = temp_y;
	temp_x = (temp_x - tx) * 5;
	temp_y = (temp_y - ty) * 5;
	auto& node = this->att(vtemp);
	node.h = std::sqrt(temp_x * temp_x + temp_y * temp_y);
	node.g = 0;
	node.f = node.getF();
	node.opend = ASTAR_OPEN;
	this->pushBack(vtemp);

	int min_f = node.f, i, j, x, y;
	while (!this->empty()) {
		// a).寻找开启列表 中F最低的格子，我们称它为当前格
		vec = this->at(0);
		if (vec.x == tx && vec.y == ty) {
			return true;
		}
		min_f = getF(0);

		//b).把遍历得到的当前格切换到关闭列表
		this->remove1st();
		// 把该格子的标记设置为“在关闭列表中”
		this->att(vec).opend = ASTAR_CLOSE;

		for (i = -1; i < 2; ++i) {
			for (j = -1; j < 2; ++j) {
				x = vec.x + i;
				y = vec.y + j;

				if (x < 0 || y < 0 || x >= _width || y >= _height) {
					continue;
				}
				auto& node = this->att(x, y);
				if (node.type == ASTAR_WALL || node.opend == ASTAR_CLOSE) {
					continue;
				}
				if (node.opend == ASTAR_UNKNOWN) {
					vtemp.x = x;
					vtemp.y = y;
					node.parent = vec;
					temp_x = (x - tx) * 5;
					temp_y = (y - ty) * 5;
					node.h = std::sqrt(temp_x * temp_x + temp_y * temp_y);
					node.g = this->att(vec).g + ((i == 0 || j == 0) ? 5 : 7);
					node.f = node.getF();
					node.opend = ASTAR_OPEN;
					this->pushBack(vtemp);
					continue;
				}
				if (node.opend == ASTAR_OPEN) {
					temp_x = this->att(vec).g + ((i == 0 || j == 0) ? 5 : 7);
					if (temp_x < node.g) {
						node.parent = vec;
						temp_y = node.heapi;
						node.g = temp_x;
						node.f = node.getF();
						this->update(temp_y);
					}
				}
			}
		}
	}
	return false;
}


bool cAstar::find(Astar* pobj, int dx, int dy, int tx, int ty) {
	reset();
	Vec2i v;
	if (!find(dx, dy, tx, ty, v)) {
		return false;
	}
	AstarNode node;
	node.parent = v;
	pobj->paths.clear();
	pobj->next_index = Astar::kStright;
	while (true) {
		if ((node.parent.x == k_1) && (node.parent.y == k_1)) {
			break;
		}
		dx = node.parent.x;
		dy = node.parent.y;
		pobj->paths.push_back(node.parent);
		pobj->next_index += 1;
		node.parent = this->att(dx, dy).parent;
	}
	pobj->next_index -= 1;
	if (pobj->next_index < 0) {
		return false;
	}
	pobj->setNext();
	pobj->next_index -= 1;
	return true;
}

