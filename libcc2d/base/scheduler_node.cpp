#include "scheduler_node.h"
#include "cxx_inl.h"

#define CCASSERT(_cond_, _str_)
#define CC_REPEAT_FOREVER (UINT_MAX -1)

////////////////////////////////////////////////////////////////////////// cSchedulerNode
SchedulerNode::SchedulerNode() {
    _scheduler = Scheduler::getInstance();
    _scheduler->retain();
}


SchedulerNode::~SchedulerNode() {
    unscheduleAllCallbacks();
    RELEASE_PTR(_scheduler);
}

void SchedulerNode::setScheduler(Scheduler* scheduler) {
    if (scheduler != _scheduler) {
        this->unscheduleAllCallbacks();
        if (scheduler != nullptr) {
            scheduler->retain();
        }
        RELEASE_PTR(_scheduler);
        _scheduler = scheduler;
    }
}

bool SchedulerNode::isScheduled(SEL_SCHEDULE selector) {
    return _scheduler->isScheduled(selector, this);
}

bool SchedulerNode::isScheduled(const std::string& key) {
    return _scheduler->isScheduled(key, this);
}

void SchedulerNode::scheduleUpdate() {
    if (!_isScheduled) {
        scheduleUpdateWithPriority(0);
        _isScheduled = true;
    }

}

void SchedulerNode::scheduleUpdateWithPriority(int priority) {
    _scheduler->scheduleUpdate(this, priority, !_running);
}


void SchedulerNode::unscheduleUpdate() {
    if (_isScheduled) {
        _scheduler->unscheduleUpdate(this);
        _isScheduled = false;
    }

}

void SchedulerNode::schedule(SEL_SCHEDULE selector) {
    this->schedule(selector, 0.0f, CC_REPEAT_FOREVER, 0.0f);
}

void SchedulerNode::schedule(SEL_SCHEDULE selector, float interval) {
    this->schedule(selector, interval, CC_REPEAT_FOREVER, 0.0f);
}

void SchedulerNode::schedule(SEL_SCHEDULE selector, float interval, unsigned int repeat, float delay) {
    CCASSERT(selector, "Argument must be non-nil");
    CCASSERT(interval >= 0, "Argument must be positive");

    _scheduler->schedule(selector, this, interval, repeat, delay, !_running);
}

void SchedulerNode::schedule(const ccSchedulerFunc& callback, const std::string& key) {
    _scheduler->schedule(callback, this, 0, !_running, key);
}

void SchedulerNode::schedule(const ccSchedulerFunc& callback, float interval, const std::string& key) {
    _scheduler->schedule(callback, this, interval, !_running, key);
}

void SchedulerNode::schedule(const ccSchedulerFunc& callback, float interval, unsigned int repeat, float delay, const std::string& key) {
    _scheduler->schedule(callback, this, interval, repeat, delay, !_running, key);
}

void SchedulerNode::scheduleOnce(SEL_SCHEDULE selector, float delay) {
    this->schedule(selector, 0.0f, 0, delay);
}

void SchedulerNode::scheduleOnce(const ccSchedulerFunc& callback, float delay, const std::string& key) {
    _scheduler->schedule(callback, this, 0, 0, delay, !_running, key);
}

void SchedulerNode::unschedule(SEL_SCHEDULE selector) {
    // explicit null handling
    if (selector == nullptr) {
        return;
        _scheduler->unschedule(selector, this);
    }
}

void SchedulerNode::unschedule(const std::string& key) {
    _scheduler->unschedule(key, this);
}

void SchedulerNode::unscheduleAllCallbacks() {
    _scheduler->unscheduleAllForTarget(this);
    _isScheduled = false;
}

void SchedulerNode::resume() {
    _scheduler->resumeTarget(this);
}

void SchedulerNode::pause() {
    _scheduler->pauseTarget(this);
}
