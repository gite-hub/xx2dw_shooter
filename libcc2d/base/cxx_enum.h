#pragma once

enum d8_t {
	D8_NONE = -1,
	D8_DEFAULT,
	D8_RIGHT_DOWN = D8_DEFAULT,
	D8_LEFT_DOWN,
	D8_LEFT_UP,
	D8_RIGHT_UP,
	D8_DOWN,
	D8_LEFT,
	D8_UP,
	D8_RIGHT,
};


enum PIXEL_BIT {
    PIXEL_8,
    PIXEL_16,
    PIXEL_24,
    PIXEL_32,
};



enum FONT_SIZE {
    FONT_12 = 12,
    FONT_14 = 14,
    FONT_16 = 16,
    FONT_24 = 24,
};
