#pragma once
#include "cxx_pod.h"

union Dye {
	dye_t inner;
	uchar indexs[4];

	Dye();
	Dye(dye_t dye);
	Dye(uchar v1, uchar v2, uchar v3 = 0, uchar v4 = 0);

	operator dye_t() const;

	static const Dye ZERO;
	static const int k256 = 256;
};
