#include "protocol.h"

//////////////////////////////////////////////////////////////////////////
bool DeltaProtocol::step(float dt, float delta) {
    if ((_delta += dt) < delta) {
        return false;
    }
    _delta -= delta;
    return true;
}

const float DeltaProtocol::kFrame = 0.08f;
const float DeltaProtocol::kBody = 0.1f;
//////////////////////////////////////////////////////////////////////////
void SpeedProtocol::reset() {
    _speed = 1.0f;
}

void SpeedProtocol::setSpeed(float speed) {
    _speed = speed;
}

bool SpeedProtocol::step(float dt, float delta) {
    return DeltaProtocol::step(dt * _speed, delta);
}



void VisibleProtocol::setVisible(bool visible) {
    _visible = visible;
}

bool VisibleProtocol::isVisible() const {
    return _visible;
}


//////////////////////////////////////////////////////////////////////////
void ColorProtocol::setColor(color_t color) {
    _color = color;
}

color_t ColorProtocol::getColor() const {
    return _color;
}




//////////////////////////////////////////////////////////////////////////
void StringProtocol::setString(cstr_t content) {
    _content = content;
}

void StringProtocol::setString(const char* content) {
    setString(std::string(content));
}

const std::string& StringProtocol::getString() {
    return _content;
}

