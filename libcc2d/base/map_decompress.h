#pragma once
#include "cxx_pod.h"

bool toJpegTurbo(uchar* jpeg_datas, uint size, rgb24_t* datas);

int decompressMask(void* in, void* out);

int jpegHandler(uchar* buffer, int in_size, uchar* out_datas);

bool decompressToMask(int width, int height, const char* pointer, uchar* temp_memory, uchar* decodes);
