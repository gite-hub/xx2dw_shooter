﻿#pragma once
#include <vector>

class Ref {
protected:
	virtual ~Ref() {}
public:
	void retain() { ++_referenceCount; }

	void release() { if ((--_referenceCount) == 0) delete this; }

	unsigned int getReferenceCount() const { return _referenceCount; }

	void autorelease();

protected:
	unsigned int _referenceCount = 1;
};


class AutoReleasePool {
public:
	static AutoReleasePool* getInstance();

	void add(Ref* was) { _refs.emplace_back(was); }

	void clear();

private:
	std::vector<Ref*> _refs;
};

