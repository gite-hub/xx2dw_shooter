#pragma once

struct Vec2i { int x = 0, y = 0; };
struct Vec2f { float x = 0.0f, y = 0.0f; };
struct Size { int width = 0, height = 0; };
struct Rect : public Vec2i, public Size {};


struct Vec2 : public Vec2f {

	Vec2();

	Vec2(float x, float y);

	static void  pointTo(float speed, float x, float y, float tx, float ty, float& out_x, float& out_y);

	void pointTo(float speed, float tx, float ty);

};