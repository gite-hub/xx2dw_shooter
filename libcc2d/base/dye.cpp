#include "dye.h"

Dye::Dye() {
	inner = ZERO;
}

Dye::Dye(dye_t dye) {
	inner = dye;
}

Dye::Dye(uchar v1, uchar v2, uchar v3 /*= 0*/, uchar v4 /*= 0*/) {
	indexs[0] = v1;
	indexs[1] = v2;
	indexs[2] = v3;
	indexs[3] = v4;
}

Dye::operator dye_t() const {
	return inner;
}

const Dye Dye::ZERO(k0u);
