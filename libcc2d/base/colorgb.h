#pragma once
#include "cxx_pod.h"

union COLOR {
	color_t inner;
	struct { uchar r, g, b, a; };

	COLOR();
	COLOR(color_t c);
	COLOR(const COLOR& c);
	COLOR(rgb16_t c);
	COLOR(uchar r, uchar g, uchar b, uchar a = k255);

	operator color_t() const;
	operator rgb32_t() const;
	operator rgb16_t() const;

	static rgb16_t to16(color_t color);
	static rgb32_t to32(color_t color);
    static rgb32_t to32(rgb16_t c565, uchar alpha5);

	static uchar rgb5to8(uchar five);
	static uchar rgb6to8(uchar six);

	static constexpr int k255 = 0xFF;
	static const COLOR BLACK;
	static const COLOR RED;
	static const COLOR GREEN;
	static const COLOR BLUE;
	static const COLOR YELLOW;
	static const COLOR WHITE;
	static const COLOR DYE;
	static const COLOR NPC;
	static const COLOR OBJECT;

};
