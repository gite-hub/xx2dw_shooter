#pragma once
#include <random>

std::mt19937& random19937();

template <typename T>
inline const T& random(const std::vector<T>& vec) { 
	return vec[std::uniform_int_distribution<int>(0, static_cast<int>(vec.size()) - 1)(random19937())]; 
}

template <typename T>
inline T random(T closed_min, T closed_max) { 
	return std::uniform_int_distribution<T>(closed_min, closed_max)(random19937());
}

template <typename T>
inline T rande(T closed_min, T closed_max) { 
	return static_cast<T>(std::uniform_int_distribution<int>(closed_min, closed_max)(random19937()));
}


inline int rand99() { return std::uniform_int_distribution<int>(0, 99)(random19937()); }

inline bool rand01() { return std::bernoulli_distribution()(random19937()); }
