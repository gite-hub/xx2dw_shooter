#pragma once

class ReferenceCount {
protected:
    virtual ~ReferenceCount();
public:
    void retain();

    virtual void release();

    unsigned int getReferenceCount() const;

protected:
	unsigned int _referenceCount = 1;
};
