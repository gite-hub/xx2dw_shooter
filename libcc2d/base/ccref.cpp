#include "ccref.h"

void Ref::autorelease() {
	AutoReleasePool::getInstance()->add(this);
}


////////////////////////////////////////////////////////////////////////// AutoReleasePool
AutoReleasePool* AutoReleasePool::getInstance() {
	static AutoReleasePool* s_autoReleasePool = new AutoReleasePool();
	return s_autoReleasePool;
}


void AutoReleasePool::clear() {
	decltype(_refs) releasings;
	releasings.swap(_refs);
	for (const auto& was : releasings) {
		was->release();
	}
}
