#pragma once

namespace cc2d {
	extern void* const g_memory;
	static constexpr int kMemoryBytesCount = 2048 * 2048 * 4;

	template<typename T>
	T* news(int unit_count) {
		if (unit_count * sizeof(T) > kMemoryBytesCount) {
			return new T[unit_count];
		}
		return (T*)g_memory;
	}

	template<typename T>
	void deletes(T* &ptr) {
		if (ptr != g_memory) {
			delete[] ptr;
			ptr = nullptr;
		}
	}
}
