#include "vec2.h"
#include <cmath>


Vec2::Vec2() {
	x = 0.0f;
	y = 0.0f;
}

Vec2::Vec2(float x, float y) {
	this->x = x;
	this->y = y;
}

void Vec2::pointTo(float speed, float x, float y, float tx, float ty, float& out_x, float& out_y) {
	float angle = std::atan2(ty - y, tx - x);
	out_y = y + speed * std::sin(angle);
	out_x = x + speed * std::cos(angle);
}

void Vec2::pointTo(float speed, float tx, float ty) {
	pointTo(speed, x, y, tx, ty, x, y);
}
