#pragma once
#include <string>

using uchar = unsigned char;
using ushort = unsigned short;
using uint = unsigned int;
using cstr_t = const std::string&;


static constexpr int k_1 = -1;
static constexpr float k0f = 1e-3f;


static const std::string k_str;

