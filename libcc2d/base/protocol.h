#pragma once
#include "colorgb.h"

class DeltaProtocol {
public:
    bool step(float dt, float delta);
protected:
    float _delta = 0;
public:
    static const float kFrame;
    static const float kBody;
};


class SpeedProtocol : public DeltaProtocol {
public:
    void reset();

    void setSpeed(float speed);

    bool step(float dt, float delta);

protected:
    float _speed = 1.0f;
};


class VisibleProtocol {
public:
    virtual void setVisible(bool visible);
    bool isVisible() const;
protected:
    bool _visible = true;
};


class ColorProtocol {
public:
    virtual void setColor(color_t color);
    color_t getColor() const;
protected:
    color_t _color = COLOR::WHITE;
};


class StringProtocol {
public:
    virtual void setString(cstr_t content);
    void setString(const char* content);
    template<typename T>
    void setString(T i) { return setString(std::to_string(i)); }
    const std::string& getString();
protected:
    std::string _content;
};
