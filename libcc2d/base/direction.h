#pragma once
#include "cxx_enum.h"

namespace Direction {

    void normalize(d8_t direction, int& x, int& y);

	d8_t reverse(d8_t direction);

	d8_t rotate(bool slant, d8_t direction, bool clockwise);

	d8_t vector(d8_t direction, float x, float y);

	d8_t vector(int dx, int dy);
};
