﻿#include "gl_manager.h"
#include "gles_inc.h"
#include "engine_extern.h"
#include "logging.h"

GLManager gl_manager;
const gluid_t GLManager::kInvalid = -1;
//////////////////////////////////////////////////////////////////////////
GLManager::GLManager() {
    _vertex_shaders.fill(kInvalid);
    _fragment_shaders.fill(kInvalid);

    _program = kInvalid;
    _texture_fbo = kInvalid;

    _uniform_texture = kInvalid;
    _uniform_texture_dye_for_fbo = kInvalid;

    _attri_xyuv0 = kInvalid;
    _attri_xyuv1 = kInvalid;
    _attri_xyuv2 = kInvalid;
    _attri_xyuv3 = kInvalid;
    _attri_rgba = kInvalid;

    _vao = kInvalid;
    _vbo = kInvalid;
	_fbo = kInvalid;
	_vao_for_fbo = kInvalid;
	_vbo_for_fbo = kInvalid;

    _vertex_shader_type = VERTEX_SHADER_COUNT;
	_fragment_shader_type = SHADER_COUNT;
	_shader_type = SHADER_COUNT;

	_binding_vao = kInvalid;
	_binding_vbo = kInvalid;

    _binding_texture = kInvalid;
    // _binding_texture_dye_for_fbo = kInvalid;

	_binding_texture_width = 32;
	_binding_texture_height = _binding_texture_width;

	_premultiply_alpha = false;

    _bath_count = 0;


}

void GLManager::initGL() {

    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBlendEquation(GL_FUNC_ADD);

    // glEnable(GL_TEXTURE_1D);
}

void GLManager::premultiplyAlpha(bool premultiply) {
	if (premultiply != _premultiply_alpha) {
		_premultiply_alpha = premultiply;
		glBlendFunc(premultiply ? GL_ONE : GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBlendEquation(GL_FUNC_ADD);
	}
}

gluid_t GLManager::loadShader(GLenum type, const char* content) {
    auto shader = glCreateShader(type);
    glShaderSource(shader, 1, &content, nullptr);
    glCompileShader(shader);
    return shader;
}

void GLManager::initShaders() {
    // vec2(_rect.x + _0_1.x * _rect.z, _rect.y + _rect.w - _0_1.y * _rect.w) / vec2(textureSize(_texture, 0))
    auto content = R"(
#version 320 es
layout(location = 0) in vec4 _xyuv0;
layout(location = 1) in vec4 _xyuv1;
layout(location = 2) in vec4 _xyuv2;
layout(location = 3) in vec4 _xyuv3;
layout(location = 4) in vec4 _rgba;
out vec2 texture_coord;
out vec4 _color;
void main() {
    vec4 xyuv[4] = vec4[4](_xyuv0, _xyuv1, _xyuv2, _xyuv3);
    _color = xyuv[gl_VertexID];
    gl_Position = vec4(_color.xy, 0, 1);
    texture_coord = _color.zw;
    _color = _rgba;
})";
    auto shader = loadShader(GL_VERTEX_SHADER, content);
    _vertex_shaders[VERTEX_SHADER_DEFAULT] = shader;
    CC_LOG("gl_load_vertex_shader_default %d", shader);

	content = R"(
#version 320 es
layout(location = 0) in vec4 _xyuv;
out vec2 texture_coord;
void main() {
    gl_Position = vec4(_xyuv.xy, 0, 1);
    texture_coord = _xyuv.zw;
})";
	shader = loadShader(GL_VERTEX_SHADER, content);
	_vertex_shaders[VERTEX_SHADER_FBO] = shader;
	CC_LOG("gl_load_vertex_shader_fbo %d", shader);

    // 浮点数精度相关: highp mediump 必须写不然android不显示
    // texture(_texture, texture_coord / vec2(textureSize(_texture, 0)));
    // vec4( (c.r + 0.00001f) * vColorplus, (c.g + 0.00001f) * vColorplus, (c.b + 0.00001f) * vColorplus, c.a );
    content = R"(
#version 320 es
precision mediump float;
layout(location = 0) uniform sampler2D _texture;
in vec2 texture_coord;
in vec4 _color;
out vec4 out_color;
void main() {
    vec4 c = texture(_texture, texture_coord);
    out_color = _color * c;
})";

    shader = loadShader(GL_FRAGMENT_SHADER, content);
    _fragment_shaders[SHADER_DEFAULT] = shader;
    CC_LOG("gl_lload_fragment_shader_default %d", shader);

	content = R"(
#version 320 es
precision highp float;
layout(location = 0) uniform sampler2D _texture;
layout(location = 1) uniform sampler2D _texture_dye;
in vec2 texture_coord;
out vec4 out_color;
void main() {
    vec4 ra = texture(_texture, texture_coord);
    vec4 c = texture(_texture_dye, vec2(ra.r, 0));
    out_color = vec4(c.r, c.g, c.b, ra.a);
})";

	shader = loadShader(GL_FRAGMENT_SHADER, content);
	_fragment_shaders[SHADER_DYE_FBO] = shader;
	CC_LOG("gl_lload_fragment_shader_dye_in_fbo %d", shader);

	// layout(location = 1) uniform sampler2D _texture_dye;
	// vec4 c = texture(_texture_dye, vec2(ra.r, 0));
	// out_color = vec4(c.r + _color.r, c.g + _color.g, c.b + _color.b, ra.a * _color.a)
    // texelFetch(_texture_dye, ivec2(ra.r * 255, 0), 0);
    content = R"(
#version 320 es
precision mediump float;
layout(location = 0) uniform sampler2D _texture;
in vec2 texture_coord;
in vec4 _color;
out vec4 out_color;
void main() {
    vec4 c = texture(_texture, texture_coord);
    out_color = vec4(c.r + (_color.r - 0.5), c.g + (_color.g - 0.5), c.b + (_color.b - 0.5), c.a * _color.a);
})";

    shader = loadShader(GL_FRAGMENT_SHADER, content);
    _fragment_shaders[SHADER_DYE] = shader;
    CC_LOG("gl_lload_fragment_shader_dye %d", shader);

    // R8的A是255
    content = R"(
#version 320 es
precision mediump float;
layout(location = 0) uniform sampler2D _texture;
in vec2 texture_coord;
in vec4 _color;
out vec4 out_color;
void main() {
    vec4 c = texture(_texture, texture_coord);
    out_color = vec4(_color.r, _color.g, _color.b, c.a);
})";

    shader = loadShader(GL_FRAGMENT_SHADER, content);
    _fragment_shaders[SHADER_A8] = shader;
    CC_LOG("gl_lload_fragment_shader_A8 %d", shader);
}


void GLManager::initPrograms() {
	_program = glCreateProgram();
	glAttachShader(_program, _vertex_shaders[VERTEX_SHADER_DEFAULT]);
	CC_LOG("glCreateProgram %d", _program);
#if 0
    auto program = new GLProgram(SHADER_DEFAULT);
    program->init(_vertex_shaders[VERTEX_SHADER_DEFAULT], _fragment_shaders[SHADER_DEFAULT]);
    _programs[SHADER_DEFAULT] = program;

	program = new GLProgram(SHADER_DYE);
	program->init(_vertex_shaders[VERTEX_SHADER_DEFAULT], _fragment_shaders[SHADER_DYE]);
	_programs[SHADER_DYE] = program;

	program = new GLProgram(SHADER_A8);
	program->init(_vertex_shaders[VERTEX_SHADER_DEFAULT], _fragment_shaders[SHADER_A8]);
	_programs[SHADER_A8] = program;
#endif
}


void GLManager::initUniformAndAttri() {
#
	_uniform_texture = 0; // glGetUniformLocation(_program, "_texture");
	_uniform_texture_dye_for_fbo = 1; // glGetUniformLocation(_program, "_texture_dye");
	// CC_LOG("gl_GetUniformLocation %d %d", _uniform_texture, _uniform_texture_dye);
	// 	少写一个逗号,找半天
// 	if (auto e = glGetError(); e != GL_NO_ERROR) {
// 		CC_LOG("glGetError() == %d\n", e);
// 	}
	_attri_xyuv0 = 0; // glGetAttribLocation(_program, "_xyuv0");
	_attri_xyuv1 = 1; // glGetAttribLocation(_program, "_xyuv1");
	_attri_xyuv2 = 2; // glGetAttribLocation(_program, "_xyuv2");
	_attri_xyuv3 = 3; // glGetAttribLocation(_program, "_xyuv3");
	_attri_rgba = 4; // glGetAttribLocation(_program, "_rgba");
	// 	CC_LOG("gl GetAttribLocation xyuv4? %d %d %d %d %d", _attri_xyuv0, _attri_xyuv1, _attri_xyuv2, _attri_xyuv3, _attri_rgba);
	if (auto e = glGetError(); e != GL_NO_ERROR) {
		CC_LOG("glGetError() == %d\n", e);
	}
}

void GLManager::initVAO() {
	glGenVertexArrays(1, &_vao);
	// 绑定/选择 VAO
	glBindVertexArray(_vao);
	CC_LOG("glBindVertexArray %d", _vao);
	glGenBuffers(1, &_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, _vbo);

	gluid_t attri_xyuv[] = { _attri_xyuv0, _attri_xyuv1, _attri_xyuv2, _attri_xyuv3 };
	for (int i = 0; i < kGLQuad4; i++) {
		glVertexAttribPointer(attri_xyuv[i], 4, GL_FLOAT, GL_FALSE, sizeof(VertexInstance), (void*)(offsetof(VertexInstance, xyuv) + i * sizeof(VertexXYUV)));
		glVertexAttribDivisor(attri_xyuv[i], 1);
		glEnableVertexAttribArray(attri_xyuv[i]);
	}
	CC_LOG("gl_vbo_xyuv4");

	glVertexAttribPointer(_attri_rgba, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexInstance), (GLvoid*)offsetof(VertexInstance, rgba));
	glVertexAttribDivisor(_attri_rgba, 1);
	glEnableVertexAttribArray(_attri_rgba);
	CC_LOG("gl_vbo_rgba");

	// 先绑定VAO，然后绑定VBO和EBO，再链接顶点属性指针即可。
	// 解绑顺序是：VBO(注意要先链接顶点属性指针)、VAO、EBO
	glBindBuffer(GL_ARRAY_BUFFER, (GLint)0);
	glBindVertexArray(0);


	////////////////////////////////////////////////////////////////////////// vao for fbo
	glGenVertexArrays(1, &_vao_for_fbo);
	// 绑定/选择 VAO
	glBindVertexArray(_vao_for_fbo);
	CC_LOG("glBindVertexArray for fbo %d", _vao_for_fbo);

	glGenBuffers(1, &_vbo_for_fbo);
	glBindBuffer(GL_ARRAY_BUFFER, _vbo_for_fbo);
	const float e = 0.5f;
	VertexXYUV xyuv4[4];
	xyuv4[0] = { -e, e, 0, 0 };
	xyuv4[1] = { -e, -e, 0, 1 };
	xyuv4[2] = { e, e, 1, 0, };
	xyuv4[3] = { e, -e, 1, 1 };
	// GL_STATIC_DRAW：数据指定一次，并多次被用于绘制。
	// GL_STREAM_DRAW：数据指定一次，最多几次用于绘制。
	// GL_DYNAMIC_DRAW：数组多次指定，多次用于绘制。
	glBufferData(GL_ARRAY_BUFFER, sizeof(xyuv4), xyuv4, GL_STATIC_DRAW);
	// 告知VAO该如何解释这个VBO的信息
	glVertexAttribPointer(_attri_xyuv0, 4, GL_FLOAT, GL_FALSE, sizeof(xyuv4[0]), 0);
	// 增量因子(每隔?个实例更新一次属性)
	// glVertexAttribDivisor
	// 允许顶点着色器读取
	glEnableVertexAttribArray(_attri_xyuv0);
	CC_LOG("gl_vbo_xyuv_for_fbo %d", _attri_xyuv0);

	glBindBuffer(GL_ARRAY_BUFFER, (GLint)0);
	glBindVertexArray(0);
}


void GLManager::initFBO() {
	glGenFramebuffers(1, &_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, _fbo);

	// 创建并绑定纹理
	glGenTextures(1, &_texture_fbo);
	glBindTexture(GL_TEXTURE_2D, _texture_fbo);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _binding_texture_width, _binding_texture_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _texture_fbo, 0);

	// 检查FBO是否完整
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		CC_LOG("Framebuffer not complete!");
	} else {
		CC_LOG("gl_GenFramebuffers %d", _fbo);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


bool GLManager::linkShader(SHADER_TYPE shader_type) {

	VERTEX_SHADER_TYPE vertex_shader_type = VERTEX_SHADER_DEFAULT;
	SHADER_TYPE fragment_shader_type = SHADER_DEFAULT;
	switch (shader_type) {
	case SHADER_DYE_FBO:
		vertex_shader_type = VERTEX_SHADER_FBO;
		fragment_shader_type = SHADER_DYE_FBO;
		break;
	case SHADER_DYE:
		fragment_shader_type = SHADER_DYE;
		break;
	case SHADER_A8:
		fragment_shader_type = SHADER_A8;
		break;
	case SHADER_COUNT:
	case SHADER_DEFAULT:
	default:
		break;
	}

	bool changed = false;
	if (_vertex_shader_type != vertex_shader_type) {
		changed = true;
		if (_vertex_shader_type != VERTEX_SHADER_COUNT) {
			glDetachShader(_program, _vertex_shaders[_vertex_shader_type]);
		}
		glAttachShader(_program, _vertex_shaders[vertex_shader_type]);
		_vertex_shader_type = vertex_shader_type;
	}
	if (_fragment_shader_type != fragment_shader_type) {
		changed = true;
		if (_fragment_shader_type != SHADER_COUNT) {
			glDetachShader(_program, _fragment_shaders[_fragment_shader_type]);
		}
		glAttachShader(_program, _fragment_shaders[fragment_shader_type]);
		_fragment_shader_type = fragment_shader_type;
	}
	if (changed) {
		// Vec2 忘了联合体
		// RGBA 倒反天罡
		// linkShader();
		// initUniformAndAttri();
		// initVAO();
		// 
		// 不解绑就一直连体(不行，合批会崩溃)
		// glBindVertexArray(_vao);
		// glBindBuffer(GL_ARRAY_BUFFER, vbo);
		// 要重新映射数据
		glLinkProgram(_program);
		glUseProgram(_program);
	}
	// CC_LOG("gl_linkShader %d %d %d", _program, vertex_shader, fragment_shader);
	return changed;
}


bool GLManager::bindVAO(gluid_t vao) {
	if (vao != _binding_vao) {
		glBindVertexArray(vao);
		_binding_vao = vao;
		_binding_vbo = kInvalid;
		return true;
	}
	return false;
}

bool GLManager::bindVBO(gluid_t vbo) {
	if (vbo != _binding_vbo) {
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		_binding_vbo = vbo;
		return true;
	}
	return false;
}

bool GLManager::activeTexture0(gluid_t texture) {
	glActiveTexture(GL_TEXTURE0);
	if (texture != _binding_texture) {
		glBindTexture(GL_TEXTURE_2D, texture);
		glUniform1i(_uniform_texture, 0);
		_binding_texture = texture;
		return true;
	}
	return false;
}

bool GLManager::activeTexture1(gluid_t texture) {
	glActiveTexture(GL_TEXTURE1);
	if (texture != _binding_texture/*_dye_for_fbo*/) {
		glBindTexture(GL_TEXTURE_2D, texture);
		glUniform1i(_uniform_texture_dye_for_fbo, 0);
		_binding_texture/*_dye_for_fbo*/ = texture;
		return true;
	}
	return false;
}

void GLManager::render(SHADER_TYPE shader_type, const VertexInstance& instance, gluid_t texture, int width, int height, gluid_t texture_dye) {
    
    if (texture != _binding_texture || 
		/*texture_dye != _binding_texture_dye_for_fbo || */
		_shader_type != shader_type || 
		_bath_count == _baths.size() - 1) {
        render();
    }
	_use_fbo_this_frame = (shader_type == SHADER_DYE && texture != kInvalid && texture_dye != kInvalid);
	if (_use_fbo_this_frame) {
		if (_binding_texture_width != width || _binding_texture_height != height) {
			glBindTexture(GL_TEXTURE_2D, _texture_fbo);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
			_binding_texture_width = width;
			_binding_texture_height = height;
			glBindTexture(GL_TEXTURE_2D, (_binding_texture == kInvalid) ? 0 : _binding_texture);
		}
		linkShader(SHADER_DYE_FBO);
		bindVAO(_vao_for_fbo);
		activeTexture0(texture);
		activeTexture1(texture_dye);
		glViewport(0, 0, width, height);
		glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
		glClear(GL_COLOR_BUFFER_BIT);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, kGLQuad4);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, g_pixel_width, g_pixel_height);
		_use_fbo_this_frame = true;
	}
	if (shader_type == SHADER_DYE && !_use_fbo_this_frame) {
		shader_type = SHADER_DEFAULT;
	}
	linkShader(shader_type);
	_shader_type = shader_type;
	bindVAO(_vao);
	activeTexture0(/*_use_fbo_this_frame ? _texture_fbo : */texture);
	memcpy(&_baths[_bath_count], &instance, sizeof(VertexInstance));
	_bath_count += 1;

}


void GLManager::render() {
	if (_bath_count == 0 || _binding_texture == kInvalid) {
		return;
	}
	bindVBO(_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexInstance) * _bath_count, &_baths[0], GL_STATIC_DRAW);


	// 当vertex buffer中的顶点会被多个图元频繁使用时，DrawElements比DrawArrays更为高效
	glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, kGLQuad4, _bath_count);

	// GLuint indexes[] = {0, 1, 2, 3};
	// 或者
	// glDrawElementsInstanced(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_INT, indexes, 4);
	// 或者
	// glDrawElements的最后一个论点可以是两件不同的事情：
	// 如果没有绑定到 GL_ELEMENT_ARRAY_BUFFER 的索引缓冲区，则它是指向存储索引的位置的指针。
	// 如果有绑定到 GL_ELEMENT_ARRAY_BUFFER 的索引缓冲区，则该数组是一个以字节为单位的偏移量()。
	// glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_INT, indexes);

	// 通过一个额外的参数GLint baseInstance指定按照baseInstace作为偏移从buffer中取出instance属性的数据
	// glDrawArraysBaseInstance

	// 扩展DrawElements，当根据indices作为索引从vertex buffer中读取数据时，有时希望允许一定数目顶点的偏移
	// 例如当vertex buffer中存着动画的多帧，需要按照一定的偏移取每一帧的数据进行渲染。
	// 该偏移通过一个额外的参数GLint basevertex 指定
	// glDrawElementsBaseVertex(mode, count, type, indicesPtr, baseVertex)

    // _drawing_program = nullptr;
	// _binding_texture = GLManager::kInvalid;
	// _binding_texture_dye = GLManager::kInvalid;
	_bath_count = 0;
}


gluid_t* GLManager::generateTextures(int count) {
	auto texture = new gluid_t[count];
	memset(texture, kInvalid, count);
	glGenTextures(count, texture);
	return texture;
}

gluid_t GLManager::generateTexture() {
	gluid_t texture = GLManager::kInvalid;
	glGenTextures(1, &texture);
	CC_LOG("glGenTextures  %d", texture);
	return texture;
}


void GLManager::toTexture(gluid_t texture, BITMAP_TYPE bitmap_type, int pitched_width, int height, const void* datas) {
	int byte;
	unsigned int inner_fmt, fmt;
	switch (bitmap_type) {
	case BITMAP_8:
		inner_fmt = GL_ALPHA;
		fmt = GL_ALPHA;
		byte = 1;
		CC_LOG("gl toTexture BITMAP_8");
		break;
	case BITMAP_16:
		// GL_RG8UIGL_RG_INTEGER 搞不定
		inner_fmt = GL_LUMINANCE_ALPHA;
		fmt = GL_LUMINANCE_ALPHA;
		byte = sizeof(rgb16_t);
		CC_LOG("gl toTexture BITMAP_88");
		break;
#if 0
	case BITMAP_565:
		inner_fmt = GL_RGB565;
		fmt = GL_RGB;
		byte = sizeof(rgb16_t);
		CC_LOG("gl toTexture BITMAP_565");
		break;
#endif
	case BITMAP_24:
		inner_fmt = GL_RGB8;
		fmt = GL_RGB;
		byte = 3;
		CC_LOG("gl toTexture BITMAP_888");
		break;
	case BITMAP_32:
	default:
		inner_fmt = GL_RGBA8;
		fmt = GL_RGBA;
		byte = sizeof(rgb32_t);
		CC_LOG("gl toTexture BITMAP_8888");
		break;
	}
	CC_LOG("GL_TEXTURE_2D %d", texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	CC_LOG("glBindTexture");
	// GL_UNSIGNED_SHORT_5_6_5
	glTexImage2D(GL_TEXTURE_2D, 0, inner_fmt, pitched_width, height, 0, fmt, GL_UNSIGNED_BYTE, datas);
	CC_LOG("glTexImage2D  %d  %d", pitched_width, height);
	// 去掉的话 字体A8全1 R8全0
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
#if 0
	// 一维纹理安卓闪退
	CC_LOG("GL_TEXTURE_1D %d", texture);
	glBindTexture(GL_TEXTURE_1D, texture);
	CC_LOG("glBindTexture");
	// internalformat 指定纹理中颜色分量的数量。 
	// 必须为 1、2、3 或 4 或以下符号常量之一：
	// GL_ALPHA、GL_ALPHA4、GL_ALPHA8、GL_ALPHA12、GL_ALPHA16、
	// GL_LUMINANCE、GL_LUMINANCE4、GL_LUMINANCE8、GL_LUMINANCE12、GL_LUMINANCE16、
	// GL_LUMINANCE_ALPHA、GL_LUMINANCE4_ALPHA4、GL_LUMINANCE6_ALPHA2、GL_LUMINANCE8_ALPHA8、
	// GL_LUMINANCE12_ALPHA4、GL_LUMINANCE12_ALPHA12、GL_LUMINANCE16_ALPHA16、
	// GL_INTENSITY、GL_INTENSITY4、GL_INTENSITY8、GL_INTENSITY12、GL_INTENSITY16、
	// GL_RGB、GL_R3_G3_B2、GL_RGB4、GL_RGB5、GL_RGB8、GL_RGB10、GL_RGB12、GL_RGB16、
	// GL_RGBA、GL_RGBA2、GL_RGBA4、GL_RGB5_A1、GL_RGBA8、GL_RGB10_A2、GL_RGBA12 或 GL_RGBA16
	// border 边框的宽度。 必须为 0 或 1。
	glTexImage1D(GL_TEXTURE_1D, 0, inner_fmt, width, 0, fmt, GL_UNSIGNED_BYTE, datas);
	CC_LOG("glTexImage1D  %d  %d", width, height);
	glGenerateMipmap(GL_TEXTURE_1D);
#endif
}


void GLManager::deleteTexture(gluid_t texture) {
	glDeleteTextures(1, &texture);
}


void GLManager::deleteTexture(int count, gluid_t* texture) {
	glDeleteTextures(count, texture);
}



