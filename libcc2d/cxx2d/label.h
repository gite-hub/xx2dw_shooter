#pragma once
#include "font_manager.h"
#include "protocol.h"

class LabelProtocol {
public:
    // before setString
    void setFont(FONT_SIZE font_size);
    // before setString
    void lock(int lock);

    void setShadow(bool shadow);
protected:
    Font* _font = nullptr;
    int _lock = Font::kLock;
    bool _shadow = false;
};


class Label : public LabelProtocol, public StringProtocol, public VisibleProtocol, public ColorProtocol, public Size {
public:
    Label();

    void setAnchorCenter(bool center);

    void setString(cstr_t content) override;

    void render(float world_x, float world_y);

protected:
    bool _anchor_center = false;
    Font::Layout _layout;
};
