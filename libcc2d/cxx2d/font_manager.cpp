#include "font_manager.h"
#include "cxx_inl.h"
#include "cxx_func.h"
#include "memory2d.h"
#include "string_utf8.h"
#include "texture_impl.h"
#include "extern2d.h"
#include "hfont.h"

FontManager g_font;
const int Font::kLock = 0;


Font::Font() {

}

Font::~Font() {
    if (_texture != kInvalidTexture) {
        TextureImpl::free(_texture);
    }
}


void Font::init(int font_size, int texture_size) {
    _font_size = font_size;
    _refer_size = font_size;
    _texture_size = texture_size;
    _cels_count = 0;
    auto ptr = cc2d::news<Pixel>(texture_size * texture_size);
    memset(ptr, 0, _texture_size * _texture_size * sizeof(*ptr));
    _texture = TextureImpl::create(PIXEL_8, texture_size, texture_size, ptr);
    cc2d::deletes(ptr);
    std::string content = "0123456789,.?!\"";
    for (const auto& c : content) {
        auto datas = emplace(c, _rect);
        _cache.Rect::operator=(_rect);
        _cache.datas = datas;
        _caches.emplace_back(_cache);
    }
}

const Font::Pixel* Font::emplace(word_view word, Rect& rect) {
    const auto& it = _words.find(word);
    if (it != _words.end()) {
        rect = it->second;
        return nullptr;
    }
    int width, height;
    auto ptr = cc2d::news<Pixel>(_font_size * _font_size * 25);
    cc2d::toA8bitmap(_font_size, StringUFT8::conver(word).c_str(), width, height, ptr);
    // cc2d::toA8bitmap(_font_size, word, width, height, ptr);
    auto datas = new Pixel[width * height];
    memcpy(datas, ptr, width * height * sizeof(*datas));
    cc2d::deletes(ptr);

    if (_cels_count == 0) {
        _refer_size = std::max(width, height);
        _cels_count = _texture_size / _refer_size;
    }
    rect.x = (_temp_index % _cels_count) * _refer_size;
    rect.y = (_temp_index / _cels_count) * _refer_size;
    rect.width = width;
    rect.height = height;
    _words.emplace(word, rect);
    _temp_index += 1;
    return datas;
}


void Font::pushCache(const Rect& rect, const uchar* ptr) {
    _cache.Rect::operator=(rect);
    _cache.datas = ptr;
    _caches.emplace_back(_cache);
}


void Font::correctLine(std::vector<Rect*>& line_layout) {
    return;
    for (auto pword : line_layout) {
        // 无语，老会访问权限冲突
        pword->y += _refer_size - pword->height;
    }
}

void Font::toLayoutNewLine(int& x, int& height, std::vector<Rect*>& line_layout) {
    x = 0;
    height += _refer_size + 1;
    correctLine(line_layout);
    line_layout.clear();
}


void Font::toLayout(cstr_t content, int lock, Layout& layout, int& width, int& height) {

    StringUFT8 str8 = content;
    width = 0;
    height = 0;
    word_t word;
    WordLayout word_layout;
    std::vector<Rect*> line_layout;


//     std::u32string str_utf32;
//     Charset::UTF8ToUTF32(":*¨༺ ☽.𝐊𝗂𝗋α.☾༻¨*:·", str_utf32);
//     std::u16string str_utf16;
//     Charset::UTF32ToUTF16(str_utf32, str_utf16);

    for (int n = 0, x = 0; n < str8.length(); n += 1) {
        word = str8[n];
        word_layout.word = word;
        if (word == ' ') {
            x += _refer_size / 2;
        } else if (word == '\n' || word == '\r\n') {
            toLayoutNewLine(x, height, line_layout);
        } else {
            auto datas = emplace(word, _rect);
            if (datas != nullptr){
                pushCache(_rect, datas);
            }
            word_layout.Size::operator=(_rect);
            if (lock != Font::kLock && (x + word_layout.width) > lock) {
                toLayoutNewLine(x, height, line_layout);
            }
            word_layout.x = x;
            word_layout.y = height;
            layout.emplace_back(word_layout);
            line_layout.emplace_back(&layout.back());

            x += word_layout.width;
        }
        width = std::max(width, x);
    }
    correctLine(line_layout);
    if (height == 0) {
        height = _refer_size;
    } else {
        height -= 1;
    }
}


void Font::updateCachesToTexture() {
    if (_caches.empty()) {
        return;
    }
    for (int begin = 0, end = 1, width, pitch, over = 0, i, k; over == 0;) {
        if (_caches.size() == 1) {
            over = 1;
        } else {
            while (_caches[end].y == _caches[begin].y) {
                end += 1;
                if (end == _caches.size()) {
                    over = 1;
                    break;
                }
            }
        }
        width = (end - begin) * _refer_size;
        pitch = cc2d::pitch<Pixel>(width);
        auto _pdata = cc2d::news<Pixel>(pitch * _refer_size);
        memset(_pdata, 0, pitch * _refer_size * sizeof(*_pdata));
        for (i = begin; i < end; i += 1) {
            auto& cache = _caches[i];
            for (k = 0; k < cache.height; k += 1) {
                memcpy((Pixel*)_pdata + k * pitch + (i - begin) * _refer_size,
                    cache.datas + k * cache.width, cache.width * sizeof(*cache.datas));
            }
            delete[] cache.datas;
        }
        const auto& cache = _caches[begin];
        TextureImpl::update(_texture, PIXEL_8, cache.x, cache.y, width, _refer_size, _pdata);
        cc2d::deletes(_pdata);
        begin = end;
    }
    _caches.clear();
}


void Font::render(float world_x, float world_y, const Layout& layout, color_t color) {
    updateCachesToTexture();
    for (const auto& lay : layout) {
        render(world_x, world_y, lay, color);
    }
}


void Font::render(float world_x, float world_y, const WordLayout& word_layout, color_t color) {
    const auto& rect = _words.at(word_layout.word);
    TextureImpl::render(_texture, SHADER_A8, color,
        world_x + word_layout.x, world_y + word_layout.y, _texture_size, _texture_size,
        rect.x, rect.y, rect.width, rect.height, rect.width, rect.height);
}






//////////////////////////////////////////////////////////////////////////
Font* FontManager::getFont(FONT_SIZE font_size) {

    const auto& it = _fonts.find(font_size);
    if (it != _fonts.end()) {
        return &it->second;
    }
    auto pfnt = &_fonts[font_size];

    pfnt->init(
        font_size + font_size * (g_height - kMinSolutionHeight) * 2 * 2 / kMinSolutionHeight,
        font_size == FONT_12 ? 1024 : 2048);
    return pfnt;
}
