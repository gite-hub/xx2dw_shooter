#include "wdfp.h"
#include "iofile.h"

WDFP::WDFP() {

}

WDFP::~WDFP() {
	DELETE_ARR(_pointer);
	DELETE_ARR(_pindex);
}


static bool indexCompare(const WDFP::Index& a, const WDFP::Index& b) {
	return a.uid < b.uid;
}

bool WDFP::open(cstr_t filename, bool memory) {

	IFile ifile;
	if (!ifile.open(filename)) {
		return false;
	}
	uint temp;
	ifile >> temp;
	// 0x57444650
	if (temp != 'WDFP') {
		return false;
	}
	ifile >> _indexs_count >> temp;
	ifile.seekSet(temp);
	std::vector<Index> idxs(_indexs_count);
	for (int k = 0; k < _indexs_count; k += 1) {
		ifile >> idxs[k];
		ifile.seek(4);
	}
	std::sort(idxs.begin(), idxs.end(), indexCompare);

	_pindex = new Index[_indexs_count];
	auto pindex = const_cast<Index*>(_pindex);
	for (int k = 0; k < _indexs_count; k += 1) {
		pindex[k] = idxs[k];
	}
	if (memory) {
		int size = ifile.getSize();
		ifile.seekSet();
		_pointer = new char[size];
		ifile.read(const_cast<char*>(_pointer), size);
	}
	_filename = filename;
	return true;
}


cstr_t WDFP::getFileName() const noexcept {
	return _filename;
}

int WDFP::binarySearch(was_t uid, const Index* indexs, int indexs_count) {
	was_t begin = 0, end = indexs_count - 1, middle;
	if (uid < (indexs + begin)->uid || uid >(indexs + end)->uid) {
		return k_1;
	}
	auto current = indexs;
	while (begin <= end) {
		middle = (begin + end) >> 1;
		current = indexs + middle;
		if (current->uid == uid) {
			return middle;
		} else if (current->uid < uid) {
			begin = middle + 1;
		} else {
			end = middle - 1;
		}
	}
	return k_1;
}




const WDFP::Index* WDFP::binarySearch(was_t uid) const {
	auto i = binarySearch(uid, _pindex, _indexs_count);
	if (i == k_1) {
		return nullptr;
	}
	return _pindex + i;
}

const WDFP::Index* WDFP::getIndexs() const noexcept {
	return _pindex;
}

int WDFP::getIndexsCount() const noexcept {
	return _indexs_count;
}

const char* WDFP::getData(const Index* pindex) const {
	// *(ushort*)ptr2 != 0x5053
	if (_pointer == nullptr) {
		return IFile::read(_filename, pindex->offset, pindex->length);
	}
	return _pointer + pindex->offset;
}


const char* WDFP::getData(was_t uid, int* plength) const {

	auto pindex = binarySearch(uid);
	if (pindex == nullptr) {
		return nullptr;
	}
	if (plength != nullptr) {
		*plength = pindex->length;
	}
	return getData(pindex);
}



const char* WDFP::getData(was_t uid, int size) const {
	auto pindex = binarySearch(uid);
	if (pindex == nullptr) {
		return nullptr;
	}
	if (_pointer == nullptr) {
		return IFile::read(_filename, pindex->offset, std::min(size, (int)pindex->length));
	}
	return _pointer + pindex->offset;
}



//////////////////////////////////////////////////////////////////////////
WDFGroup* WDFGroup::getWav() {
	static WDFGroup* s_wav = new WDFGroup();
	return s_wav;
}

WDFGroup* WDFGroup::getMp3() {
	static WDFGroup* s_mp3 = new WDFGroup();
	return s_mp3;
}


WDFGroup::WDFGroup() {
	_all_memory = true;
}


bool WDFGroup::open(cstr_t filename, bool memory) {
	_wdfs.emplace_back(WDFP());
	auto wdf = &_wdfs.back();
	if (wdf->open(filename, memory)){
		if (!memory) {
			_all_memory = false;
		}
		return true;
	}
	_wdfs.pop_back();
	return false;
}


const char* WDFGroup::getData(was_t uid, int* psize /*= nullptr*/) {
	for(auto& wdf : _wdfs) {
		auto ptr = wdf.getData(uid, psize);
		if (ptr != nullptr) {
			return ptr;
		}
	}
	return nullptr;
}


bool WDFGroup::freeData(was_t uid, const char* ptr) {
	if (_all_memory) {
		return false;
	}
	for (auto wdf : _wdfs) {
		if (wdf._pointer != nullptr) {
			continue;
		}
		auto index = wdf.binarySearch(uid);
		if (index != nullptr) {
			delete[] ptr;
			return true;
		}
	}
	return false;
}


