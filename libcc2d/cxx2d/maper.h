#pragma once
#include "cxx.h"
#include "vec2.h"
#include "bitmap.h"

class MapDecode : public Rect {
public:
	~MapDecode();
	void decode(const char* ptr);
public:
	int x, y, width, height;
	uchar* pdecode = nullptr;
};



class MapAlpha : public Size {
public:
	MapAlpha();
	~MapAlpha();
	void toAlpha(const rgb24_t* p24, const MapDecode* pmap_decode, int pitched_width, rgb44_t* p44);
public:
	int mask_idx, mask_x, mask_y, block_x, block_y;
	Bitmap bitmap;
};


class MapBlock {
public:
	MapBlock();
	~MapBlock();
	void insert(int mask_idx, int bx, int by, int x, int y, int w, int h);
	bool toBitmap(const char* ptr, uchar* temp_data, rgb24_t* datas);
	void loadBitmapAndAlphas(const char* ptr, const MapDecode* pmap_decode);
public:
	std::vector<MapAlpha> alphas;
	Bitmap bitmap;
	bool loaded = false;
};


class Maper {
public:
	Maper();
	~Maper();
protected:
	void load(const char* ptr);
	bool allocMasks();
	void assignMasks();
	bool allocObstacles();
	void readObstacles();
public:
	void loadAstar();
	bool isObstacle(int x, int y);
	bool isStraight(int x, int y, int tx, int ty);
protected:
	void loadBitmapAndAlphas(int block_index);
public:
	int toDoorY(int map_y);
	int toY(int dy);
	void randCoor(int& x, int& y);

	void render(float world_x, float world_y, int x1, int x2, int y1, int y2);
	void renderAlphas(float world_x, float world_y, int x1, int x2, int y1, int y2);

public:
	int width, height;
protected:
	uint _mapid;
	int _block_width, _block_height;
	int _blocks_count, _masks_count;

	const char* _pointer;
	uint* _pblock_offset;
	uint* _pmask_offset;
	MapBlock* _pblock = nullptr;
	MapDecode* _pmaskdec = nullptr;
	uchar* _pobstacle = nullptr;

public:
	friend class MapFollow;
	friend class MaperManager;
	static const int k20;
};


class MapFollow : public Vec2 {
public:
	MapFollow();

	void setSpeed(float speed);
	void resetSpeed();
	void setSpeedPerSecond(int speed);

	void setTarget(Maper* pmap);

	void setTarget(float obj_x, float obj_y, bool right_now);

	void step(float dt);

	void apply();

protected:
	int _block_x1, _block_x2, _block_y1, _block_y2, _block_width, _block_height;
	int _speed_per_sec;
	float _follow_x1, _follow_x2, _follow_y1, _follow_y2;
	bool _hsmall, _vsmall, _following, _moving;
	float _speed_rate;
	Vec2f _target;
};


class MaperManager {
public:
	MaperManager();
	~MaperManager();

	static bool isMap(const char* ptr);

	void setRoot(const std::string& root);
	void setPaths(const std::vector<std::string>& paths);

	const char* getPointer(int mapid);

	Maper* getMaper(int mapid, bool alpha_info);

	void free(int retain_mapid);

private:
	std::string _root;
	std::vector<std::string> _paths;
	std::map<int, const char*> _pointers;
	std::map<int, Maper> _maps;
};

extern MaperManager g_maper;
