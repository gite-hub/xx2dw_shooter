#pragma once
#include "cxx_stl.h"
#include "cxx_pod.h"

class StringUFT8 {
public:
    StringUFT8();
    StringUFT8(char_utf8_t c);
    StringUFT8(const StringUFT8& str8);
    StringUFT8(cstr_t str);

    ~StringUFT8();

    using Chars = std::vector<char_utf8_t>;
    static bool conver(cstr_t str, Chars& chars);
    static char_utf8_t conver(const char* str, int length);
    static std::string conver(char_utf8_t i);
    static int getLength(char_utf8_t i);

    StringUFT8& operator=(char_utf8_t c);
    // StringUFT8& operator=(const StringUFT8& str8);
    StringUFT8& operator=(cstr_t str);

    StringUFT8& operator+=(char_utf8_t c);
    StringUFT8& operator+=(const StringUFT8& str8);
    StringUFT8& operator+=(cstr_t str);

    StringUFT8 operator+(char_utf8_t c) const;
    StringUFT8 operator+(const StringUFT8& str8) const;
    StringUFT8 operator+(cstr_t str) const;

    char_utf8_t operator[](int i) const;
    operator const std::string&() const;

    std::size_t length() const;
    void pop_back();

private:
    void replace(std::string_view newStr);

    std::string getAsCharSequence() const;
    std::string getAsCharSequence(std::size_t pos) const;
    std::string getAsCharSequence(std::size_t pos, std::size_t len) const;

    bool erase(std::size_t pos);
    bool insert(std::size_t pos, cstr_t str);
    bool insert(std::size_t pos, const StringUFT8& str8);

private:
    std::string _inner;
    Chars _chars;
};
