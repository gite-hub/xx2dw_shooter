#include "maper.h"
#include "cxx_func.h"
#include "rand19937.h"
#include "memory2d.h"
#include "map_decompress.h"
#include "extern2d.h"
#include "iofile.h"
#include "astar.h"

MaperManager g_maper;

static const int k320 = 320, k240 = 240;
static const int k12 = 12, k16 = 16, k20 = 20, k10 = 10;
////////////////////////////////////////////////////////////////////////// MapDecode
MapDecode::~MapDecode() {
	DELETE_ARR(pdecode);
}


void MapDecode::decode(const char* ptr) {
	if (pdecode == nullptr) {
		pdecode = new uchar[width * height];
        auto temp = cc2d::news<uchar>(width * height);
		decompressToMask(width, height, ptr, temp, pdecode);
        cc2d::deletes(temp);
	}
}


////////////////////////////////////////////////////////////////////////// MapAlpha
MapAlpha::MapAlpha() {

}

MapAlpha::~MapAlpha() {
}

void MapAlpha::toAlpha(const rgb24_t* p24, const MapDecode* masker, int pitched_width, rgb44_t* p44) {
	masker += mask_idx;
	memset(p44, 0, pitched_width * height * sizeof(*p44));
	auto ppng = p44;
	auto alpha = masker->pdecode + mask_y * masker->width + mask_x;
	p24 += block_y * k320 + block_x;
	for (int k = 0, i; k < height; ++k, alpha += masker->width - width, p24 += k320 - width) {
		ppng = p44 + k * pitched_width;
		for (i = 0; i < width; ++i, ++alpha, ++ppng, ++p24) {
			if (*alpha) {
				ppng->r = p24->r >> 4;
				ppng->g = p24->g >> 4;
				ppng->b = p24->b >> 4;
				ppng->a = 0x07;
			}
		}
	}
	// bitmap = TextureOperator::create(width, height, p16);
	// TextureOperator::setAlpha(pbitmap, 0x7F);
}




////////////////////////////////////////////////////////////////////////// sMapBlock
MapBlock::MapBlock() {
}

MapBlock::~MapBlock() {
}


void MapBlock::insert(int mask_idx, int bx, int by, int x, int y, int w, int h) {
	MapAlpha alpha;
	alpha.block_x = bx;
	alpha.block_y = by;
	alpha.mask_x = x;
	alpha.mask_y = y;
	alpha.width = w;
	alpha.height = h;
	alpha.mask_idx = mask_idx;
	alphas.emplace_back(alpha);
}


bool MapBlock::toBitmap(const char* ptr, uchar* temp_data, rgb24_t* datas) {
	uint size = *(uint*)ptr;
	if (size > 0) {
		ptr += (size << 2);
	}
	uint flag = *(uint*)(ptr + 4);
	size = *(uint*)(ptr + 8);
	ptr += 12;

	if (flag == 'JPEG') {
		size = jpegHandler((uchar*)ptr, size/*, size*/, temp_data);
	} else {
		temp_data = (uchar*)ptr;
	}
	return toJpegTurbo(temp_data, size, datas);
}



void MapBlock::loadBitmapAndAlphas(const char* ptr, const MapDecode* masker) {
	static auto temp8 = new rgb24_t[k320 * k240];
	auto tempjpg = cc2d::news<rgb24_t>(k320 * k240);
	toBitmap(ptr, (uchar*)tempjpg, temp8);
	cc2d::deletes(tempjpg);
	bitmap.load(PIXEL_24, k320, k240, temp8);
	int pitch;
	forv(alphas, i) {
		auto& a = alphas[i];
		pitch = cc2d::pitch<rgb44_t>(a.width);
		auto ptr = cc2d::news<rgb44_t>(pitch * a.height);
		alphas[i].toAlpha(temp8, masker, pitch, ptr);
		a.bitmap.load(PIXEL_16, a.width, a.height, ptr);
		cc2d::deletes(ptr);
	}
}

////////////////////////////////////////////////////////////////////////// sMap
const int Maper::k20 = ::k20;

Maper::Maper() {
}

Maper::~Maper() {
	DELETE_ARR(_pblock);
	DELETE_ARR(_pmaskdec);
	DELETE_ARR(_pobstacle);
}


void Maper::load(const char* ptr) {

	_pointer = ptr;
	width = *(uint*)(_pointer + 4);
	height = *(uint*)(_pointer + 8);
	_block_width = std::ceil(width * 1.0f / k320);
	_block_height = std::ceil(height * 1.0f / k240);
	_blocks_count = _block_width * _block_height;
	_pblock_offset = (uint*)(_pointer + 12);

	_pblock = new MapBlock[_blocks_count];

}

bool Maper::allocMasks() {

	auto ptr = _pointer + (12 + 4 * _blocks_count + 4);
	_masks_count = *(uint*)ptr;
	_pmask_offset = (uint*)(ptr + 4);

	if (_pmaskdec == nullptr) {
		_pmaskdec = new MapDecode[_masks_count];
		assignMasks();
		return true;
	}
	return false;
}


void Maper::assignMasks() {
	auto pmask_offset = _pmask_offset;
	auto pdec = _pmaskdec;
	int ix, iy, ix2, iy2, k, i;
	int bx, by, rx, ry, rw, rh;
	auto ptr = _pointer;
	Rect rect;
	for (int mask_i = 0; mask_i < _masks_count; mask_i += 1, pmask_offset += 1, pdec += 1) {
		ptr = _pointer + *pmask_offset;
		memcpy(&rect, ptr, sizeof(rect));
		pdec->x = rect.x;
		pdec->y = rect.y;
		pdec->width = rect.width;
		pdec->height = rect.height;

		ix = pdec->x / k320;
		iy = pdec->y / k240;
		ix2 = (pdec->x + pdec->width - 1) / k320;
		iy2 = (pdec->y + pdec->height - 1) / k240;
		if (ix == ix2 && iy == iy2) {
			// (blocks + iy * block_width + ix)->mask_indexs.emplace_back(n);
			bx = ix * k320;
			by = iy * k240;
			(_pblock + iy * _block_width + ix)->insert(mask_i, pdec->x - bx, pdec->y - by, 0, 0, pdec->width, pdec->height);
			continue;
		}
		for (k = iy; k <= iy2; ++k) {
			for (i = ix; i <= ix2; ++i) {
				bx = i * k320;
				by = k * k240;
				rx = std::max(bx, pdec->x);
				ry = std::max(by, pdec->y);
				rw = std::min(bx + k320, pdec->x + pdec->width);
				rh = std::min(by + k240, pdec->y + pdec->height);
				(_pblock + k * _block_width + i)->insert(mask_i, rx - bx, ry - by, rx - pdec->x, ry - pdec->y, rw - rx, rh - ry);
			}
		}
	}
}


bool Maper::allocObstacles() {
	if (_pobstacle == nullptr) {
		int astar_width = _block_width * k16;
		int astar_height = _block_height * k12;
		_pobstacle = new uchar[astar_width * astar_height];
		memset(_pobstacle, ASTAR_WALL, astar_width * astar_height * sizeof(*_pobstacle));
		readObstacles();
		return true;
	}
	return false;
}

void Maper::readObstacles() {
	int astar_width = _block_width * k16;

	auto pobstacle = _pobstacle;
	auto pblock_offset = _pblock_offset;
	auto ptr = _pointer;
	uint flag, size;
	for (int k = 0, i, offy; k < _block_height; ++k) {
		offy = k * k12 * astar_width;
		for (i = 0; i < _block_width; i += 1, pblock_offset += 1) {
			ptr = _pointer + (*pblock_offset);
			flag = *(uint*)ptr;
			ptr += 4;
			if (flag > 0) {
				ptr += (flag << 2);
			}
			bool loop = true;
			while (loop) {
				flag = *(uint*)ptr;
				size = *(uint*)(ptr + 4);
				ptr += 8;
				switch (flag) {
				case 0x43454C4C: // LLEC "4C 4C 45 43"
					// 默认填1
					pobstacle = _pobstacle + offy + i * k16;
					for (flag = 0; flag < size; ++flag) {
						if (*ptr == 0) {
							*(pobstacle + (flag / k16 * astar_width) + (flag % k16)) = ASTAR_ROAD;
						}
						++ptr;
					}
					break;
				case 0x494D4147:
				case 0x4D534B32: // 2KSM "32 4B 53 4D"
				case 0x4D41534B: // KSAM "4B 53 41 4D"
				case 0x4A504547: // GEPJ "47 45 50 4A"
				case 0x4A504732: // 2GPJ
				case 0x424C4F4B: // KOLB "4B 4F 4C 42"
					ptr += size;
					break;
				case 0x42524947: // GIRB "47 49 52 42"
				case 0x4C494754: // TGIL "54 47 49 4C"
					break;
				case 0x454E4420: //  DNE "20 44 4E 45"
				default:
					loop = false;
					break;
				}
			}
		}
	}
}


void Maper::loadAstar() {
	g_astar.load(_mapid, _block_width * k16, _block_height * k12, _pobstacle);
}


bool Maper::isObstacle(int x, int y) {
	if (x < 0 || y < 0 || x >= (this->width / k20) || y >= (this->height / k20)) {
		return true;
	}
	return *(_pobstacle + y * _block_width * k16 + x) == ASTAR_WALL;
}


bool Maper::isStraight(int x, int y, int tx, int ty) {
	float fx = x;
	float fy = y;
	while (std::abs(tx - fx) > k10 || std::abs(ty - fy) > k10) {
		Vec2::pointTo(k10, fx, fy, tx, ty, fx, fy);
		if (isObstacle(fx / k20, fy / k20)) {
			return false;
		}
	}
	return true;
}


void Maper::loadBitmapAndAlphas(int block_index) {
	auto pblock = _pblock + block_index;
	if (!pblock->loaded) {
		for (auto& alpha : pblock->alphas) {
			(_pmaskdec + alpha.mask_idx)->decode(_pointer + *(_pmask_offset + alpha.mask_idx));
		}
		pblock->loadBitmapAndAlphas(_pointer + *(_pblock_offset + block_index), _pmaskdec);
		pblock->loaded = true;
	}
}


int Maper::toDoorY(int map_y) {
	return (this->height - 1 - map_y) / k20;
}

int Maper::toY(int dy) {
	return  this->height - 1 - dy * k20;
}


void Maper::randCoor(int& dx, int& dy) {
	loadAstar();
	int tx, ty, nw = 0, nh = 0, n = 16;
	do {
		dx = random(nw, this->width / k20 - 1 - nw);
		dy = random(nh, this->height / k20 - 1 - nh);
		tx = random(nw, this->width / k20 - 1 - nw);
		ty = random(nh, this->height / k20 - 1 - nh);

		if (isObstacle(dx, dy) || isObstacle(tx, ty)) {
			continue;
		}
		if ((dx - tx) * (dx - tx) + (dy - ty) * (dy - ty) < n * n) {
			continue;
		}
		Vec2i v;
		if (g_astar.find(dx, dy, tx, ty, v)) {
			break;
		}
	} while (true);
	dy = this->height / k20 - 1 - dy;
}



void Maper::render(float world_x, float world_y, int x1, int x2, int y1, int y2) {

	for (int k = y1, i, index; k <= y2; ++k) {
		for (i = x1; i <= x2; ++i) {
			index = k * _block_width + i;
			loadBitmapAndAlphas(index);
			(_pblock + index)->bitmap.render(world_x + i * k320, world_y + k * k240);
		}
	}
}

void Maper::renderAlphas(float world_x, float world_y, int x1, int x2, int y1, int y2) {
	MapBlock* pblock;
	for (int k = y1, i, x, y; k <= y2; ++k) {
		for (i = x1; i <= x2; ++i) {
			// loadBitmapAndAlphas(k * _block_width + i);
			pblock = _pblock + k * _block_width + i;
			if (pblock->loaded) {
				for (const auto& alpha : pblock->alphas) {
					x = i * k320 + alpha.block_x;
					y = k * k240 + alpha.block_y;
					alpha.bitmap.render(world_x + x, world_y + y);
				}
			}
		}
	}
}



//////////////////////////////////////////////////////////////////////////
MapFollow::MapFollow() {
	resetSpeed();
}


void MapFollow::setSpeed(float speed) {
	_speed_rate = speed;
}


void MapFollow::resetSpeed() {
	_speed_rate = 0.0f;
}


void MapFollow::setSpeedPerSecond(int speed) {
	_speed_per_sec = speed;
}


void MapFollow::setTarget(Maper* pmap) {
	_block_x1 = _block_x2 = _block_y1 = _block_y2 = 0;

	_block_width = pmap->_block_width;
	_block_height = pmap->_block_height;
	_hsmall = pmap->width <= g_width;
	_vsmall = pmap->height <= g_height;

	if (_hsmall) {
		_follow_x1 = _follow_x2 = (g_width - pmap->width) / 2;
	} else {
		_follow_x1 = g_width - pmap->width;
		_follow_x2 = 0;
	}
	if (_vsmall) {
		_follow_y1 = _follow_y2 = (g_height - pmap->height) / 2;
	} else {
		_follow_y1 = g_height - pmap->height;
		_follow_y2 = 0;
	}
	_speed_rate = 0.0f;
	_following = false;
}


void MapFollow::setTarget(float obj_x, float obj_y, bool right_now) {
	if (_hsmall) {
		obj_x = _follow_x1;
	} else {
		obj_x = std::max(g_width / 2 - obj_x, _follow_x1);
		obj_x = std::min(obj_x, _follow_x2);
	}
	if (_vsmall) {
		obj_y = _follow_y1;
	} else {
		obj_y = std::max(g_height / 2 - obj_y, _follow_y1);
		obj_y = std::min(obj_y, _follow_y2);
	}
	_moving = (std::abs(obj_x - _target.x) > 1 || std::abs(obj_y - _target.y) > 1);
	_target.x = obj_x;
	_target.y = obj_y;
	if (right_now || (_vsmall && _hsmall) || (std::abs(obj_x - this->x) < 2 && std::abs(obj_y - this->y) < 2)) {
		this->x = obj_x;
		this->y = obj_y;
		_speed_rate = 0.0f;
		_following = false;
		apply();
	} else {
		_following = true;
	}
}


void MapFollow::step(float dt) {
	if (_following) {
		if (std::abs(_target.x - this->x) < 0.5f && std::abs(_target.y - this->y) < 0.5f) {
			_speed_rate = 0.0f;
			_following = false;
		} else {
			if (_moving) {
				if (_speed_rate < 1) {
					_speed_rate = std::max(0.1f, _speed_rate * 1.04f);
				}
			} else {
				_speed_rate *= 0.98f;
			}
			pointTo(std::max(1.0f, _speed_rate * _speed_per_sec * dt), _target.x, _target.y);
			if (g_height <= 400) {
				this->y = _target.y;
			}
		}
		apply();
	}
}



void MapFollow::apply() {
	_block_x1 = std::max(0, (0 - (int)this->x) / k320);
	_block_y1 = std::max(0, (0 - (int)this->y) / k240);
	_block_x2 = std::min(_block_width - 1, (int)(g_width - this->x) / k320);
	_block_y2 = std::min(_block_height - 1, (int)(g_height - this->y) / k240);
}


////////////////////////////////////////////////////////////////////////// cMaper
MaperManager::MaperManager() {
	_paths = { k_str };
}

MaperManager::~MaperManager() {

}


bool MaperManager::isMap(const char* ptr) {
	uint flag = *(uint*)ptr;
	return flag == 'MAPX' || flag == 'M1.0';
}

void MaperManager::setRoot(const std::string& root) {
	_root = root;
}

void MaperManager::setPaths(const std::vector<std::string>& paths) {
	_paths = paths;
}


const char* MaperManager::getPointer(int mapid) {
	const auto& it = _pointers.find(mapid);
	if (it != _pointers.end()) {
		return it->second;
	};

	IFile ifile;
	for (const auto& path : _paths) {
		ifile.open(_root + path + std::to_string(mapid) + ".map");
		if (ifile.isOpend()) {
			char ptr4[4];
			ifile >> ptr4;
			if (!isMap(ptr4)) {
				ifile.close();
				continue;
			}
			auto length = ifile.getSize();
			auto pointer = new char[length];
			ifile.read(pointer + 4, length - 4);
			ifile.close();
			if (length < (1 << 20) || length >(5 << 20)) {
				_pointers.emplace(mapid, pointer);
			}
			return pointer;
		}
	}
	return nullptr;
}


Maper* MaperManager::getMaper(int mapid, bool alpha_info) {
	const auto& it = _maps.find(mapid);
	if (it != _maps.end()) {
		if (alpha_info) {
			it->second.allocMasks();
		}
		return &it->second;
	};
	auto pointer = getPointer(mapid);
	if (pointer == nullptr) {
		return nullptr;
	}
	auto maper = &_maps[mapid];
	maper->_mapid = mapid;
	maper->load(pointer);
	maper->allocObstacles();
	maper->loadAstar();
	if (alpha_info) {
		maper->allocMasks();
	}
	// _maps.emplace(mapid, maper);
	return maper;
}



void MaperManager::free(int retain_mapid) {

	uint mapid;
	for (auto it = _maps.begin(); it != _maps.end();) {
		mapid = it->second._mapid;
		if (mapid == retain_mapid) {
			++it;
			continue;
		}
		if (_pointers.find(mapid) == _pointers.end()) {
			delete[] it->second._pointer;
		}
		// delete it->second;
		it = _maps.erase(it);
	}
}
