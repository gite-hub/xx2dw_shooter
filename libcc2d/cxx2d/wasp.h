#pragma once
#include "dye_manager.h"
#include "reference_count.h"
#include "texture.h"

struct FrameInfo {
	int x = 0, y = 0, width = 0, height = 0;
};

class FrameDecoder {
public:
	~FrameDecoder();

	bool isValid() const;
private:
	void decode(int width, int height, const uchar* pHead, uint offs);

	void toBitmap(int width, int height, const rgb16_t* palette, ITexture bitmap) const;

	uchar* _indexs = nullptr;
	uchar* _alphas = nullptr;

	friend class Was;
};

class WASP;
class Was : public ReferenceCount {
public:
	Was();
	~Was();
	struct Info {
		ushort directions_count;
		ushort frames_count_per_direction;
		ushort width, height;
		short kx, ky;
	};
	int directions_count = 0;
	int frames_count_per_direction = 0;
	int width = 0, height = 0;
	int kx = 0, ky = 0;
	int frames_count = 0;

	const FrameInfo* frames = nullptr;

private:
	bool load(const char* pointer);

	void decode(int iframe);

public:
	bool isValid() const;

	const rgb16_t* getPalette(uint color, dye_t dye);

	ITexture getBitmap(uint color, dye_t dye, int iframe);
	ITexture getBitmap(int iframe);

	// 0-1F info.width  info.height
	uchar getOpacity(int mouse_x, int mouse_y, int iframe);

	static void render(ITexture texture, const FrameInfo* frame, float world_x, float world_y, int rect_x, int rect_y, int rect_width, int rect_height);
	void render(int iframe, float world_x, float world_y);
	void render(int iframe, float world_x, float world_y, int rect_x, int rect_y, int rect_width, int rect_height);
	void render4(int iframe, float world_x, float world_y, int rect_width, int e);
	void render4(int iframe, float world_x, float world_y, int rect_width, int rect_height, int e);

private:
	void freeBitmaps();

public:
	void release() override;

private:
	const char* _pointer = nullptr;
	// only point to datas
	const rgb16_t* _palatte = nullptr;
	// only point to datas
	const uint* _offsets = nullptr;

	const FrameDecoder* _decoders = nullptr;

	struct Pitmap {
		rgb16_t palette[Dye::k256];
		bool paletted = false;
		ITexture* bitmaps = nullptr;
		bool* loadeds = nullptr;
	};

	std::map<uint, Pitmap> _pitmaps;

	friend class WASP;
	WASP* _wdfp = nullptr;
	uint _uid = k0u;
};



class WASP : public WDFP {
public:
	~WASP();

	Was* getWas(uint uid);
private:
	void release(uint uid);

	std::map<uint, Was> _was;

	friend class Was;
};



class WASPCache {
public:
	WASPCache();

	WASP* open(cstr_t filename, bool memory);

private:
	std::vector<WASP> _was;
};
extern WASPCache g_wasp;




class WASGroup {
public:
	~WASGroup();
	static WASGroup* getInstance();
	static WASGroup* getSmap();
	static WASGroup* getState();

	bool open(cstr_t filename, bool memory);

	Was* getWas(uint uid);

	Was::Info getInfo(uint uid);
private:
	std::vector<WASP*> _was;
};



class WasAssignor {
protected:
	virtual ~WasAssignor();
public:
	WasAssignor& operator=(const WasAssignor& assignor);

	Was* _pwas = nullptr;
};