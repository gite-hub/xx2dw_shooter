#pragma once
#include <stack>
#include "font_manager.h"
#include "string_utf8.h"

class Was;
class TextManager : public Size {
public:
    TextManager();
    ~TextManager();
    void init();
    int checkFace(int& value);
    void applyFace(int value);
    int checkAndApplyColor();
    void applyChar(Font::word_view word);
    int checkAndApplyChar();
    int applySharp();
    void applyX(Rect* prect);
    void applyXscale(Rect* prect);
    void pushLine();
    void applyLine(cstr_t text);
    bool isEnd(int go);

    bool init(Font* pfont, int lock);
    void setString(bool channel, cstr_t text);
private:
    int _lock, _channel_y;
    int _x, _line, _index, _length;
    char_utf8_t* _pword;
    int _pword_length;

    std::stack<color_t> _sharp_color_stack;

    struct WordLayout : public Font::WordLayout {
        color_t color;
    };
    struct Face : public Rect {
        Was* pwas = nullptr;
    };
    struct Line {
        int height;
        std::vector<WordLayout> chars;
        std::vector<Face> faces;
        void init();
    };
    bool _inited;
    Font* _pfont;
    Rect _rect;
    WordLayout _char;
    Face _face;
    std::map<char, color_t> _sharp_color_table;
    std::vector<Line> _lines;
public:
    static const int k36 = 36, k17 = 17, k120 = 120;
    friend class Text;
};


extern TextManager g_text;
