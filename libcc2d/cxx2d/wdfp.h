#pragma once
#include "cxx.h"
#include "cxx_pod.h"

class WDFP {
public:
	WDFP();
	virtual ~WDFP();

	bool open(cstr_t filename, bool memory);

	cstr_t getFileName() const noexcept;

	struct Index { uint uid, offset, length; };

	static int binarySearch(was_t uid, const Index* indexs, int indexs_count);

	const Index* binarySearch(was_t uid) const;

	const Index* getIndexs() const noexcept;

	int getIndexsCount() const noexcept;

	const char* getData(const Index* pindex) const;

	const char* getData(was_t uid, int* plength = nullptr) const;

	const char* getData(was_t uid, int size) const;

protected:
	const Index* _pindex = nullptr;
	const char* _pointer = nullptr;
	std::string _filename;
	int _indexs_count;

	friend class WDFGroup;
    friend class WASGroup;
};


class WDFGroup {
public:
	static WDFGroup* getWav();
	static WDFGroup* getMp3();
	WDFGroup();

	bool open(cstr_t filename, bool memory);

	const char* getData(was_t uid, int* size = nullptr);

	bool freeData(was_t uid, const char* ptr);

protected:
	bool _all_memory;
	std::vector<WDFP> _wdfs;
};
