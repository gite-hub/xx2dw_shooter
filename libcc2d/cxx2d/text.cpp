#include "text.h"
#include "cxx_inl.h"
#include "extern2d.h"
#include "wasp.h"


Text::Text() {
}

Text::~Text() {

    RELEASE_PTR(_pwas);

    for (int k = 0; k < _lines.size(); ++k) {
        auto& line = _lines[k];
        for (auto& face : line.faces) {
            RELEASE_PTR(face.pwas);
        }
    }
}


void Text::setChannel(uint channel) {
    if (_channel == channel) {
        return;
    }
    _pwas = WASGroup::getInstance()->getWas(channel);
}

void Text::setString(cstr_t content) {
    if (_content == content) {
        return;
    }
    StringProtocol::setString(content);
    if (_font == nullptr) {
        setFont(FONT_14);
    }
    auto manager = &g_text;
    manager->init();
    manager->init(_font, _lock);
    manager->setString(_pwas != nullptr, content);

    _framePD = 0;
    _lines.resize(manager->_line);
    for (int i = 0; i < manager->_line; ++i) {
        _lines[i] = manager->_lines[i];
    }
    this->width = manager->width;
    this->height = manager->height;
}


void Text::render(float world_x, float world_y) {
    if (_pwas != nullptr) {
        _pwas->render(0, world_x, world_y);
    }
    _font->updateCachesToTexture();
    if (DeltaProtocol::step(g_dt, kFrame)) {
        ++_framePD;
    }
    for (int k = 0, iframe; k < _lines.size(); ++k) {
        const auto& line = _lines[k];
        for (const auto& face : line.faces) {
            iframe = _framePD % face.pwas->frames_count;
            face.pwas->render(iframe, world_x + face.x, world_y + face.y);
        }
        if (line.chars.empty()) {
            continue;
        }
        if (_shadow) {
            for (const auto& c : line.chars) {
                _font->render(
                    world_x + g_scale,
                    world_y + g_scale,
                    c, COLOR::BLACK);
            }
        }
        for (const auto& c : line.chars) {
            _font->render(world_x, world_y, c, c.color == 0 ? _color : c.color);
        }
    }
}
