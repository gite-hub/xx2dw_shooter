#include "dye_manager.h"
#include "colorgb.h"

DyeManager g_dye;

const was_t DyeManager::k1657 = 0x00001657;
DyeManager::~DyeManager() {
}


int DyeManager::readInt(const char* ptr, int& i, int size) {
	if (i >= size) {
		return k_1;
	}
	ptr += i;
	int v = std::atoi(ptr);
	do {
		++ptr;
		if (++i >= size) {
			return v;
		}
	} while ((*ptr) >= '0' && (*ptr) <= '9');

	do {
		++ptr;
		if (++i >= size) {
			return v;
		}
	} while ((*ptr) < '0' || (*ptr) > '9');

	return v;
}

int DyeManager::readInt(const char* ptr, int& i) {
	ptr += i;
	int v = std::atoi(ptr);
	do {
		++ptr;
		++i;
	} while ((*ptr) >= '0' && (*ptr) <= '9');

	do {
		++ptr;
		++i;
	} while ((*ptr) < '0' || (*ptr) > '9');

	return v;
}



void DyeManager::readDye(const char* ptr, int size, Segments& segments, const Dye& dye, Matrix3s& matrix3s) {
	int idx = 0;
	int n = readInt(ptr, idx);
	int nn = n % 10;
	segments.resize(nn);
	matrix3s.resize(nn);
	for (int i = 0; i < nn; ++i) {
		segments[i].start = readInt(ptr, idx);
		if (i > 0) {
			segments[i - 1].end = segments[i].start;
		}
	}
	readInt(ptr, idx);
	segments.back().end = Dye::k256;


	Matrix3 m3;
	for (int k = 0, count; k < nn; ++k) {
		count = readInt(ptr, idx);
		segments[k].count = count;
		for (int i = 0; i < count; ++i) {
			if (n > 1000) {
				readInt(ptr, idx);
			}
			m3.r1 = readInt(ptr, idx);
			m3.r2 = readInt(ptr, idx);
			m3.r3 = readInt(ptr, idx);
			m3.g1 = readInt(ptr, idx);
			m3.g2 = readInt(ptr, idx);
			m3.g3 = readInt(ptr, idx);
			m3.b1 = readInt(ptr, idx);
			m3.b2 = readInt(ptr, idx);
			m3.b3 = readInt(ptr, idx, size);
			if (n > 1000) {
				readInt(ptr, idx);
				readInt(ptr, idx);
				readInt(ptr, idx);
				readInt(ptr, idx, size);
			}
			if (i == dye.indexs[k]) {
				matrix3s[k] = m3;
			} else if (dye.indexs[k] >= count && i == 0) {
				matrix3s[k] = m3;
			}
			if (idx >= size) {
				return;
			}
		}

	}
}


bool DyeManager::open(cstr_t filename) {

	_wdfs.emplace_back(WDFP());
	auto wdf = &_wdfs.back();
	if (!wdf->open(filename, true)) {
		_wdfs.pop_back();
		return false;
	}
	if (_pIndex1657 == nullptr) {
		_pIndex1657 = wdf->binarySearch(k1657);
		if (_pIndex1657 != nullptr) {
			_pWdf1657 = wdf;
		}
	}
	return true;
}


const char* DyeManager::toData(was_t uid, int* size) {
    const WDFP::Index* pindex = nullptr;
    WDFP* pwdf = nullptr;
    forv(_wdfs, i) {
        pindex = _wdfs[i].binarySearch(uid);
        if (pindex != nullptr) {
            pwdf = &_wdfs[i];
            break;
        }
    }
    if (pindex == nullptr) {
        pwdf = _pWdf1657;
        pindex = _pIndex1657;
    }
    if (pindex == nullptr) {
        return nullptr;
    }
    if (size != nullptr) {
        *size = pindex->length;
    }
    return pwdf->getData(pindex);
}



void DyeManager::toPalette(const Segments& segments, const Matrix3s& matrix9s, const rgb16_t* palette_input, rgb16_t* palette_output) {
	auto p565 = palette_input;
	auto p888 = palette_output;
	for (int k = 0, i, r, g, b, start, end, size = segments.size(); k < size; k += 1) {
		const auto& m9 = matrix9s[k];
		start = segments[k].start;
		end = segments[k].end;
		for (i = start; i < end; i += 1) {
			p565 = palette_input + i;
			p888 = palette_output + i;
			r = p565->r * m9.r1 + p565->g * m9.r2 + p565->b * m9.r3;
			g = p565->r * m9.g1 + p565->g * m9.g2 + p565->b * m9.g3;
			b = p565->r * m9.b1 + p565->g * m9.b2 + p565->b * m9.b3;

			p888->r = std::min(r >> 8, 0x1F);
			p888->g = std::min(g >> 8, 0x3F);
			p888->b = std::min(b >> 8, 0x1F);
		}
	}
}

void DyeManager::toPalette(const rgb16_t* palette_input, rgb24_t* palette_output) {
	auto p565 = palette_input;
	auto p888 = palette_output;
	for (int k = 0; k < Dye::k256; k += 1, p565 += 1, p888 += 1) {
		p888->r = COLOR::rgb5to8(p565->r);
		p888->g = COLOR::rgb6to8(p565->g);
		p888->b = COLOR::rgb5to8(p565->b);
	}
}


bool DyeManager::toPalette(was_t uid, const Dye& dye, const rgb16_t* palette_input, rgb16_t* palette_output) {
    int size;
    auto ptr = toData(uid, &size);
    if (ptr == nullptr) {
        return false;
    }
	Segments segments;
	Matrix3s matrix3s;
	readDye(ptr, size, segments, dye, matrix3s);
	toPalette(segments, matrix3s, palette_input, palette_output);
	return true;
}





bool DyeManager::toSegment(was_t uid, Segments& segments) {
    int size;
    auto ptr = toData(uid, &size);
    if (ptr == nullptr) {
        return false;
    }
	Matrix3s matrix3s;
	Dye dye;
	readDye(ptr, size, segments, dye, matrix3s);
	return true;
}
