#pragma once
#include "wdfp.h"
#include "dye.h"

class DyeManager {
public:
	struct Matrix3 { int r1, r2, r3, g1, g2, g3, b1, b2, b3; };
	using Matrix3s = std::vector<Matrix3>;
	struct Segment { int start, end, count; };
	using Segments = std::vector<Segment>;
	~DyeManager();

	static int readInt(const char* ptr, int& i, int size);
	static int readInt(const char* ptr, int& i);

	static void readDye(const char* ptr, int size, Segments& segments, const Dye& dye, Matrix3s& matrix3s);

	bool open(cstr_t filename);
    const char* toData(was_t uid, int* size = nullptr);
	static void toPalette(const Segments& segments, const Matrix3s& matrix3s, const rgb16_t* palette_input, rgb16_t* palette_output);
	static void toPalette(const rgb16_t* palette_input, rgb24_t* palette_output);
	bool toPalette(was_t uid, const Dye& dye, const rgb16_t* palette_input, rgb16_t* palette_output);
	bool toSegment(was_t uid, Segments& segments);

private:
	std::vector<WDFP> _wdfs;
	WDFP* _pWdf1657 = nullptr;
	const WDFP::Index* _pIndex1657 = nullptr;
public:
	static const was_t k1657;
};

extern DyeManager g_dye;
