#include "label.h"
#include "extern2d.h"


void LabelProtocol::setFont(FONT_SIZE font_size) {
    _font = g_font.getFont(font_size);
}

void LabelProtocol::lock(int lock) {
    _lock = lock;
}


void LabelProtocol::setShadow(bool shadow) {
    _shadow = shadow;
}

//////////////////////////////////////////////////////////////////////////
Label::Label() {
}

void Label::setAnchorCenter(bool center) {
    _anchor_center = center;
}


void Label::setString(cstr_t content) {
    if (content == _content) {
        return;
    }
    StringProtocol::setString(content);
    if (_font == nullptr) {
        setFont(FONT_14);
    }
    _layout.clear();
    _font->toLayout(content, _lock, _layout, this->width, this->height);
}


void Label::render(float world_x, float world_y) {
    if (_font == nullptr || !_visible || _content.empty()) {
        return;
    }
    if (_anchor_center) {
        world_x -= width * 0.5f;
        world_y -= height * 0.5f;
    }
    if (_shadow) {
        _font->render(
            world_x + g_scale,
            world_y + g_scale,
            _layout, COLOR::BLACK);
    }
    _font->render(world_x, world_y, _layout, _color);
}
