#pragma once
#include "cxx_stl.h"
#include "cxx_t.h"
#include "cxx_enum.h"
#include "colorgb.h"
#include "vec2.h"
#include "texture.h"

class Font {
public:
    Font();
    ~Font();

    using word_t = char_utf8_t;
    using word_view = char_utf8_t;
    void init(int font_size, int texture_size);

    using Pixel = uchar;
    struct Cache : public Rect { const Pixel* datas; };
    const Pixel* emplace(word_view word, Rect& rect);

    void pushCache(const Rect& rect, const uchar* ptr);

    void correctLine(std::vector<Rect*>& line_layout);

    void toLayoutNewLine(int& x, int& height, std::vector<Rect*>& line_layout);

    struct WordLayout : public Rect { word_t word; };
    using Layout = std::vector<WordLayout>;
	void toLayout(cstr_t content, int lock, Layout& layout, int& width, int& height);

	void updateCachesToTexture();

	void render(float world_x, float world_y, const Layout& layout, color_t color);

	void render(float world_x, float world_y, const WordLayout& word_layout, color_t color);

private:
    int _font_size;
	int _refer_size;
    int _cels_count;
    int _temp_index = 0;
	std::map<word_t, Rect> _words;

    int _texture_size;
	ITexture _texture = kInvalidTexture;

    Rect _rect;
    Cache _cache;
	std::vector<Cache> _caches;
public:
    static const int kLock;
    friend class FontManager;
};


class FontManager {
public:
	Font* getFont(FONT_SIZE font_size);

private:
	std::map<FONT_SIZE, Font> _fonts;
};

extern FontManager g_font;
