#include "string_utf8.h"
#include "ConvertUTF/ConvertUTF.h"


StringUFT8::StringUFT8() {
}
StringUFT8::StringUFT8(char_utf8_t c) {
    operator+=(c);
}
StringUFT8::StringUFT8(const StringUFT8& str8) {
    operator+=(str8);
}
StringUFT8::StringUFT8(cstr_t str) {
    operator+=(str);
}


StringUFT8::~StringUFT8() {

}


bool StringUFT8::conver(cstr_t str, Chars& chars) {
    llvm::UTF8* sequence_utf8 = (llvm::UTF8*)str.c_str();
    int length = llvm::getUTF8StringLength(sequence_utf8);
    if (length == 0) {
        // CC_LOG("Bad utf-8 set string: %s", newStr.data());
        return false;
    }
    while (*sequence_utf8) {
        length = llvm::getNumBytesForUTF8(*sequence_utf8);
        chars.push_back(conver((char*)sequence_utf8, length));
        sequence_utf8 += length;
    }
    return true;
}


char_utf8_t StringUFT8::conver(const char* str, int length) {
    char temps[sizeof(char_utf8_t)] = { 0 };
    memcpy(temps, str, length);
    return *(char_utf8_t*)temps;
}


std::string StringUFT8::conver(char_utf8_t i) {
    char temps[sizeof(char_utf8_t)] = { 0 };
    memcpy(temps, &i, sizeof(char_utf8_t));
    return std::string(temps);
}


int StringUFT8::getLength(char_utf8_t i) {
    int temp = 0;
    while (true) {
        if (temp == 0) {
            break;
        }
        temp += 1;
        i >>= 8;
    }
    return temp;
}


StringUFT8& StringUFT8::operator=(char_utf8_t c) {
    _chars = { c };
    _inner = conver(c);
    return *this;
}
// StringUFT8& StringUFT8::operator=(const StringUFT8& str8) {
//     
// }
StringUFT8& StringUFT8::operator=(cstr_t str) {
    _chars.clear();
    if (conver(str, _chars)) {
        _inner = str;
    } else {
        _inner.clear();
    }
    return *this;
}

StringUFT8& StringUFT8::operator+=(char_utf8_t c) {
    _chars.push_back(c);
    _inner.append(conver(c));
    return *this;
}
StringUFT8& StringUFT8::operator+=(const StringUFT8& str8) {
    for (int i = 0; i < str8._chars.size(); i += 1) {
        _chars.push_back(str8._chars[i]);
    }
    _inner += str8._inner;
    return *this;
}
StringUFT8& StringUFT8::operator+=(cstr_t str) {
    if (conver(str, _chars)) {
        _inner += str;
    }
    return *this;
}


StringUFT8 StringUFT8::operator+(char_utf8_t c) const{
    StringUFT8 temp;
    temp += c;
    return temp;
}
StringUFT8 StringUFT8::operator+(const StringUFT8& str8) const {
    StringUFT8 temp;
    temp += str8;
    return temp;
}
StringUFT8 StringUFT8::operator+(cstr_t str) const{
    StringUFT8 temp;
    temp += str;
    return temp;
}


char_utf8_t StringUFT8::operator[](int i) const {
    return _chars[i];
}

StringUFT8::operator const std::string& () const {
    return _inner;
}


std::size_t StringUFT8::length() const {
    return _chars.size();
}

void StringUFT8::pop_back() {
    auto l = getLength(_chars.back());
    _chars.pop_back();
    for (int i = 0; i < l; i += 1) {
        _inner.pop_back();
    }
}

#if 0
void StringUFT8::replace(std::string_view newStr) {
    _str.clear();
    if (!newStr.empty()) {
        UTF8* sequenceUtf8 = (UTF8*)newStr.data();

        int lengthString = getUTF8StringLength(sequenceUtf8);

        if (lengthString == 0) {
            CC_LOG("Bad utf-8 set string: %s", newStr.data());
            return;
        }

        while (*sequenceUtf8) {
            std::size_t lengthChar = getNumBytesForUTF8(*sequenceUtf8);

            CharUTF8 charUTF8;
            charUTF8._char.append((char*)sequenceUtf8, lengthChar);
            sequenceUtf8 += lengthChar;

            _str.emplace_back(charUTF8);
        }
    }
}

std::string StringUFT8::getAsCharSequence() const {
    return getAsCharSequence(0, std::numeric_limits<std::size_t>::max());
}

std::string StringUFT8::getAsCharSequence(std::size_t pos) const {
    return getAsCharSequence(pos, std::numeric_limits<std::size_t>::max());
}

std::string StringUFT8::getAsCharSequence(std::size_t pos, std::size_t len) const {
    std::string charSequence;
    std::size_t maxLen = _str.size() - pos;
    if (len > maxLen) {
        len = maxLen;
    }

    std::size_t endPos = len + pos;
    while (pos < endPos) {
        charSequence.append(_str[pos++]._char);
    }

    return charSequence;
}

bool StringUFT8::erase(std::size_t pos) {

}

bool StringUFT8::deleteChar(std::size_t pos) {
    if (pos < _str.size()) {
        _str.erase(_str.begin() + pos);
        return true;
    } else {
        return false;
    }
}

bool StringUFT8::insert(std::size_t pos, cstr_t str) {

}


bool StringUFT8::insert(std::size_t pos, std::string_view insertStr) {
    StringUFT8 utf8(insertStr);

    return insert(pos, utf8);
}

bool StringUFT8::insert(std::size_t pos, const StringUFT8& insertStr) {
    if (pos <= _str.size()) {
        _str.insert(_str.begin() + pos, insertStr._str.begin(), insertStr._str.end());

        return true;
    } else {
        return false;
    }
}

#endif
