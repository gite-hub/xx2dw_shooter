#pragma once
#include "text_manager.h"
#include "label.h"

class Text : public LabelProtocol, public StringProtocol, public VisibleProtocol, public ColorProtocol, public DeltaProtocol, public Size {
public:
    Text();
    virtual ~Text();

    void setChannel(uint channel);

    void setString(cstr_t content) override;

    void render(float world_x, float world_y);

protected:
    int _framePD = 0;

    std::vector<TextManager::Line> _lines;

    uint _channel = k0u;
    Was* _pwas = nullptr;
};
