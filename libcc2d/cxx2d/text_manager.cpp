#include "text_manager.h"
#include "cxx_inl.h"
#include "wasp.h"

TextManager g_text;

void TextManager::Line::init() {
    height = 0;
    chars.clear();
    faces.clear();
}


static bool isNot7F(unsigned long long text) {
    return /*(uchar)*/text > 0x7F;
}


TextManager::TextManager() {
    _inited = false;
}

TextManager::~TextManager() {
    DELETE_ARR(_pword);
}

void TextManager::init() {
    if (_inited) {
        return;
    }
    _inited = true;
    _lines.reserve(16);
    _lines.resize(1);
    _sharp_color_table.emplace('W', COLOR::WHITE);
    _sharp_color_table.emplace('Y', COLOR::YELLOW);
    _sharp_color_table.emplace('R', COLOR::RED);
    _sharp_color_table.emplace('G', COLOR::GREEN);
    _sharp_color_table.emplace('B', COLOR::BLUE);
    // _sharp_color_map.emplace('M', COLOR::MAGENTA);
    // _sharp_color_map.emplace('O', COLOR::ORANGE);
    _sharp_color_table.emplace('X', COLOR::BLACK);

    _pword = new char_utf8_t[_pword_length = 128];
}

int TextManager::checkFace(int& temp) {
    int go = 0;
    temp = k_1;
    // _index is #
    for (int i = 1, v, t = 0; i <= 3; ++i) {
        if (isEnd(i)) {
            break;
        }
        v = _pword[_index + i] - '0';
        if (v < 0 || v > 9) {
            break;
        }
        t = (i == 1) ? v : (temp * 10 + v);
        if (t >= k120) {
            break;
        }
        temp = t;
        go = i;
    }
    return go;
}


void TextManager::applyFace(int value) {
    _face.pwas = nullptr; // WasManager::getInstance()->getWas(txtChat::getUID(value));
    if (_face.pwas != nullptr) {
        _face.width = _face.pwas->width;
        _face.height = _face.pwas->height;
        applyX(&_face);
        _lines[_line].faces.push_back(_face);
    }
}


int TextManager::checkAndApplyColor() {
    // _index is #
    const auto& cmap = _sharp_color_table;
    const auto& it = cmap.find(_pword[_index + 1]);
    if (it == cmap.end()) {
        return 0;
    }
    auto color = it->second;
    auto& cs = _sharp_color_stack;
    if (cs.empty()) {
        cs.push(color);
    } else if (color == cs.top()) {
        cs.pop();
    } else {
        cs.push(color);
    }
    return 1;
}


void TextManager::applyChar(Font::word_view word) {
    auto ptr = _pfont->emplace(word, _rect);
    if (ptr != nullptr) {
        _pfont->pushCache(_rect, ptr);
    }
    _char.word = word;
    _char.height = _rect.height;
    _char.width = (word == ' ') ? (_char.height / 2) : _rect.width;
    if (_sharp_color_stack.empty()) {
        _char.color = 0;
    } else {
        _char.color = _sharp_color_stack.top();
    }
    applyXscale(&_char);
    if (word != ' ') {
        _lines[_line].chars.push_back(_char);
    }
}


int TextManager::checkAndApplyChar() {
    int temp, go;
    if (isNot7F(_pword[_index])) {
        if (isEnd(1)) {
            temp = 0;
            go = 1;
        } else {
            temp = StringUFT8::getLength(_pword[_index]);
            go = temp;
        }
    } else {
        temp = 1;
        go = 1;
    }
    if (temp > 0) {
        // auto wchat = cFont::toString(_pword + _index, temp);
        // applyChar(wchat);
    }
    return go;
}


int TextManager::applySharp() {
    // 不包括#本身长度
    if (isEnd(1)) {
        applyChar('#');
        return 0;
    }
    int temp;
    int go = checkFace(temp);
    if (temp != k_1) {
        applyFace(temp);
        return go;
    }
    go = checkAndApplyColor();
    if (go > 0) {
        return go;
    }
    applyChar('#');
    if (_pword[_index + 1] == '#') {
        return 1;
    }
    return 0;
}



void TextManager::applyX(Rect* prect) {
    if (_lock > 0 && _x + prect->width >= _lock) {
        pushLine();
    }
    prect->x = _x;
    _x += prect->width;
    if (this->width < _x) {
        this->width = _x;
    }
    auto& h = _lines[_line].height;
    if (h < prect->height) {
        h = prect->height;
    }
}

void TextManager::applyXscale(Rect* prect) {
    applyX(prect);
#if 0
    int width = std::ceil(prect->width * _pfont->getScale() * _font_scale);
    if (_lock > 0 && _x + width >= _lock) {
        pushLine();
    }
    prect->x = _x;
    _x += width;
    if (this->width < _x) {
        this->width = _x;
    }
    auto& h = _lines[_line].height;
    int height = std::ceil(prect->height * _pfont->getScale() * _font_scale);
    if (h < height) {
        h = height;
    }
#endif
}


void TextManager::pushLine() {
    if ((++_line) >= _lines.size()) {
        Line line;
        _lines.emplace_back(line);
    }
    _lines[_line].init();
    _x = 0;
}



void TextManager::applyLine(cstr_t text) {
    StringUFT8 str8 = text;
    _length = str8.length();
    if (_pword_length < _length + 1) {
        delete[] _pword;
        _pword = new char_utf8_t[_pword_length = _length + 1];
    }
    for (int i = 0; i < _length; i += 1) {
        _pword[i] = str8[i];
    }
    _pword[_length] = 0;
    // _pword = Charset::toUnicode(text.c_str(), _length);
    _index = 0;
    int go;
    while (_index < _length) {
        if (_pword[_index] == '#') {
            go = applySharp() + 1;
        } else {
            go = 1; // checkAndApplyChar();
            if (_index < _length/* - 1*/) {
                applyChar(_pword[_index]);
            }
        }
        _index += go;
    }
    // delete[] _pword;
}


bool TextManager::isEnd(int go) {
    return _index + go >= _length;
}


bool TextManager::init(Font* pfont, int lock) {
    // _font_scale = font_scale;
    _pfont = pfont; // ptruetype;
    _lock = lock;
    return true;
}


void TextManager::setString(bool channel, cstr_t text) {
    std::vector<std::string> enters;
    if (true) {
        size_t last = 0;
        size_t index = text.find_first_of('\n', last);

        if (index == std::string::npos) {
            enters.push_back(text);
        } else {
            while (index != std::string::npos) {
                enters.push_back(text.substr(last, index - last));
                last = index + 1;
                index = text.find_first_of('\n', last);
            }
            if (index - last > 0) {
                enters.push_back(text.substr(last, index - last));
            }
        }
    }

    _x = channel ? k36 : 0;
    this->width = _line = 0;
    _lines[0].init();
    if (channel) {
        _lines[0].height = k17;
    }
    while (!_sharp_color_stack.empty()) {
        _sharp_color_stack.pop();
    }
    for (int k = 0; k < enters.size(); ++k) {
        applyLine(enters[k]);
        pushLine();
    }

    int height = 0;
    int kIntervalHeight = channel ? 1 : 3;
    // float scale = _pfont->getScale() * _font_scale;
    for (int k = 0; k < _line; ++k) {
        auto& line = _lines[k];
        for (int i = 0; i < line.chars.size(); ++i) {
            auto& c = line.chars[i];
            c.y = height + line.height - std::ceil(c.height/* * scale*/);
        }
        for (int i = 0; i < line.faces.size(); ++i) {
            auto& f = line.faces[i];
            f.x += f.pwas->kx;
            f.y = height + line.height - f.height + f.pwas->ky;
        }
        height += line.height;
        if (k < _line - 1) {
            height += kIntervalHeight;
        }
    }
    this->height = height;
}
