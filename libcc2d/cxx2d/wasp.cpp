#include "wasp.h"
#include "cxx_inl.h"
#include "cxx_func.h"
#include "memory2d.h"
#include "texture_impl.h"
#include "gl_manager.h"

// #include "opencv2/imgproc.hpp"

WASPCache g_wasp;

FrameDecoder::~FrameDecoder() {
	DELETE_ARR(_indexs);
	DELETE_ARR(_alphas);
}


bool FrameDecoder::isValid() const {
	return _alphas != nullptr;
}


void FrameDecoder::decode(int width, int height, const uchar* palette, uint offsets) {
	if (isValid()) {
		return;
	}
	int size = width * height;
	_indexs = new uchar[size];
	_alphas = new uchar[size];
	memset(_alphas, 0, size * sizeof(*_alphas));

	auto ptr_lines = (uint*)(palette + offsets + 16);
	auto ptr_index = _indexs;
	auto ptr_alpha = _alphas;
	auto ptr_data = palette;
	uchar value_check, value_index, value_alpha;
	for (int k = 0, i; k < height; ++k, ++ptr_lines, ptr_index += width, ptr_alpha += width) {
		ptr_data = palette + offsets + (*ptr_lines);
		if (k > 0 && (*ptr_data) == 0) {
			memcpy(ptr_index, ptr_index - width, width);
			memcpy(ptr_alpha, ptr_alpha - width, width);
			continue;
		}
		for (i = 0; i < width;) {
			if ((value_check = *ptr_data++) == 0) {
				break;
			}
			size = (value_check & 0x3F);
			switch ((value_check >> 6) & 3) {
			case 0:
				size = (value_check & 0x1F);
				if (value_check & 0x20) {
					value_alpha = size;
					ptr_index[i] = *ptr_data++;
					ptr_alpha[i] = value_alpha;
					++i;
				} else {
					value_alpha = *ptr_data++;
					value_index = *ptr_data++;
					memset(ptr_index + i, value_index, size);
					memset(ptr_alpha + i, value_alpha, size);
					i += size;
				}
				break;
			case 1:
				memcpy(ptr_index + i, ptr_data, size);
				memset(ptr_alpha + i, 0x1F, size);
				ptr_data += size;
				i += size;
				break;
			case 2:
				value_index = *ptr_data++;
				memset(ptr_index + i, value_index, size);
				memset(ptr_alpha + i, 0x1F, size);
				i += size;
				break;
			default: // 11??????
				if (size == 0) {
					// 看起来镂空,其实透明度不是0
					if (ptr_alpha[i - 1] > 0) {
						ptr_alpha[i - 1] = 0x1F;
					}
					ptr_data += 2;
				} else {
					i += size;
				}
				break;
			}
		}
	}
}


void FrameDecoder::toBitmap(int width, int height, const rgb16_t* palette, ITexture bitmap) const {
	int pitch = cc2d::pitch<rgb32_t>(width);
	auto datas = cc2d::news<rgb32_t>(pitch * height);
	memset(datas, 0, sizeof(datas[0]) * pitch * height);
	auto p8 = datas;
	auto pI = _indexs;
	auto pA = _alphas;
	auto p5 = palette;
	for (int k = 0, i; k < height; k += 1) {
		p8 = datas + (k * pitch);
		pI = _indexs + (k * width);
		pA = _alphas + (k * width);
		for (i = 0; i < width; i += 1, p8 += 1, pI += 1, pA += 1) {
            // COLOR::rgb5to8(*pA) << 8 | (*pI)
			if (*pA) {
                *p8 = COLOR::to32(palette[*pI], *pA);
			}
		}
	}

#if 0
    cv::Mat image(height, pitch, CV_8UC4);
    memcpy(image.data, datas, pitch * height * sizeof(cv::Vec4b));
    cc2d::deleteMemory(datas);

    cv::Mat resized_image;
    cv::Size size(pitch * 2, height * 2);
    cv::pyrUp(image, resized_image, size/*, cv::INTER_CUBIC*/);
    datas = cc2d::newMemory<rgb32_t>(size.width * size.height);
    memcpy(datas, resized_image.datastart, size.width * size.height * sizeof(*datas));

	TextureImpl::load(bitmap, PIXEL_32, size.width, size.height, datas);
#else
    TextureImpl::load(bitmap, PIXEL_32, pitch, height, datas);
#endif
	cc2d::deletes(datas);
}



////////////////////////////////////////////////////////////////////////// sSp
Was::Was() {

}

Was::~Was() {	
	freeBitmaps();
	for (auto& it : _pitmaps) {
		DELETE_ARR(it.second.loadeds);
	}
	DELETE_ARR(frames);
	DELETE_ARR(_decoders);
}

bool Was::load(const char* pointer) {
	if (*(ushort*)pointer != 0x5053) {
		return false;
	}
	ushort head = *(ushort*)(pointer + 2);
	auto info = (Info*)(pointer + 4);
	directions_count = info->directions_count;
	frames_count_per_direction = info->frames_count_per_direction;
	width = info->width;
	height = info->height;
	kx = info->kx;
	ky = info->ky;
	frames_count = directions_count * frames_count_per_direction;

	head += 4;
	_palatte = (rgb16_t*)(pointer + head);
	_offsets = (uint*)(pointer + head + 512);

	head = *(ushort*)(pointer + 2);
	frames = new FrameInfo[frames_count];
	auto frame = frames;
	auto offs = _offsets;
	auto pter = pointer + head + 4;
	for (int k = 0; k < frames_count; ++k, ++frame, ++offs) {
		if (*offs == 0) {
			continue;
		}
		memcpy(const_cast<FrameInfo*>(frame), (pter + *offs), sizeof(FrameInfo));
	}
	return true;
}


void Was::decode(int iframe) {
	if (_decoders == nullptr) {
		_decoders = new FrameDecoder[frames_count];
	}
	auto frame = frames + iframe;
	const_cast<FrameDecoder*>(_decoders + iframe)->decode(
		frame->width, frame->height, (const uchar*)_palatte, *(_offsets + iframe));
}

bool Was::isValid() const {
	return frames != nullptr;
}


const rgb16_t* Was::getPalette(uint color, dye_t dye) {
	if (color == k0u && dye == k0u) {
		return _palatte;
	}
	auto& pitmap = _pitmaps[dye];
	if (!pitmap.paletted) {
		g_dye.toPalette(color, dye, _palatte, pitmap.palette);
		pitmap.paletted = true;
	}
	return pitmap.palette;
}


gluid_t Was::getBitmap(uint color, dye_t dye, int iframe) {
	auto palette = getPalette(color, dye);
	auto& pitmap = _pitmaps[dye];

	if (pitmap.loadeds == nullptr) {
		pitmap.loadeds = new bool[frames_count];
		for (int i = 0; i < frames_count; i += 1) {
			pitmap.loadeds[i] = false;
		}
	}
	if (pitmap.loadeds[iframe]) {
		return pitmap.bitmaps[iframe];
	}
	if (pitmap.bitmaps == nullptr) {
		pitmap.bitmaps = TextureImpl::generate(frames_count);
	}
	decode(iframe);
	auto frame = frames + iframe;
	auto decoder = _decoders + iframe;
	decoder->toBitmap(frame->width, frame->height, palette, pitmap.bitmaps[iframe]);
	pitmap.loadeds[iframe] = true;
	return pitmap.bitmaps[iframe];
}



gluid_t Was::getBitmap(int iframe) {
	return getBitmap(k0u, k0u, iframe);
}


uchar Was::getOpacity(int mouse_x, int mouse_y, int iframe) {
	auto frame = frames + iframe;
	int x = frame->x - (kx - mouse_x);
	int y = frame->y - (ky - mouse_y);
	if (x < 0 || x >= frame->width || y < 0 || y >= frame->height) {
		return 0;
	}
	decode(iframe);
	return (_decoders + iframe)->_alphas[y * frame->width + x];
}


void Was::render(ITexture texture, const FrameInfo* frame, 
	float world_x, float world_y, int rect_x, int rect_y, int rect_width, int rect_height) {

#if 1
    TextureImpl::render(texture, SHADER_DEFAULT, COLOR::WHITE,
        world_x - frame->x, world_y - frame->y,
        frame->width, frame->height,
        rect_x, rect_y, rect_width, rect_height,
        rect_width, rect_height);
#else
    TextureImpl::render(texture, SHADER_DEFAULT, COLOR::WHITE,
        world_x - frame->x * 2, world_y - frame->y * 2,
		frame->width * 2, frame->height * 2,
        rect_x * 2, rect_y * 2, rect_width * 2, rect_height * 2,
        rect_width * 2, rect_height * 2);
#endif
}


void Was::render(int iframe, float world_x, float world_y) {
	auto frame = frames + iframe;
	auto texture = getBitmap(iframe);
	render(texture, frame, world_x, world_y, 0, 0, frame->width, frame->height);
}

void Was::render(int iframe, float world_x, float world_y, int rect_x, int rect_y, int rect_width, int rect_height) {
	auto frame = frames + iframe;
	auto texture = getBitmap(iframe);
	render(texture, frame, world_x, world_y, rect_x, rect_y, rect_width, rect_height);
}

void Was::render4(int iframe, float world_x, float world_y, int width, int e) {
	auto frame = frames + iframe;
	auto texture = getBitmap(iframe);
	width -= e;
	render(texture, frame, world_x, world_y, 0, 0, width, frame->height);
	render(texture, frame, world_x + width, world_y, frame->width - e, 0, e, frame->height);
}

void Was::render4(int iframe, float world_x, float world_y, int width, int height, int e) {
	auto frame = frames + iframe;
	auto texture = getBitmap(iframe);
	width -= e;
	height -= e;
	render(texture, frame, world_x, world_y, 0, 0, width, height);
	render(texture, frame, world_x, world_y + height, 0, frame->height - e, width, e);
	render(texture, frame, world_x + width, world_y, frame->width - e, 0, e, height);
	render(texture, frame, world_x + width, world_y + height, frame->width - e, frame->height - e, e, e);
}


void Was::freeBitmaps() {
	for (auto& it : _pitmaps) {
		auto& pitmap = it.second;
		if (pitmap.bitmaps != nullptr) {
			TextureImpl::free(frames_count, pitmap.bitmaps);
			pitmap.bitmaps = nullptr;
		}
		if (pitmap.loadeds != nullptr) {
			for (int i = 0; i < frames_count; i += 1) {
				pitmap.loadeds[i] = false;
			}
		}
	}
}


void Was::release() {
	if (_wdfp != nullptr) {
		_wdfp->release(_uid);
	}
}

//////////////////////////////////////////////////////////////////////////
WASP::~WASP() {

}

Was* WASP::getWas(uint uid) {
	const auto& it = _was.find(uid);
	if (it != _was.end()) {
		it->second.retain();
		return &it->second;
	}
	auto ptr = getData(uid);
	if (ptr == nullptr) {
		return nullptr;
	}
	Was* was = &_was[uid];
	was->_uid = uid;
	was->_wdfp = this;
	if (_pointer == nullptr) {
		was->_pointer = ptr;
	}
	was->load(ptr);
	return was;
}

void WASP::release(uint uid) {
	const auto& it = _was.find(uid);
	if (it != _was.end()) {
		it->second.ReferenceCount::release();
		if (it->second.getReferenceCount() == 0) {
			_was.erase(it);
		}
	}
}



//////////////////////////////////////////////////////////////////////////
WASPCache::WASPCache() {
	_was.reserve(48);
}

WASP* WASPCache::open(cstr_t filename, bool memory) {
	for (auto& wdf : _was) {
		if (wdf.getFileName() == filename) {
			return &wdf;
		}
	}
	_was.push_back(WASP());
	auto pwdf = &_was.back();
	if (pwdf->open(filename, memory)) {
		return pwdf;
	}
	_was.pop_back();
	return nullptr;
}



////////////////////////////////////////////////////////////////////////// WASGroup
WASGroup::~WASGroup() {
}


WASGroup* WASGroup::getInstance() {
	static WASGroup* s_manager = new WASGroup();
	return s_manager;
}


WASGroup* WASGroup::getSmap() {
	static WASGroup* s_smap = new WASGroup();
	return s_smap;
}


WASGroup* WASGroup::getState() {
	static WASGroup* s_state = new WASGroup();
	return s_state;
}


bool WASGroup::open(cstr_t filename, bool memory) {
	auto pwdf = g_wasp.open(filename, memory);
	if (pwdf != nullptr) {
		_was.emplace_back(pwdf);
		return true;
	}
	return false;
}


Was* WASGroup::getWas(uint uid) {
	forv(_was, i) {
		auto pwas = _was[i]->getWas(uid);
		if (pwas != nullptr) {
			return pwas;
		}
	}
	return nullptr;
}


Was::Info WASGroup::getInfo(uint uid) {
	Was::Info info;
	forv(_was, i) {
		auto ptr = _was[i]->getData(uid, 16);
		if (ptr != nullptr) {
			info = *(Was::Info*)(ptr + 4);
            // _was[i]->freeData(uid, ptr);
            if (_was[i]->_pointer == nullptr) {
                delete[] ptr;
            }
			break;
		}
	}
	return info;
}



//////////////////////////////////////////////////////////////////////////
WasAssignor::~WasAssignor() {
	RELEASE_PTR(_pwas);
}

WasAssignor& WasAssignor::operator=(const WasAssignor& assignor) {
	RELEASE_PTR(_pwas);
	_pwas = assignor._pwas;
	_pwas->retain();
	return *this;
}
