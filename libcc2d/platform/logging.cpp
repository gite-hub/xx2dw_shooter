#include "logging.h"

#if defined(CC_ANDROID)
#include <android/log.h>
#elif defined(CC_WIN32)
#include "win32.h"
#include "unicode32.h"
#endif

void cc2d::print(LOG_LEVEL level, const std::string& message) {
#ifdef CC_ANDROID
    int prio;
    switch (level) {
    case LOG_INFO:
        prio = ANDROID_LOG_INFO;
        break;
    case LOG_WARN:
        prio = ANDROID_LOG_WARN;
        break;
    case LOG_ERROR:
        prio = ANDROID_LOG_ERROR;
        break;
    default:
        prio = ANDROID_LOG_DEBUG;
    }
//	struct trim_one_eol {
//		explicit trim_one_eol(std::string& v) : value(v) {
//			trimed = !v.empty() && v.back() == '\n';
//			if (trimed)
//				value.back() = '\0';
//		}
//		~trim_one_eol() {
//			if (trimed)
//				value.back() = '\n';
//		}
//		operator const char* const() const { return value.c_str(); }
//		std::string& value;
//		bool trimed{ false };
//	};
    __android_log_print(prio, "logging", "%s", message.c_str());

#elif defined(CC_WIN32)
    if (::IsDebuggerPresent()) {
		OutputDebugString((Charset::toUnicode(true, message.c_str()) + L"\n").c_str());
    }
    // 	auto hStdout = ::GetStdHandle(item.level_ != LogLevel::Error ? STD_OUTPUT_HANDLE : STD_ERROR_HANDLE);
    // 	if (hStdout) {
    // 		// print to console if possible
    // 		// since we use win32 API, the ::fflush call doesn't required.
    // 		// see: https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-flushfilebuffers#return-value
    // 		::WriteFile(hStdout, item.qualified_message_.c_str(), static_cast<DWORD>(item.qualified_message_.size()),
    // 			nullptr, nullptr);
    // 	}
#else
    // Linux, Mac, iOS, etc
    auto outfp = item.level_ != LogLevel::Error ? stdout : stderr;
    auto outfd = ::fileno(outfp);
    ::fflush(outfp);
    ::write(outfd, item.qualified_message_.c_str(), item.qualified_message_.size());
#endif
}



void cc2d::print(LOG_LEVEL level, const char* format, ...) {
    va_list args;
    va_start(args, format);
    auto message = cc2d::format(format, args);
    va_end(args);
    print(level, message);
}

void cc2d::print(const char* format, ...) CC_FORMAT_PRINTF(1, 2) {
    va_list args;
    va_start(args, format);
    auto message = cc2d::format(format, args);
    va_end(args);
    print(LOG_DEFAULT, message);
}
