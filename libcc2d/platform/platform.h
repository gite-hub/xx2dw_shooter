#ifndef __CC_PLATFORM_MACROS_H__
#define __CC_PLATFORM_MACROS_H__

// #if __has_include(<winapifamily.h>)
// #    include <winapifamily.h>
// #endif
// 
// #define CC_PLATFORM_UNKNOWN 0
// #define CC_PLATFORM_WIN32   1
// #define CC_PLATFORM_WINUWP  2
// #define CC_PLATFORM_LINUX   3
// #define CC_PLATFORM_OSX     4
// #define CC_PLATFORM_ANDROID 5
// #define CC_PLATFORM_IOS     6
// #define CC_PLATFORM_TVOS    7
// #define CC_PLATFORM_WASM    8
// 
// // alias platform macros
// #define CC_PLATFORM_WINRT      CC_PLATFORM_WINUWP
// #define CC_PLATFORM_MAC        CC_PLATFORM_OSX
// #define CC_PLATFORM_EMSCRIPTEN CC_PLATFORM_WASM
// 
// // Determine target platform by compile environment macro.
// #define CC_PLATFORM CC_PLATFORM_UNKNOWN
// 
// // Apple: Mac and iOS
// #if defined(__APPLE__) && !defined(__ANDROID__)  // exclude android for binding generator.
// #    include <TargetConditionals.h>
// #    if TARGET_OS_IPHONE  // TARGET_OS_IPHONE includes TARGET_OS_IOS TARGET_OS_TV and TARGET_OS_WATCH. see
//                           // TargetConditionals.h
// #        undef CC_PLATFORM
// #        define CC_PLATFORM CC_PLATFORM_IOS
// #        if TARGET_OS_TV && !defined(AX_TARGET_OS_TVOS)
// #            define AX_TARGET_OS_TVOS 1
// #        endif
// #    elif TARGET_OS_MAC
// #        undef CC_PLATFORM
// #        define CC_PLATFORM CC_PLATFORM_MAC
// #    endif
// #endif
// 
// // win32
// #if defined(_WIN32) && WINAPI_FAMILY == WINAPI_FAMILY_DESKTOP_APP
// #    undef CC_PLATFORM
// #    define CC_PLATFORM CC_PLATFORM_WIN32
// #endif
// 
// // WinRT (Windows 10.0 UWP App)
// #if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)
// // Windows Store or Universal Windows Platform
// #    undef CC_PLATFORM
// #    define CC_PLATFORM CC_PLATFORM_WINRT
// #endif
// 
// // linux
// #if defined(__linux__)
// #    undef CC_PLATFORM
// #    define CC_PLATFORM CC_PLATFORM_LINUX
// #endif
// 
// #if defined(__EMSCRIPTEN__)
// #    undef CC_PLATFORM
// #    define CC_PLATFORM CC_PLATFORM_WASM
// #endif
// 
// // android, override linux
// #if defined(__ANDROID__) || defined(ANDROID)
// #    undef CC_PLATFORM
// #    define CC_PLATFORM CC_PLATFORM_ANDROID
// #endif
// 
// //////////////////////////////////////////////////////////////////////////
// // post configure
// //////////////////////////////////////////////////////////////////////////
// 
// // check user set platform
// #if !CC_PLATFORM
// #    error "Cannot recognize the target platform; are you targeting an unsupported platform?"
// #endif
// 
// #if (CC_PLATFORM == CC_PLATFORM_WIN32)
// #    ifndef __MINGW32__
// #        pragma warning(disable : 4127)
// #    endif
// #endif  // CC_PLATFORM_WIN32

/*
The google/angle is library which translate native graphics API to GLES3+ APIs
repo: https://github.com/google/angle
windows: d3d9/d3d11/Desktop GL/Vulkan
macOS/iOS: Metal
Android: GLES/Vulkan
Linux: Desktop GL/Vulkan
*/
// 0: indicate: not use GLES
// mac/iOS/android use system builtin GL/GLES, not ANGLE
// Windows: use ANGLE GLES
// #ifndef AX_GLES_PROFILE
// #    if defined(__ANDROID__)
// #        define AX_GLES_PROFILE 200
// #    elif (CC_PLATFORM == CC_PLATFORM_WINRT)
// #        define AX_GLES_PROFILE 300
// #    else
// #        define AX_GLES_PROFILE 0
// #    endif
// #endif
// 
// #define AX_GLES_PROFILE_DEN 100
// 
// #if ((CC_PLATFORM == CC_PLATFORM_ANDROID) || (CC_PLATFORM == CC_PLATFORM_IOS) || \
//      (CC_PLATFORM == CC_PLATFORM_WINRT) || (CC_PLATFORM == CC_PLATFORM_WASM))
// #    define CC_PLATFORM_MOBILE
// #else
// #    define CC_PLATFORM_PC
// #endif

#if defined(_WIN32)
#define CC_WIN32
// #undef CC_ANDROID
// #undef CC_MOBILE
#elif defined(__ANDROID__)
// #undef CC_WIN32
#define CC_ANDROID
#define CC_MOBILE
#endif


// #if defined(__APPLE__)
// #    if !defined(AX_USE_GL)
// #        define AX_USE_METAL 1
// #    endif
// #else  // win32,linux,winuwp,android
// #    if !defined(AX_USE_GL)
// #        define AX_USE_GL 1
// #    endif
// #endif



/** @def AX_DISALLOW_COPY_AND_ASSIGN(TypeName)
 * A macro to disallow the copy constructor and operator= functions.
 * This should be used in the private: declarations for a class
 */
#if defined(__GNUC__) && ((__GNUC__ >= 5) || ((__GNUG__ == 4) && (__GNUC_MINOR__ >= 4))) || \
    (defined(__clang__) && (__clang_major__ >= 3)) || (_MSC_VER >= 1800)
#    define CC_DISALLOW_COPY_AND_ASSIGN(TypeName) \
        TypeName(const TypeName&) = delete;       \
        TypeName& operator=(const TypeName&) = delete;
#else
#    define CC_DISALLOW_COPY_AND_ASSIGN(TypeName) \
        TypeName(const TypeName&);                \
        TypeName& operator=(const TypeName&);
#endif

/** @def AX_DISALLOW_IMPLICIT_CONSTRUCTORS(TypeName)
 * A macro to disallow all the implicit constructors, namely the
 * default constructor, copy constructor and operator= functions.
 *
 * This should be used in the private: declarations for a class
 * that wants to prevent anyone from instantiating it. This is
 * especially useful for classes containing only static methods.
 */
#define CC_DISALLOW_IMPLICIT_CONSTRUCTORS(TypeName) \
    TypeName();                                     \
    CC_DISALLOW_COPY_AND_ASSIGN(TypeName)

/** @def AX_DEPRECATED_ATTRIBUTE
 * Only certain compilers support __attribute__((deprecated)).
 */
// #if defined(__GNUC__) && ((__GNUC__ >= 4) || ((__GNUC__ == 3) && (__GNUC_MINOR__ >= 1)))
// #    define CC_DEPRECATED_ATTRIBUTE __attribute__((deprecated))
// #elif _MSC_VER >= 1400  // vs 2005 or higher
// #    define CC_DEPRECATED_ATTRIBUTE __declspec(deprecated)
// #else
// #    define CC_DEPRECATED_ATTRIBUTE
// #endif

/** @def AX_DEPRECATED(...)
 * Macro to mark things deprecated as of a particular version
 * can be used with arbitrary parameters which are thrown away.
 * e.g. AX_DEPRECATED(4.0) or AX_DEPRECATED(4.0, "not going to need this anymore") etc.
 */
// #define CC_DEPRECATED(...) CC_DEPRECATED_ATTRIBUTE


#ifdef __GNUC__
#    define CC_UNUSED __attribute__((unused))
#else
#    define CC_UNUSED
#endif

/** @def AX_REQUIRES_NULL_TERMINATION
 *
 */
// #if !defined(AX_REQUIRES_NULL_TERMINATION)
// #    if defined(__APPLE_AX__) && (__APPLE_AX__ >= 5549)
// #        define AX_REQUIRES_NULL_TERMINATION __attribute__((sentinel(0, 1)))
// #    elif defined(__GNUC__)
// #        define AX_REQUIRES_NULL_TERMINATION __attribute__((sentinel))
// #    else
// #        define AX_REQUIRES_NULL_TERMINATION
// #    endif
// #endif

// compatibility with non-clang compilers...
#ifndef __has_attribute
#    define __has_attribute(x) 0
#endif
#ifndef __has_builtin
#    define __has_builtin(x) 0
#endif

/*
 * helps the compiler's optimizer predicting branches
 */
// #if __has_builtin(__builtin_expect)
// #    ifdef __cplusplus
// #        define UTILS_LIKELY(exp) (__builtin_expect(!!(exp), true))
// #        define UTILS_UNLIKELY(exp) (__builtin_expect(!!(exp), false))
// #    else
// #        define UTILS_LIKELY(exp) (__builtin_expect(!!(exp), 1))
// #        define UTILS_UNLIKELY(exp) (__builtin_expect(!!(exp), 0))
// #    endif
// #else
// #    define UTILS_LIKELY(exp) (!!(exp))
// #    define UTILS_UNLIKELY(exp) (!!(exp))
// #endif


 /* Define NULL pointer value */
// #ifndef NULL
// #    ifdef __cplusplus
// #        define NULL 0
// #    else
// #        define NULL ((void*)0)
// #    endif
// #endif

#endif  // __CC_PLATFORM_MACROS_H__
