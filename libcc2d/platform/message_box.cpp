#include "message_box.h"
#include "string_fmt.h"
#include "platform.h"

#if defined(CC_WIN32)
#include "win32.h"
#include "unicode32.h"
#else
#include "jni_helper.h"
#endif

void cc2d::messageBox(const char* caption, const char* format, ...) {
    va_list args;
    va_start(args, format);
    auto message = cc2d::format(format, args);
    va_end(args);
#if defined(CC_WIN32)
    MessageBox(nullptr, Charset::toUnicode(true, caption).c_str(), Charset::toUnicode(true, message.c_str()).c_str(), MB_OK);
#else
    JniHelper::callStaticVoidMethod("org.cc2d.lib.ccEngine", "showDialog", caption, message.c_str());
#endif
}
