#include "input.h"
#include "cxx_stl.h"
#include "extern2d.h"
#include "platform.h"
#include "charset.h"
#include "logging.h"

Vec2f g_mouse;
Input g_input;

void Input::reset() {
    _mouse = MOUSE_COUNT;
    _keycode = KEY_NONE;
    _action = INPUT_COUNT;

    memset(_codes, 0, sizeof(_codes));
    memset(_locks, 0, sizeof(_locks));
}

void Input::acquire(float x, float y) {
	g_mouse.x = x * g_scale;
	g_mouse.y = y * g_scale;
}

void Input::acquire(MOUSE_TYPE mouse, INPUT_ACTION_TYPE action) {
    _codes[_mouse = mouse][_action = action] = 1;
}

void Input::acquire(INPUT_ACTION_TYPE left_action) {
    _codes[_mouse = MOUSE_LEFT][_action = left_action] = 1;
}


void Input::acquire(KEY_CODE_TYPE keycode, INPUT_ACTION_TYPE action) {
    if (action == INPUT_PRESS) {
        switch (keycode) {
        case KEY_CODE_TYPE::KEY_BACKSPACE:
            // IMEDispatcher::sharedDispatcher()->dispatchDeleteBackward();
            break;
        case KEY_CODE_TYPE::KEY_HOME:
        case KEY_CODE_TYPE::KEY_KP_HOME:
        case KEY_CODE_TYPE::KEY_DELETE:
        case KEY_CODE_TYPE::KEY_KP_DELETE:
        case KEY_CODE_TYPE::KEY_END:
        case KEY_CODE_TYPE::KEY_LEFT_ARROW:
        case KEY_CODE_TYPE::KEY_RIGHT_ARROW:
        case KEY_CODE_TYPE::KEY_ESCAPE:
            // IMEDispatcher::sharedDispatcher()->dispatchControlKey(g_keyCodeMap[key]);
            break;
        default:
            break;
        }
    }
    _codes[_keycode = keycode][_action = action] = 1;
}


void Input::acquire(int char_code) {
	std::string utf8String;
	Charset::UTF32ToUTF8(std::u32string_view{ (char32_t*)&char_code, (size_t)1 }, utf8String);
	static std::unordered_set<std::string_view> controlUnicode = {
		"\xEF\x9C\x80",  // up
		"\xEF\x9C\x81",  // down
		"\xEF\x9C\x82",  // left
		"\xEF\x9C\x83",  // right
		"\xEF\x9C\xA8",  // delete
		"\xEF\x9C\xA9",  // home
		"\xEF\x9C\xAB",  // end
		"\xEF\x9C\xAC",  // pageup
		"\xEF\x9C\xAD",  // pagedown
		"\xEF\x9C\xB9"   // clear
	};
	// Check for send control key
	if (controlUnicode.find(utf8String) == controlUnicode.end()) {
		// IMEDispatcher::sharedDispatcher()->dispatchInsertText(utf8String.c_str(), utf8String.size());
	}

}



void Input::lock(MOUSE_TYPE mouse, bool lock /*= true*/) {
    _locks[mouse] = lock;
}

void Input::lock(KEY_CODE_TYPE keycode, bool lock /*= true*/) {
    _locks[keycode] = lock;
}

bool Input::isDown(MOUSE_TYPE mouse) const {
    return _codes[mouse][INPUT_PRESS] && !_locks[mouse];
}

bool Input::isDown(KEY_CODE_TYPE keycode) const {
    return _codes[keycode][INPUT_PRESS] && !_locks[keycode];
}

bool Input::isUp(MOUSE_TYPE mouse) const {
    return _codes[mouse][INPUT_RELEASE];
}

bool Input::isKeep(MOUSE_TYPE mouse) const {
    return _codes[mouse][INPUT_RELEASE];
}
