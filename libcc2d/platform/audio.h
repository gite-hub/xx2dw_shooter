#pragma once
#include "cxx_stl.h"
#include "cxx_pod.h"

class Bass {
public:
	Bass();
	~Bass();

	void init();

	void setVolume(bool wav_or_mp3, int volume_0_100);

	int getVolume(bool wav_or_mp3) const;

	void playWav(const char* ptr, int size);

	void playMp3(const char* ptr, int size);

	bool isPlayed() const;

	void replay();

private:
	bool _inited = false;
	unsigned long _hstream = 0;
};

extern Bass g_bass;


class VolumeOperator {
public:
	virtual void setVolume(int volume_0_100);

	int getVolume() const;

protected:
	int _volume_0_100 = 100;
};



class Wav : public VolumeOperator {
public:
	~Wav();

	void setVolume(int volume_0_100) override;

	void play(uint uid);

};
extern Wav g_wav;


class Mp3 : public VolumeOperator {
public:
	~Mp3();

	void setVolume(int volume_0_100) override;

	void clearPaths();
	void addPath(cstr_t path);

	void play(uint uid);
	bool play(std::string filename);



private:
	std::vector<std::string> _paths;
	std::map<uint, std::string> _filenames_map;

	uint _uid = k0u;
	std::string _filename;
};

extern Mp3 g_mp3;
