#pragma once
#include "cxx_t.h"
#include "vec2.h"
#include "keycode.h"

enum MOUSE_TYPE { MOUSE_LEFT = 256, MOUSE_RIGHT, MOUSE_COUNT };
enum INPUT_ACTION_TYPE { INPUT_PRESS, INPUT_RELEASE, INPUT_REPEAT, INPUT_COUNT };
class Input {
public:
	void reset();
	void acquire(float x, float y);
	void acquire(MOUSE_TYPE mouse, INPUT_ACTION_TYPE action);
	void acquire(INPUT_ACTION_TYPE left_action);
    void acquire(KEY_CODE_TYPE keycode, INPUT_ACTION_TYPE action);
    void acquire(int char_code);
	void lock(MOUSE_TYPE mouse, bool lock = true);
    void lock(KEY_CODE_TYPE keycode, bool lock = true);
	bool isDown(MOUSE_TYPE mouse) const;
    bool isDown(KEY_CODE_TYPE mouse) const;
	bool isUp(MOUSE_TYPE mouse) const;
    bool isKeep(MOUSE_TYPE mouse) const;

private:
    uchar _codes[MOUSE_COUNT][INPUT_COUNT];
    uchar _locks[MOUSE_COUNT];
    MOUSE_TYPE _mouse;
    KEY_CODE_TYPE _keycode;
    INPUT_ACTION_TYPE _action;
};
extern Vec2f g_mouse;
extern Input g_input;
