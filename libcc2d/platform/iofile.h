#pragma once
#include "cxx.h"
#include <fstream>
#include <sstream>

class IFile {
public:
	struct Handle {
		std::ifstream* ifile;
		void* asset;
		Handle();
		~Handle();
		operator bool() const;
	};

    static void setAssetManager(void* assetmanager, cstr_t asset_path);

	static void setPaths(const std::vector<std::string>& paths);

	static void ignorePathsFlag();

	static char* readText(cstr_t filename, int* psize = nullptr);

	static char* read(cstr_t filename, bool binary, int* psize);

	static char* read(cstr_t filename, int* psize);

	static char* read(cstr_t filename, int position, int size);

	IFile();
	~IFile();

    static bool open(cstr_t filename, std::istringstream& istr);

	static bool open(cstr_t filename, bool binary, Handle& handle);

	bool open(cstr_t filename, bool binary = true);

	static bool isOpend(Handle& handle);
	bool isOpend() const;

	static int tellg(Handle& handle);
	int tellg();

	static int getSize(Handle& handle);
	int getSize();

	static void seekSet(Handle& handle, int position);
	void seekSet(int position = 0);
	static void seek(Handle& handle, int position);
	void seek(int position);
	static void seekEnd(Handle& handle, int position);
	void seekEnd(int position = 0);

	static void read(Handle& handle, void* ptr, int size);
	IFile& read(void* ptr, int size);

	template<typename T>
	static void read(Handle& handle, T& value) { return read(handle, &value, sizeof(value)); }
	template<typename T>
	IFile& operator>>(T& value) { read(_handle, &value, sizeof(value)); return *this; }

	static void close(Handle& handle);
	void close();
private:
	Handle _handle;
};


class OFile {
public:
	static bool write(cstr_t filename, bool binary, const char* pdata, int size);
	static bool write(cstr_t filename, const char* pdata, int size);
	static bool writeText(cstr_t filename, const char* pdata, int size);
};
