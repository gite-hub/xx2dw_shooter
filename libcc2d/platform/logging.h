#ifndef __CC_LOG_MACROS_H__
#define __CC_LOG_MACROS_H__

#include "platform.h"
#include "string_fmt.h"
#if defined(CC_WIN32) && defined(CC_DEBUG)
#include "unicode32.h"
#include <crtdbg.h>
#endif

namespace cc2d {

	enum LOG_LEVEL { LOG_DEFAULT, LOG_INFO, LOG_WARN, LOG_ERROR };

	void print(LOG_LEVEL level, const std::string& message);
	void print(const char* format, ...) CC_FORMAT_PRINTF(1, 2);
	void print(LOG_LEVEL level, const char* format, ...);

}


#define CC_LOG_FUNC(s, ...) cc2d::print(cc2d::LOG_WARN, "%s : %s", __FUNCTION__, toFormat(s, ##__VA_ARGS__).c_str())

#if !defined(CC_DEBUG) || CC_DEBUG == 0
#define CC_LOG(...) do { } while (0)
#define CC_LOG_INFO CC_LOG
#define CC_LOG_WARN CC_LOG
#define CC_LOG_ERROR CC_LOG

#elif CC_DEBUG == 1
#define CC_LOG(format, ...) cc2d::print(cc2d::LOG_DEFAULT, format, ##__VA_ARGS__)
#define CC_LOG_INFO(format, ...) do { } while (0)
#define CC_LOG_WARN CC_LOG_FUNC
#define CC_LOG_ERROR(format, ...) cc2d::print(cc2d::LOG_ERROR, format, ##__VA_ARGS__)

#elif CC_DEBUG > 1
#define CC_LOG(format, ...) cc2d::print(cc2d::LOG_DEFAULT, format, ##__VA_ARGS__)
#define CC_LOG_INFO(format, ...) cc2d::print(cc2d::LOG_INFO, format, ##__VA_ARGS__)
#define CC_LOG_WARN CC_LOG_FUNC
#define CC_LOG_ERROR(format, ...) cc2d::print(cc2d::LOG_ERROR, format, ##__VA_ARGS__)
#endif  // _AX_DEBUG



#if defined(CC_WIN32) && defined(CC_DEBUG)
#define CC_LOG(format, ...) do {\
auto wstr = Charset::toUnicode(true, format, ##__VA_ARGS__) + L"\n";\
auto file = Charset::toUnicode(true, __FILE__);\
_CrtDbgReportW(_CRT_WARN, file.c_str(), __LINE__, L"", wstr.c_str()); } while(0)
#endif


/** Lua engine debug */
// #if !defined(_AX_DEBUG) || _AX_DEBUG == 0 || AX_LUA_ENGINE_DEBUG == 0
// #    define LUALOG(...)
// #else
// #    define LUALOG(format, ...) ax::print(format, ##__VA_ARGS__)
// #endif  // Lua engine debug

//  end of debug group
/// @}


#endif
