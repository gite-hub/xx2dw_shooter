#include "audio.h"
#include "wdfp.h"
#include "iofile.h"
#include "string_fmt.h"
#include "bass/bass.h"


Bass g_bass;
Wav g_wav;
Mp3 g_mp3;


Bass::Bass() {
    _inited = false;
}

Bass::~Bass() {
    BASS_Free();
}

void Bass::init() {
	if (!_inited && BASS_Init(-1, 44100, 0, nullptr, nullptr)) {
		_inited = true;
	}
}

void Bass::setVolume(bool wav_or_mp3, int volume_0_100) {
	BASS_SetConfig(wav_or_mp3 ? BASS_CONFIG_GVOL_SAMPLE : BASS_CONFIG_GVOL_STREAM, volume_0_100 * 100);
}

int Bass::getVolume(bool wav_or_mp3) const {
	return BASS_GetConfig(wav_or_mp3 ? BASS_CONFIG_GVOL_SAMPLE : BASS_CONFIG_GVOL_STREAM) / 100;
}


void Bass::playWav(const char* ptr, int size) {
    init();
	// max 最大通道数
	// BASS_SAMPLE_MONO 单声道
	auto hsample = BASS_SampleLoad(TRUE, ptr, 0, size, 16, BASS_SAMPLE_MONO);
	if (hsample) {
		// TRUE 总是新的通道
		// FALSE 利用空闲的通道
		auto channel = BASS_SampleGetChannel(hsample, FALSE);
		if (channel) {
			if (BASS_ChannelPlay(channel, TRUE)) {
                // 安卓不加这个就慢吞吞
				BASS_ChannelSetAttribute(channel, BASS_ATTRIB_FREQ, 44100);
			} else {
				BASS_ChannelFree(channel);
				BASS_SampleFree(hsample);
			}
		} else {
			BASS_SampleFree(hsample);
		}
	}
}

void Bass::playMp3(const char* ptr, int size) {
    init();
	if (_hstream) {
		BASS_StreamFree(_hstream);
	}
	// BASS_STREAM_AUTOFREE | BASS_MUSIC_LOOP
	_hstream =  BASS_StreamCreateFile(TRUE, ptr, 0, size, 0);
	if (_hstream) {
		if (BASS_ChannelPlay(_hstream, FALSE)) {
		} else {
			BASS_StreamFree(_hstream);
		}
	}
}


bool Bass::isPlayed() const {
	return (!_hstream) || BASS_ChannelIsActive(_hstream) == BASS_ACTIVE_STOPPED;
}


void Bass::replay() {
	if (_hstream) {
		BASS_ChannelPlay(_hstream, TRUE);
	}
#if 0
	BASS_CHANNELINFO info;
	BASS_ChannelGetInfo(handle, &info);
	if (loop) {
		info.flags |= BASS_MUSIC_LOOP;
	} else {
		info.flags &= ~BASS_MUSIC_LOOP;
	}
	BASS_ChannelFlags(handle, info.flags, 0);
	//	BASS_ChannelSetAttribute(handle, BASS_ATTRIB_VOL, BASS_GetConfig(BASS_CONFIG_GVOL_SAMPLE) / 100.0f);
	BASS_ChannelPlay(handle, TRUE);
#endif
}


//////////////////////////////////////////////////////////////////////////
void VolumeOperator::setVolume(int volume_0_100) {
	_volume_0_100 = volume_0_100;
}

int VolumeOperator::getVolume() const {
	return _volume_0_100;
}



//////////////////////////////////////////////////////////////////////////
Wav::~Wav() {

}

void Wav::setVolume(int volume_0_100) {
	VolumeOperator::setVolume(volume_0_100);
	g_bass.setVolume(true, volume_0_100);
}


void Wav::play(uint uid) {
	if (getVolume() == 0) {
		return;
	}
	int size;
	auto ptr = WDFGroup::getWav()->getData(uid, &size);
	g_bass.playWav(ptr, size);
}



//////////////////////////////////////////////////////////////////////////
Mp3::~Mp3() {

}

void Mp3::setVolume(int volume_0_100) {
	VolumeOperator::setVolume(volume_0_100);
	g_bass.setVolume(false, volume_0_100);
}


void Mp3::clearPaths() {
	_paths.clear();
}

void Mp3::addPath(cstr_t path) {
	_paths.emplace_back(path);
}


void Mp3::play(uint uid) {
	if (_uid == uid) {
		return;
	}
	_uid = uid;
	const auto& it = _filenames_map.find(uid);
	if (it != _filenames_map.end()) {
		if (it->second == _filename) {
			return;
		}
		_filename = it->second;
		play(_filename);
		return;
	}
	_filename.clear();
	int length;
	auto ptr = WDFGroup::getMp3()->getData(uid, &length);
	if (ptr != nullptr) {
		g_bass.playMp3(ptr, length);
	} else {
		for (auto path : _paths) {
			path += cc2d::format("%08X.mp3", uid);
			if (play(path)) {
				_filename = path;
				_filenames_map.emplace(uid, path);
				return;
			}
		}
	}
}



bool Mp3::play(std::string filename) {
	int size;
	auto ptr = IFile::read(filename, &size);
	if (ptr != nullptr) {
		g_bass.playMp3(ptr, size);
		return true;
	}
	return false;
}

