#pragma once
#include "cxx_stl.h"
#include "keycode.h"

namespace cc2d {

    void messageBox(const char* caption, const char* format, ...);
}
