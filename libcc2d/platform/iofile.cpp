#include "iofile.h"
#include "platform.h"
#include "logging.h"

#if defined(CC_ANDROID)
#include "android/asset_manager.h"
static AAssetManager* s_asset_manager = nullptr;
static std::string s_asset_path;
#else
#include "unicode32.h"
#endif

static std::vector<std::string> s_paths, s_empty_paths = { k_str };
static bool s_ignore_paths = false;

IFile::Handle::Handle() {
	ifile = nullptr;
	asset = nullptr;
}

IFile::Handle::~Handle() {
	IFile::close(*this);
}

IFile::Handle::operator bool() const {
	return ifile != nullptr || asset != nullptr;
}


void IFile::setAssetManager(void* assetmanager, cstr_t asset_path){
#if defined(CC_ANDROID)
    s_asset_manager = (AAssetManager*)assetmanager;
    CC_LOG("setAssetManager %p", assetmanager);
#endif
    CC_LOG("setAssetManager %s", asset_path.c_str());
    s_empty_paths[0] = asset_path;
    if (!FOUND(s_paths, asset_path)) {
        s_paths.emplace_back(asset_path);
    }
}

void IFile::setPaths(const std::vector<std::string>& paths) {
	s_paths = paths;
	if (!s_empty_paths[0].empty() && !FOUND(s_paths, s_empty_paths[0])) {
		s_paths.emplace_back(s_empty_paths[0]);
	}
}

void IFile::ignorePathsFlag() {
	s_ignore_paths = true;
}

char* IFile::readText(cstr_t filename, int* psize /*= nullptr*/) {
	return read(filename, false, psize);
}

char* IFile::read(cstr_t filename, bool binary, int* psize) {
	Handle handle;
	if (!open(filename, binary, handle)) {
		return nullptr;
	}
	int size = getSize(handle);
	auto ptr = new char[size + 1];
	ptr[size] = '\0';
	read(handle, ptr, size);
	close(handle);
	if (psize != nullptr) {
		*psize = size;
	}
	return ptr;
}

char* IFile::read(cstr_t filename, int* psize /*= nullptr*/) {
	return read(filename, true, psize);
}


char* IFile::read(cstr_t filename, int position, int size) {
	Handle handle;
	if (!open(filename, true, handle)) {
		return nullptr;
	}
	auto ptr = new char[size + 1];
	ptr[size] = '\0';
	seekSet(handle, position);
	read(handle, ptr, size);
	close(handle);
	return ptr;
}



IFile::IFile() {

}

IFile::~IFile() {
}


bool IFile::open(cstr_t filename, std::istringstream& istr) {
    Handle handle;
    auto correct = filename;
    for (auto& c : correct) {
        if (c == '\\') {
            c = '/';
        }
    }
    forr(correct, i) {
        if (correct[i] == '/' && correct[i - 1] == '/') {
            correct.erase(i, 1);
        }
    }
    istr.clear();
    if (!open(correct, false, handle)) {
        istr.str(k_str);
        return false;
    }
    int size = IFile::getSize(handle);
    auto ptr = new char[size + 1];
    ptr[size] = '\0';
    IFile::read(handle, ptr, size);
    IFile::close(handle);
    istr.str(ptr);
    return true;
}


bool IFile::open(cstr_t filename, bool binary, Handle& handle) {
#if defined(CC_ANDROID)
	handle.asset = AAssetManager_open(s_asset_manager, (/*s_asset_path + */filename).c_str(), AASSET_MODE_STREAMING);
    // CC_LOG("AAssetManager_open %s --> %p", filename.c_str(), handle.asset);
#endif
	if (handle.asset != nullptr) {
		s_ignore_paths = false;
		return true;
	}
	handle.ifile = new std::ifstream();
    const auto& paths =
        (filename.find(":") != std::string::npos) ?
        std::vector<std::string> { k_str } :
        (s_ignore_paths ? s_empty_paths : s_paths);
    std::string name;
	if (binary) {
		for (const auto& path : paths) {
            name = path + filename;
#if defined(CC_WIN32)
            // name = Charset::toAscii(false, Charset::toUnicode(true, (path + filename).c_str()));  
#endif
            CC_LOG("std::ifstream::open %s", name.c_str());
			handle.ifile->open(name, std::ios::binary);
			if (handle.ifile->is_open()) {
				s_ignore_paths = false;
				return true;
			}
		}
	} else {
		for (const auto& path : paths) {
            name = path + filename;
#if defined(CC_WIN32)
            // name = Charset::toAscii(false, Charset::toUnicode(true, (path + filename).c_str()));     
#endif
            CC_LOG("std::ifstream::open %s", name.c_str());
			handle.ifile->open(name);
			if (handle.ifile->is_open()) {
				s_ignore_paths = false;
				return true;
			}
		}
	}
	delete handle.ifile;
	handle.ifile = nullptr;
	s_ignore_paths = false;
	return false;
}


bool IFile::open(cstr_t filename, bool binary) {
	 open(filename, binary, _handle);
	return isOpend();
}


bool IFile::isOpend(Handle& handle) {
	return handle;
}

bool IFile::isOpend() const {
	return _handle;
}

int IFile::tellg(Handle& handle) {
	if (handle.ifile) {
		return handle.ifile->tellg();
	}
#if defined(CC_ANDROID)
	return AAsset_getLength64((AAsset*)handle.asset);
#endif
}

int IFile::tellg() {
	return tellg(_handle);
}

int IFile::getSize(Handle& handle) {
	if (handle.ifile) {
		int temp = tellg(handle);
		seekEnd(handle, 0);
		int size = tellg(handle);
		seekSet(handle, temp);
		return size;
	}
#if defined(CC_ANDROID)
	return AAsset_getLength64((AAsset*)handle.asset);
#endif
}
int IFile::getSize() {
	return getSize(_handle);
}

void IFile::seekSet(Handle& handle, int position) {
	if (handle.ifile) {
		handle.ifile->seekg(position, std::ios::beg);
	} else {
#if defined(CC_ANDROID)
		AAsset_seek64((AAsset*)handle.asset, position, 0);
#endif
	}
}
void IFile::seekSet(int position) {
	seekSet(_handle, position);
}
void IFile::seek(Handle& handle, int position) {
	if (handle.ifile) {
		handle.ifile->seekg(position, std::ios::cur);
	} else {
#if defined(CC_ANDROID)
		AAsset_seek64((AAsset*)handle.asset, position, 1);
#endif
	}
}
void IFile::seek(int position) {
	seek(_handle, position);
}
void IFile::seekEnd(Handle& handle, int position) {
	if (handle.ifile) {
		handle.ifile->seekg(position, std::ios::end);
	} else {
#if defined(CC_ANDROID)
		AAsset_seek64((AAsset*)handle.asset, position, 2);
#endif
	}
}

void IFile::seekEnd(int position) {
	seekEnd(_handle, position);
}

void IFile::read(Handle& handle, void* ptr, int size) {
	if (handle.ifile) {
		handle.ifile->read((char*)ptr, size);
	} else {
#if defined(CC_ANDROID)
		AAsset_read((AAsset*)handle.asset, ptr, size);
#endif
	}
}

IFile& IFile::read(void* ptr, int size) {
	read(_handle, ptr, size);
	return *this;
}

void IFile::close(Handle& handle) {
	if (handle.ifile) {
		handle.ifile->close();
		delete handle.ifile;
		handle.ifile = nullptr;
	} else if (handle.asset){
#if defined(CC_ANDROID)
		AAsset_close((AAsset*)handle.asset);
#endif
		handle.asset = nullptr;
	}
}

void IFile::close() {
	close(_handle);
}



//////////////////////////////////////////////////////////////////////////
bool OFile::write(cstr_t filename, bool binary, const char* pdata, int size) {
	std::ofstream ofile;
	if (binary) {
		ofile.open(filename, std::ios::binary);
	} else {
		ofile.open(filename);
	}
	if (!ofile.is_open()) {
		return false;
	}
	ofile.write(pdata, size);
	ofile.close();
	return true;
}

bool OFile::write(cstr_t filename, const char* pdata, int size) {
	return write(filename, true, pdata, size);
}

bool OFile::writeText(cstr_t filename, const char* pdata, int size) {
	return write(filename, false, pdata, size);
}
