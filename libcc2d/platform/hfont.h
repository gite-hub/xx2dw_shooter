#pragma once
#include "cxx_t.h"

namespace cc2d {
	bool toA8bitmap(int font_size, const char* str8, int& width, int& height, uchar* datas);
    bool toA8bitmap(int font_size, char16_t word, int& width, int& height, uchar* datas);
}
