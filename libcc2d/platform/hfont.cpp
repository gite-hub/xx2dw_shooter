#include "hfont.h"
#include "platform.h"

#if defined(CC_WIN32)
#include "unicode32.h"
#include "hfont32.h"
#else
#include "hfont_jni.h"
#endif

bool cc2d::toA8bitmap(int font_size, const char* str8, int& width, int& height, uchar* datas) {
#if defined(CC_WIN32)
	auto word = (HFontManager::word_t)Charset::toUnicode(str8, true);
	return g_hfont.toData(font_size, word, width, height, datas);
#else
    setBitmapA8pointerJni(datas);
	auto ret = toBitmapA8Jni(font_size, str8, width, height);
    setBitmapA8pointerJni(nullptr);
    return ret;
#endif
}

bool cc2d::toA8bitmap(int font_size, char16_t word, int& width, int& height, uchar* datas) {
#if defined(CC_WIN32)
    return g_hfont.toData(font_size, word, width, height, datas);
#endif
}
