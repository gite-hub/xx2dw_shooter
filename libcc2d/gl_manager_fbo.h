﻿#pragma once
#include "cplus.h"
#include "vertex_inc.h"
#include "colorgb.h"

using gluid_t = uint; // GLuint;
class GLManager {
public:
	GLManager();
	void initGL();
	void premultiplyAlpha(bool premultiply);
	static gluid_t loadShader(uint/*GLenum*/ type, const char* content);
	void initShaders();
	void initPrograms();
	void initUniformAndAttri();
	void initVAO();
	void initFBO();

	bool linkShader(SHADER_TYPE shader_type);
	bool bindVAO(gluid_t vao);
	bool bindVBO(gluid_t vbo);
	bool activeTexture0(gluid_t texture);
	bool activeTexture1(gluid_t texture);


	void render(SHADER_TYPE shader_type, const VertexInstance& instance, gluid_t texture, int width = 0, int height = 0, gluid_t texture_dye = kInvalid);
	void render();

	static gluid_t* generateTextures(int count);
	static gluid_t generateTexture();
	static void toTexture(gluid_t texture, BITMAP_TYPE bitmap_type, int pitched_width, int height, const void* datas);
	static void deleteTexture(gluid_t texture);
	static void deleteTexture(int count, gluid_t* texture);

private:
	std::array<gluid_t, VERTEX_SHADER_COUNT> _vertex_shaders;
	std::array<gluid_t, SHADER_COUNT> _fragment_shaders;

	gluid_t _program;
	gluid_t _texture_fbo;

	gluid_t _uniform_texture;
	gluid_t _uniform_texture_dye_for_fbo;

	gluid_t _attri_xyuv0;
	gluid_t _attri_xyuv1;
	gluid_t _attri_xyuv2;
	gluid_t _attri_xyuv3;
	gluid_t _attri_rgba;

	gluid_t _vao;
	gluid_t _vbo;
	gluid_t _fbo;
	gluid_t _vao_for_fbo;
	gluid_t _vbo_for_fbo;

	VERTEX_SHADER_TYPE _vertex_shader_type;
	SHADER_TYPE _fragment_shader_type;
	SHADER_TYPE _shader_type;

	gluid_t _binding_vao;
	gluid_t _binding_vbo;

	gluid_t _binding_texture;
	// gluid_t _binding_texture_dye_for_fbo;

	int _binding_texture_width;
	int _binding_texture_height;

	bool _use_fbo_this_frame;
	bool _premultiply_alpha;

	std::array <VertexInstance, 16384> _baths;
	int _bath_count;
public:
	static const gluid_t kInvalid;
};
extern GLManager gl_manager;


