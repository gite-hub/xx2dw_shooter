#include <stdio.h>
#include "main.h"
#include "cxx2d.h"
#include "gl_manager.h"
#include "input.h"
#include "audio.h"
#include "scheduler.h"

#include "iofile.h"
#include "wasp.h"
#include "maper.h"
#include "text.h"

#include "logging.h"
#include "charset.h"

Was* was;
Maper* maper;
Label label;
Text text;

auto g_scheduler = Scheduler::getInstance();
auto g_ref_pool = AutoReleasePool::getInstance();

bool cc2d::mainInit(int width, int height) {
#if 0
    g_integer_scale = true;
    g_scale = 1;
    g_height = height / g_scale;
#else
    g_height = height;
    for (int i = 1, h;; i += 1) {
        h = height / i;
        if (h > kMinSolutionHeight * 3 / 2) {
            continue;
        }
        if (h < kMinSolutionHeight) {
            break;
        }
        g_height = h;
    }

    if (g_height > kMinSolutionHeight * 3 / 2) {
        g_scale = 1.5f;
        g_height = height / g_scale;
        g_integer_scale = false;
    } else {
        g_scale = height / g_height;
        g_integer_scale = true;
    }
#endif
    g_width = width / g_scale;

    int hor = width - g_width * g_scale;
    int ver = height - g_height * g_scale;

    CC_LOG("物理宽高 %d %d", width, height);
    CC_LOG("视口宽高 %d %d", width - hor, height - ver);
    CC_LOG("逻辑宽高 %d %d", g_width, g_height);
    CC_LOG("缩放倍数 %.1f", g_scale);


    gl_manager.init(hor / 2, ver / 2, width - hor + hor / 2, height - ver + ver / 2);
    g_input.reset();

    WASGroup::getSmap()->open("smap.wd1", true);

    auto group = WASGroup::getInstance();
    group->open("shape.wdb", false);

    was = group->getWas(0x186B6FD0);

    WDFGroup::getWav()->open("sound.wdf", true);


    maper = g_maper.getMaper(1002, true);


    const char* _中文标识符 = "中文标识符";
    CC_LOG("输出中文:%s", _中文标识符);


    int size;
    auto ptr = IFile::read("D148D483.WAV", &size);
    if (ptr != nullptr) {
        g_bass.playWav(ptr, size);
    }

    g_mp3.play("68F6DDA1.mp3");

    std::u32string str_utf32;
    Charset::UTF8ToUTF32(":*¨༺ ☽.𝐊𝗂𝗋α.☾༻¨*:·", str_utf32);
    std::u16string str_utf16;
    Charset::UTF32ToUTF16(str_utf32, str_utf16);

    // :*¨༺ ☽.𝐊𝗂𝗋α.☾༻¨*:·
    label.setString("问我干啥");
    text.setString("ABC#Rabc#Y123-.\n一我行\n囊饕餮");

    cc2d::createOrthographicOffCenter(0, g_width, g_height, 0);

    return true;
}



void cc2d::mainLoop(float dt) {
    g_dt = dt;
    g_scheduler->update(dt);
    // CC_LOG("%f", g_dt);

    if (g_input.isDown(MOUSE_LEFT)) {
        g_wav.play(0x02EF9247);
    } else if (g_input.isUp(MOUSE_LEFT)) {
        // g_mp3.play("68F6DDA1.mp3");
    }
    if (maper) {
        maper->render(-640, -640, 2, 5, 2, 4);
        maper->renderAlphas(0, 0, 0, 0, 0, 0);
    }

    static int iframe;
    static DeltaProtocol delta;
    if (delta.step(g_dt, DeltaProtocol::kFrame)) {
        iframe += 1;
    }
    was->render(iframe % was->frames_count_per_direction, 200, 200/*, 384, 256, 32*/);

//     TextureImpl::render(bitmap_font.getTexture(), SHADER_A8, COLOR::YELLOW, g_width / 2, g_height / 2,
//         bitmap_font.width, bitmap_font.height, bitmap_font.width, bitmap_font.height);

    label.render(10, 30);
    text.render(100, 30);


    gl_manager.render();
    g_input.reset();
    g_ref_pool->clear();
}
