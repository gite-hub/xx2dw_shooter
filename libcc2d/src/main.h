#pragma once

namespace cc2d {

	bool mainInit(int width, int height);

	void mainLoop(float dt);

}
