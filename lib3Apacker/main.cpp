﻿#include <stdio.h>
#include <io.h>
#include <direct.h>
#include <string>
// #include <set>
#include <vector>
#include <Windows.h>

// #define cc_only_check

static long long total_size, file_size;
static int file_small, file_big;
static const char gang = '\\';
static std::string path_root, exec, packer, str_temp, path_temp;
// std::set<std::string> mkdirs;

static inline bool isGang(char c) {
	return c == '/' || c == '\\';
}

void CreateDir(const std::string& directoryPath) {
	std::string tmpDirPath;
	for (uint32_t i = 0; i < directoryPath.size(); ++i) {
		tmpDirPath.push_back(directoryPath[i]);

		// 检查是否需要创建目录
		if (tmpDirPath[i] == '/' || tmpDirPath[i] == '\\') {
			// 检查目录是否已存在
			if (_access(tmpDirPath.c_str(), 0)) {
				// 创建目录
				int32_t ret = _mkdir(tmpDirPath.c_str());
				if (ret != 0) {
					// 创建目录失败，退出函数
					return;
				}
			}
		}
	}
	// 创建完整目录路径
	_mkdir(tmpDirPath.c_str());
}


// 字符串 分割
static void split(const std::string& str, const std::string& delim, std::vector<std::string>& ret) {
	ret.clear();
	size_t last = 0;
	size_t index = str.find_first_of(delim, last);

	if (index == std::string::npos) {
		ret.push_back(str);
		return;
	}

	while (index != std::string::npos) {
		ret.push_back(str.substr(last, index - last));
		last = index + 1;
		index = str.find_first_of(delim, last);
	}

	if (index - last > 0) {
		ret.push_back(str.substr(last, index - last));
	}
}

static void toMove(const std::string& path, std::string& path4move) {
	static std::vector<std::string> str_temps;
	split(path, "/\\", str_temps);
	path4move.clear();
	for (auto& str : str_temps) {
		if (str.find(" ") != std::string::npos || str.find("&") != std::string::npos) {
			str = "\"" + str + "\"";
		}
		path4move += str + gang;
	}
	if (path.back() != gang) {
		path4move.pop_back();
	}
}

// 文件超4G
static _finddatai64_t file_data;

void recurMove(const std::string& path) {
	if (path.find(packer) != std::string::npos) {
		return;
	}
	str_temp = path_root + path + "*";
	auto handle = _findfirsti64(str_temp.c_str(), &file_data);
	if (handle == -1L) {
		return;
	}

	do {

		if (file_data.attrib & _A_SUBDIR) {
			if (strcmp(file_data.name, ".") && strcmp(file_data.name, "..")) {
				recurMove(path + file_data.name + gang);
			}
		} else if (std::string(file_data.name).find(exec) != std::string::npos) {
		} else if (file_data.size >= file_size) {
			file_big += 1;
		} else {
#ifdef cc_only_check
#else
			path_temp = path + file_data.name;
			while (!path_temp.empty()) {
				path_temp.pop_back();
				if (!path_temp.empty() && isGang(path_temp.back())) {
					path_temp.pop_back();
					break;
				}
			}
// 			if (mkdirs.find(path_temp) == mkdirs.end()) {
// 				mkdirs.insert(path_temp);
// 				str_temp = "md " + path_root + packer + gang + path_temp;
// 				system(str_temp.c_str());
// 			}
			CreateDir(path_root + packer + gang + path_temp);

			toMove(path_root + path + file_data.name, str_temp);
			toMove(path_root + packer + gang + path_temp, path_temp);

			str_temp = "move " + str_temp + " " + path_temp;
			system(str_temp.c_str());


			// MoveFile(str_temp.c_str(), path_temp.c_str());

#endif

			printf("%s%s\n", path.c_str(), file_data.name);
			total_size += file_data.size;
			file_small += 1;
		}
	} while (!_findnexti64(handle, &file_data));
	_findclose(handle);

	str_temp = path_root + path + "*";
	handle = _findfirsti64(str_temp.c_str(), &file_data);
	if (handle != -1L) {
		int sub = 0;
		do {
			if (file_data.attrib & _A_SUBDIR) {
				if (strcmp(file_data.name, ".") && strcmp(file_data.name, "..")) {
					sub += 1;
				}
			} else  {
				sub += 1;
			}
		} while (!_findnexti64(handle, &file_data));
		_findclose(handle);
		if (sub == 0) {
			RemoveDirectory((path_root + path).c_str());
		}
	}
}




int main() {
	path_temp = __argv[0];
	while (true) {
		path_temp.pop_back();
		if (!path_temp.empty() && isGang(path_temp.back())) {
			// path_temp.pop_back();
			break;
		}
	}
	printf("输入大小(兆):");
	int temp;
	scanf("%d", &temp);
	file_size = temp * 1048576LL;

	path_root = path_temp;
	path_temp = __argv[0];
	exec = path_temp.substr(path_root.length());
	packer = exec.substr(0, exec.length() - 4);
	
	if (true){
		exec = packer + "_check.exe";
		std::vector<std::string> ret;
		split(exec, "_", ret);
		exec = ret.front();
	}

	total_size = 0;
	file_small = file_big = 0;

	recurMove("");

	temp = total_size / 1048576;
	printf("小文件%d个, 共%d兆, 大文件%d个\n", file_small, temp, file_big);
	for (;;);
	return 0;
}