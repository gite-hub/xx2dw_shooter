package com.example.udcim;

import static java.security.AccessController.getContext;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.widget.CompoundButton;
import android.widget.Switch;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static void nativePermissionResult(int requestCode, boolean result){

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, 1);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && !Environment.isExternalStorageManager()) {
            // 方案一：跳转到系统文件访问页面，手动赋予
            Intent intent = getIntent();
            intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
            intent.setData(Uri.parse("package:" + this.getPackageName()));
            startActivityForResult(intent, 2);
        }

        Switch sw = findViewById(R.id.switch1);

        String pictures = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Pictures/";
        String dcim = pictures + "dcim";
        sw.setChecked(new File(dcim).exists());
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String _dcim = pictures + ".dcim";
                if (sw.isChecked()) {
                    File file = new File(_dcim);
                    if (file.exists()){
                        file.renameTo(new File(dcim));
                    }
                } else {
                    File file = new File(dcim);
                    if (file.exists()){
                        file.renameTo(new File(_dcim));
                    }
                }
            }
        });
    }

    public void requestPermission(String permission, int requestCode) {
        if (Build.VERSION.SDK_INT < 23 /* Android 6.0 (M) */) {
            nativePermissionResult(requestCode, true);
            return;
        }

        Activity activity = this;
        if (activity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            activity.requestPermissions(new String[]{permission}, requestCode);
        } else {
            nativePermissionResult(requestCode, true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean result = (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED);
        nativePermissionResult(requestCode, result);
    }
}