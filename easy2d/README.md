# easy2d

![](demo.png)

lua5.4

目前引擎仅支持png图片和wav音效

推荐编辑器： vscode

https://code.visualstudio.com/Download

# 如何启动

### 方案1.运行 easy.exe

默认启动 main.lua 主脚本

### 方案2.用命令

打开 cmd 或者 powershell 或者任何terminal控制台，输入命令行：

easy.exe xxx
或者
./easy xxx

其中xxx 就是启动的xxx.lua 主脚本


### 方案3.bat

新建记事本，编辑如下内容：

%~dp0easy.exe xxx

写完保存，后缀txt改成bat，双击运行bat
