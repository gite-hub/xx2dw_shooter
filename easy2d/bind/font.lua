-- 文字类
local Font = require('class')()
local nullptr = 0

-- 创建(字号)
function Font:init(font_size)
	if math.type(font_size) ~= 'integer' then error('Font size error') end
	self._font = font_create(font_size)
	if self._font == nullptr then error('Font.font nullptr') end
	self._fontsize = font_size
	self._text = ''
	self._lock = 0
	self._x = 0
	self._y = 0
	self._color = 0xFFFFFFFF
	self._shadow = false
end


-- 文字
function Font:setString(text)
	if type(text) ~= 'string' then error('Font:setString param error') end
	self._text = text
	font_string(self._font, text)
end


function Font:getString(text)
	return self._text
end

-- 颜色(ARGB像素值)
function Font:setColor(color)
	self._color = color
end

function Font:getColor()
	return self._color
end

-- 坐标(x, y)
function Font:setPosition(x, y)
    self._x = x
	self._y = y
end

function Font:setPositionX(x)
    self._x = x
end

function Font:setPositionY(y)
    self._y = y
end

function Font:getPosition()
    return self._x, self._y
end

function Font:getPositionX()
    return self._x
end

function Font:getPositionY()
    return self._y
end


-- 宽高
function Font:getSize()
	return font_getw(self._font), font_geth(self._font)
end


function Font:getWidth()
	return font_getw(self._font)
end

function Font:getHeight()
	return font_geth(self._font)
end


-- 阴影(true/false)
function Font:setShadow(shadow)
    self._shadow = shadow
end


function Font:isShadow()
    return self._shadow
end


-- 渲染文字
function Font:render()
	if type(self._x) ~= 'number' then error('Font.x error') end
    if type(self._y) ~= 'number' then error('Font.y error') end
    if math.type(self._color) ~= 'integer' then error('Font.color error') end
	if type(self._shadow) ~= 'boolean' then error('Font.shadow error') end
	font_render(self._font, self._x, self._y, self._color, self._shadow)
end


-- 销毁
function Font:GC()
	if math.type(self._font) == 'integer' and self._font ~= nullptr then
		font_release(self._font)
		self._font = nullptr
	end
end



function Font:汉化()

	self.设字号 = self.setFont
	self.取字号 = self.getFont
	--self.设最大宽度 = self.setLock
	--self.取最大宽度 = self.getLock
	self.设文字 = self.setString
	self.取文字 = self.getString
	self.设颜色 = self.setColor
	self.取颜色 = self.getColor
    self.设坐标 = self.setPosition
    self.设坐标x = self.setPositionX
    self.设坐标y = self.setPositionY
    self.取坐标 = self.getPosition
    self.取坐标x = self.getPositionX
    self.取坐标y = self.getPositionY   
	self.取宽高 = self.getSize
	self.取宽度 = self.getWidth
	self.取高度 = self.getHeight
	self.设阴影 = self.setShadow
	self.是否阴影 = self.isShadow
	self.显示 = self.render
	
end




return Font