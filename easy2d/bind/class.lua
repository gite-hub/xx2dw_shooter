-- 实例
local function new(c, ...)
	local instance = {}
	setmetatable(instance, {
		__index = getmetatable(c).__newindex,
		__gc = function(i) if type(i.GC) == 'function' then i:GC() end end
	})
	if type(instance.init) == 'function' then instance:init(...) end
	if type(instance.汉化) == 'function' then instance:汉化() end

	return instance
end

-- 类
local function class()
    local c = {}
	setmetatable(c, {
        __newindex = {},
        -- t is c
        __call = function (t, ...) return new(t, ...) end
    })
	return c
end

-- t找不到key才会触发 mt.__index[key] / mt._index(t, key)
-- t赋值[没这个key]才会触发 __newindex
return class