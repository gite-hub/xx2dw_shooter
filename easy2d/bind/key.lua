-- 按键
local KEY = {}

KEY.MOUSE_LEFT      = 0x01
KEY.MOUSE_RIGHT     = 0x02
KEY._0 = 0x30
KEY._1 = 0x31
KEY._2 = 0x32
KEY._3 = 0x33
KEY._4 = 0x34
KEY._5 = 0x35
KEY._6 = 0x36
KEY._7 = 0x37
KEY._8 = 0x38
KEY._9 = 0x39

KEY.A = 0x41
KEY.B = 0x42
KEY.C = 0x43
KEY.D = 0x44
KEY.E = 0x45
KEY.F = 0x46
KEY.G = 0x47
KEY.H = 0x48
KEY.I = 0x49
KEY.J = 0x4A
KEY.K = 0x4B
KEY.L = 0x4C
KEY.M = 0x4D
KEY.N = 0x4E
KEY.O = 0x4F
KEY.P = 0x50
KEY.Q = 0x51
KEY.R = 0x52
KEY.S = 0x53
KEY.T = 0x54
KEY.U = 0x55
KEY.V = 0x56
KEY.W = 0x57
KEY.X = 0x58
KEY.Y = 0x59
KEY.Z = 0x5A


KEY.LBUTTON      = 0x01
KEY.RBUTTON      = 0x02
KEY.CANCEL       = 0x03
KEY.MBUTTON      = 0x04    -- /* NOT contiguous with L & RBUTTON */

-- #if(_WIN32_WINNT >= 0x0500)
KEY.XBUTTON1     = 0x05    -- /* NOT contiguous with L & RBUTTON */
KEY.XBUTTON2     = 0x06    -- /* NOT contiguous with L & RBUTTON */
-- #endif /* _WIN32_WINNT >= 0x0500 */

-- /*
--  * 0x07 : unassigned
--  */

KEY.BACK         = 0x08
KEY.TAB          = 0x09

-- /*
--  * 0x0A - 0x0B : reserved
--  */

KEY.CLEAR        = 0x0C
KEY.RETURN       = 0x0D

KEY.SHIFT        = 0x10
KEY.CONTROL      = 0x11
KEY.MENU         = 0x12
KEY.PAUSE        = 0x13
KEY.CAPITAL      = 0x14

KEY.KANA         = 0x15
KEY.HANGEUL      = 0x15  -- /* old name - should be here for compatibility */
KEY.HANGUL       = 0x15
KEY.JUNJA        = 0x17
KEY.FINAL        = 0x18
KEY.HANJA        = 0x19
KEY.KANJI        = 0x19

KEY.ESCAPE       = 0x1B

KEY.CONVERT      = 0x1C
KEY.NONCONVERT   = 0x1D
KEY.ACCEPT       = 0x1E
KEY.MODECHANGE   = 0x1F

KEY.SPACE        = 0x20
KEY.PRIOR        = 0x21
KEY.NEXT         = 0x22
KEY.END          = 0x23
KEY.HOME         = 0x24
KEY.LEFT         = 0x25
KEY.UP           = 0x26
KEY.RIGHT        = 0x27
KEY.DOWN         = 0x28
KEY.SELECT       = 0x29
KEY.PRINT        = 0x2A
KEY.EXECUTE      = 0x2B
KEY.SNAPSHOT     = 0x2C
KEY.INSERT       = 0x2D
KEY.DELETE       = 0x2E
KEY.HELP         = 0x2F

-- /*
--  * VK_0 - VK_9 are the same as ASCII '0' - '9' (0x30 - 0x39)
--  * 0x40 : unassigned
--  * VK_A - VK_Z are the same as ASCII 'A' - 'Z' (0x41 - 0x5A)
--  */

KEY.LWIN         = 0x5B
KEY.RWIN         = 0x5C
KEY.APPS         = 0x5D

-- /*
--  * 0x5E : reserved
--  */

KEY.SLEEP        = 0x5F

KEY.NUMPAD0      = 0x60
KEY.NUMPAD1      = 0x61
KEY.NUMPAD2      = 0x62
KEY.NUMPAD3      = 0x63
KEY.NUMPAD4      = 0x64
KEY.NUMPAD5      = 0x65
KEY.NUMPAD6      = 0x66
KEY.NUMPAD7      = 0x67
KEY.NUMPAD8      = 0x68
KEY.NUMPAD9      = 0x69
KEY.MULTIPLY     = 0x6A
KEY.ADD          = 0x6B
KEY.SEPARATOR    = 0x6C
KEY.SUBTRACT     = 0x6D
KEY.DECIMAL      = 0x6E
KEY.DIVIDE       = 0x6F
KEY.F1           = 0x70
KEY.F2           = 0x71
KEY.F3           = 0x72
KEY.F4           = 0x73
KEY.F5           = 0x74
KEY.F6           = 0x75
KEY.F7           = 0x76
KEY.F8           = 0x77
KEY.F9           = 0x78
KEY.F10          = 0x79
KEY.F11          = 0x7A
KEY.F12          = 0x7B
KEY.F13          = 0x7C
KEY.F14          = 0x7D
KEY.F15          = 0x7E
KEY.F16          = 0x7F
KEY.F17          = 0x80
KEY.F18          = 0x81
KEY.F19          = 0x82
KEY.F20          = 0x83
KEY.F21          = 0x84
KEY.F22          = 0x85
KEY.F23          = 0x86
KEY.F24          = 0x87

-- /*
--  * 0x88 - 0x8F : unassigned
--  */

KEY.NUMLOCK      = 0x90
KEY.SCROLL       = 0x91

-- /*
--  * NEC PC-9800 kbd definitions
--  */
KEY.OEM_NEC_EQUAL= 0x92   -- // '=' key on numpad

-- /*
--  * Fujitsu/OASYS kbd definitions
--  */
KEY.OEM_FJ_JISHO = 0x92   -- // 'Dictionary' key
KEY.OEM_FJ_MASSHOU=0x93   -- // 'Unregister word' key
KEY.OEM_FJ_TOUROKU=0x94   -- // 'Register word' key
KEY.OEM_FJ_LOYA  = 0x95   -- // 'Left OYAYUBI' key
KEY.OEM_FJ_ROYA  = 0x96   -- // 'Right OYAYUBI' key

-- /*
--  * 0x97 - 0x9F : unassigned
--  */

-- /*
--  * VK_L* & VK_R* - left and right Alt, Ctrl and Shift virtual keys.
--  * Used only as parameters to GetAsyncKeyState() and GetKeyState().
--  * No other API or message will distinguish left and right keys in this way.
--  */
KEY.LSHIFT       = 0xA0
KEY.RSHIFT       = 0xA1
KEY.LCONTROL     = 0xA2
KEY.RCONTROL     = 0xA3
KEY.LMENU        = 0xA4
KEY.RMENU        = 0xA5

-- #if(_WIN32_WINNT >= 0x0500)
KEY.BROWSER_BACK      = 0xA6
KEY.BROWSER_FORWARD   = 0xA7
KEY.BROWSER_REFRESH   = 0xA8
KEY.BROWSER_STOP      = 0xA9
KEY.BROWSER_SEARCH    = 0xAA
KEY.BROWSER_FAVORITES = 0xAB
KEY.BROWSER_HOME      = 0xAC

KEY.VOLUME_MUTE       = 0xAD
KEY.VOLUME_DOWN       = 0xAE
KEY.VOLUME_UP         = 0xAF
KEY.MEDIA_NEXT_TRACK  = 0xB0
KEY.MEDIA_PREV_TRACK  = 0xB1
KEY.MEDIA_STOP        = 0xB2
KEY.MEDIA_PLAY_PAUSE  = 0xB3
KEY.LAUNCH_MAIL       = 0xB4
KEY.LAUNCH_MEDIA_SELECT=0xB5
KEY.LAUNCH_APP1       = 0xB6
KEY.LAUNCH_APP2       = 0xB7

-- #endif /* _WIN32_WINNT >= 0x0500 */

-- /*
--  * 0xB8 - 0xB9 : reserved
--  */

KEY.OEM_1        = 0xBA   -- // ';:' for US
KEY.OEM_PLUS     = 0xBB   -- // '+' any country
KEY.OEM_COMMA    = 0xBC   -- // ',' any country
KEY.OEM_MINUS    = 0xBD   -- // '-' any country
KEY.OEM_PERIOD   = 0xBE   -- // '.' any country
KEY.OEM_2        = 0xBF   -- // '/?' for US
KEY.OEM_3        = 0xC0   -- // '`~' for US

-- /*
--  * 0xC1 - 0xD7 : reserved
--  */
-- 
-- /*
--  * 0xD8 - 0xDA : unassigned
--  */

KEY.OEM_4        = 0xDB  -- //  '[{' for US
KEY.OEM_5        = 0xDC  -- //  '\|' for US
KEY.OEM_6        = 0xDD  -- //  ']}' for US
KEY.OEM_7        = 0xDE  -- //  ''"' for US
KEY.OEM_8        = 0xDF

-- /*
--  * 0xE0 : reserved
--  */
-- 
-- /*
--  * Various extended or enhanced keyboards
--  */
KEY.OEM_AX       = 0xE1  -- //  'AX' key on Japanese AX kbd
KEY.OEM_102      = 0xE2  -- //  "<>" or "\|" on RT 102-key kbd.
KEY.ICO_HELP     = 0xE3  -- //  Help key on ICO
KEY.ICO_00       = 0xE4  -- //  00 key on ICO

-- #if(WINVER >= 0x0400)
KEY.PROCESSKEY   = 0xE5
-- #endif /* WINVER >= 0x0400 */

KEY.ICO_CLEAR    = 0xE6


-- #if(_WIN32_WINNT >= 0x0500)
KEY.PACKET       = 0xE7
-- #endif /* _WIN32_WINNT >= 0x0500 */

-- /*
--  * 0xE8 : unassigned
--  */
-- 
-- /*
--  * Nokia/Ericsson definitions
--  */
KEY.OEM_RESET    = 0xE9
KEY.OEM_JUMP     = 0xEA
KEY.OEM_PA1      = 0xEB
KEY.OEM_PA2      = 0xEC
KEY.OEM_PA3      = 0xED
KEY.OEM_WSCTRL   = 0xEE
KEY.OEM_CUSEL    = 0xEF
KEY.OEM_ATTN     = 0xF0
KEY.OEM_FINISH   = 0xF1
KEY.OEM_COPY     = 0xF2
KEY.OEM_AUTO     = 0xF3
KEY.OEM_ENLW     = 0xF4
KEY.OEM_BACKTAB  = 0xF5

KEY.ATTN         = 0xF6
KEY.CRSEL        = 0xF7
KEY.EXSEL        = 0xF8
KEY.EREOF        = 0xF9
KEY.PLAY         = 0xFA
KEY.ZOOM         = 0xFB
KEY.NONAME       = 0xFC
KEY.PA1          = 0xFD
KEY.OEM_CLEAR    = 0xFE

return KEY