local KEY = require('key')

-- 引擎接口
local Easy2d = {}

-- 弹框(标题,内容)
function Easy2d:msgbox(title, message)
    if not message then
        message = title
        title = 'easy2d'
    end
    if type(title) ~= 'string' or type(message) ~= 'string' then error('easy2d:msgbox param error') end
    return easy2d_msgbox(title, message)
end


-- 窗口大小(宽,高)
function Easy2d:resize(width, height)
    if type(width) ~= 'number' or type(height) ~= 'number' then error('easy2d:resize param error') end
    easy2d_resize(width, height)
end


-- 窗口标题(标题)
function Easy2d:setCaption(title)
    if type(title) ~= 'string' then error('easy2d:setCaption param error') end
    return easy2d_caption(title)
end



-- 鼠标
function Easy2d:getMouse()
    return easy2d_mousex(), easy2d_mousey()
end
function Easy2d:getMouseX()
    return easy2d_mousex()
end
function Easy2d:getMouseY()
    return easy2d_mousey()
end

-- 按住
function Easy2d:isKeyHold(key)
    if key == KEY.MOUSE_LEFT then
        return easy2d_mouse_hold(0)
    elseif key == KEY.MOUSE_RIGHT then
        return easy2d_mouse_hold(1)
    else
        if math.type(key) ~= 'integer' then error('easy2d:isKeyHold param error') end
        return easy2d_key_hold(key)
    end
end


-- 按下
function Easy2d:isKeyDown(key)
    if key == KEY.MOUSE_LEFT then
        return easy2d_mouse_down(0)
    elseif key == KEY.MOUSE_RIGHT then
        return easy2d_mouse_down(1)
    else
        if math.type(key) ~= 'integer' then error('easy2d:isKeyDown param error') end
        return easy2d_key_down(key)
    end
end

-- 弹起
function Easy2d:isKeyUp(key)
    if key == KEY.MOUSE_LEFT then
        return easy2d_mouse_up(0)
    elseif key == KEY.MOUSE_RIGHT then
        return easy2d_mouse_up(1)
    else
        if math.type(key) ~= 'integer' then error('easy2d:isKeyUp param error') end
        return easy2d_key_up(key)
    end
end





-- 播放wav音效(文件名)
function Easy2d:playWav(filename)
    if type(filename) ~= 'string' then error('easy2d:playWav param error') end
   return easy2d_wav(filename)
end



-- 汉化
Easy2d.弹框 = Easy2d.msgbox
Easy2d.设窗口 = Easy2d.resize
Easy2d.设标题 = Easy2d.setCaption

Easy2d.取鼠标 = Easy2d.getMouse
Easy2d.取鼠标x = Easy2d.getMouseX
Easy2d.取鼠标y = Easy2d.getMouseY

Easy2d.按住 = Easy2d.isKeyHold
Easy2d.按下 = Easy2d.isKeyDown
Easy2d.弹起 = Easy2d.isKeyUp

Easy2d.播放wav = Easy2d.playWav



return Easy2d