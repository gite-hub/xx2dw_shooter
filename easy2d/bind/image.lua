-- 图像类
local Image = require('class')()
local nullptr = 0

-- 创建(png图片文件)
function Image:init(filename)
    if type(filename) ~= 'string' then error('Image.filename error') end
    self._bitmap = image_create(filename)
    if self._bitmap == nullptr then error('Image.bitmap nullptr') end
	self._scale = 1
end


-- 坐标(x, y)
function Image:setPosition(x, y)
    self._x = x
    self._y = y
end

function Image:setPositionX(x)
    self._x = x
end

function Image:setPositionY(y)
    self._y = y
end

function Image:getPosition()
    return self._x, self._y
end

function Image:getPositionX()
    return self._x
end

function Image:getPositionY()
    return self._y
end


-- 宽高
function Image:getSize()
	return image_getw(self._bitmap), image_geth(self._bitmap)
end

function Image:getWidth()
	return image_getw(self._bitmap)
end

function Image:getHeight()
	return image_geth(self._bitmap)
end


-- 缩放(number)
function Image:setScale(scale)
	self._scale = scale
end

function Image:getScale()
    return self._scale
end



--逆时针/顺时针90度 （true/false）
function Image:clockwise(not_anti)
    if type(not_anti) ~= 'boolean' then error('Image:clockwise param error') end
	image_clockwise(self._bitmap, not_anti and true or false)
end


-- 水平/垂直 翻转 (true/false)
function Image:setFlip(horizontal)
    if type(horizontal) ~= 'boolean' then error('Image:setFlip param error') end
	image_flip(self._bitmap, horizontal)
end


-- 渲染
function Image:render()
    if type(self._x) ~= 'number' then error('Image.x error') end
    if type(self._y) ~= 'number' then error('Image.y error') end
    if type(self._scale) ~= 'number' then error('Image.scale error') end
	image_render(self._bitmap, self._x, self._y, self._scale)
end


function Image:GC()
    if math.type(self._bitmap) == 'integer' and self._bitmap ~= nullptr then
        image_release(self._bitmap)
        self._bitmap = nullptr
    end
end



function Image:汉化()

    self.设坐标 = self.setPosition
    self.设坐标x = self.setPositionX
    self.设坐标y = self.setPositionY
    self.取坐标 = self.getPosition
    self.取坐标x = self.getPositionX
    self.取坐标y = self.getPositionY
	
	self.取宽高 = self.getSize
	self.取宽度 = self.getWidth
    self.取高度 = self.getHeight
	
	--self.设锚点 = self.setAnchor
    --self.取锚点 = self.getAnchor
	--self.取锚点x = self.getAnchorX
	--self.取锚点y = self.getAnchorY
	
    self.设缩放 = self.setScale
    self.取缩放 = self.getScale
	--self.取水平缩放 = self.getScaleX
	--self.取垂直缩放 = self.getScaleY

    --self.设角度 = self.setRotation
    --self.取角度 = self.getRotation

	--self.设颜色 = self.setColor
	--self.取颜色 = self.getColor

	self.旋转 = self.clockwise
	self.翻转 = self.setFlip
	
    self.显示 = self.render
	
end



return Image