__easy.setRequirePaths('bind/')

local Easy = require('easy')
local Image = require('image')
local KEY = require('key')

local width = 640
local height = 480
Easy:resize(width, height)
Easy:setCaption('W S A D ↑ ↓ ← →')


local image_food = Image('snake/food.png')
local w = image_food:getWidth()
local h = image_food:getHeight()
local x = width / 2 - w / 2
local y = height / 2 - h / 2
-- 居中
image_food:setPosition(x, y)


local speed = 1
local vector_left  = { x = -speed, y = 0 }
local vector_right = { x = speed, y = 0 }
local vector_up    = { x = 0, y = -speed }
local vector_down  = { x = 0, y = speed }


local map_table = {
    [KEY.A] = vector_left,
    [KEY.D] = vector_right,
    [KEY.W] = vector_up,
    [KEY.S] = vector_down
}
 map_table[KEY.UP] = map_table[KEY.W]
 map_table[KEY.DOWN] = map_table[KEY.S]
 map_table[KEY.LEFT] = map_table[KEY.A]
 map_table[KEY.RIGHT] = map_table[KEY.D]


local moved
function mainLoop(dt)

    moved = false
    for key, vector in pairs(map_table) do
        if Easy:isKeyHold(key) then
            x = x + vector.x
            y = y + vector.y
            moved = true
        end
    end

    image_food:setPosition(x, y)

    image_food:render()

end