__easy.setRequirePaths('bind/')

local 引擎 = require('easy')
local 图像类 = require('image')
local 文字类 = require('font')
local KEY = require('key')

引擎:设标题('作业：贪吃蛇游戏')

local 描述 = 文字类(16)
描述:设颜色(0xFFFFFF00) --黄色
描述:设坐标(50, 50)

local 身体图 = 图像类('snake/body.png')

local 新头图 = function() return 图像类('snake/head.png') end

local 上头图 = 新头图()
上头图:设缩放(1)

local 下头图 = 新头图()
下头图:翻转(false)

local 左头图 = 新头图()
左头图:旋转(false)

local 右头图 = 新头图()
右头图:旋转(true)

local 食物图 = 图像类('snake/food.png')

local 图组 = { 身体图, 上头图, 下头图, 左头图, 右头图, 食物图 }

-- 非笛卡尔坐标系
-- 原点0,0在左上角
for k, v in pairs (图组) do
	v:设坐标(k * 50, 100)
end


function mainLoop(dt)

	描述:设文字(table.concat({ '帧数 ', string.format('%2.1f', 1 / dt), '    鼠标 ', 引擎:取鼠标x(), ',', 引擎:取鼠标y() }))
	
	描述:显示()
	for k, v in pairs(图组) do
		v:显示()
	end

	if 引擎:按下(KEY.MOUSE_LEFT) then
		引擎:播放wav('snake/click.wav')
	elseif 引擎:弹起(KEY.MOUSE_RIGHT) then
		描述:设颜色(0xFFFF0000) --红色
	end

end