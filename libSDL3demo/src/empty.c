﻿#include "SDL3/SDL.h"
#include "stdbool.h"

#if defined(WIN32) || defined(_WIN32)
#define CC_PLATFORM_WINDOWS
#include <windows.h>
#pragma comment(lib, "Winmm")
#pragma comment(lib, "Setupapi")
#pragma comment(lib, "Version")
#pragma comment(lib, "Imm32")
#pragma comment(lib, "SDL3-static")
#elif defined(ANDROID) || defined(__ANDROID__)
#define CC_PLATFORM_ANDROID
#elif defined(linux) || defined(__linux) || defined(__linux__)
#define CC_PLATFORM_LINUX
#else
#define CC_PLATFORM_UNKNOWN
#endif

#ifdef CC_PLATFORM_WINDOWS
int WINAPI WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nShowCmd) {
#else
int main(int argc, char* args[]) {
#endif
	if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_EVENTS | SDL_INIT_TIMER) < 0) {
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "", "引擎初始化失败", NULL);
		return 0;
	}

#ifdef CC_PLATFORM_WINDOWS
	SDL_Window* window = SDL_CreateWindow("SDL3demo", 1280, 800, SDL_WINDOW_HIGH_PIXEL_DENSITY);
#else
	SDL_Window* window = SDL_CreateWindow("", 0, 0, SDL_WINDOW_FULLSCREEN);
#endif
	if (!window) {
		return 0;
	}

	// g_hWnd = GetActiveWindow();
	SDL_Renderer* g_prender = SDL_CreateRenderer(window, NULL);
	int height, g_width, g_height;
	SDL_GetCurrentRenderOutputSize(g_prender, &g_width, &height);
	// SDL_SetRenderDrawColor(screen_render, 0x11, 0x11, 0x11, 0x00);
	const int kSolution = 477;
	g_height = kSolution;
	bool g_linear = true;
	for (int i = 1, h;; ++i) {
		h = height / i;
		if (h > 540) {
			continue;
		}
		if (h < kSolution) {
			break;
		}
		g_height = h;
		g_linear = false;
	}
	g_width = g_width * g_height / height;
	SDL_SetRenderLogicalPresentation (g_prender, g_width, g_height, 
		g_linear ? SDL_LOGICAL_PRESENTATION_STRETCH : SDL_LOGICAL_PRESENTATION_INTEGER_SCALE,
		g_linear ? SDL_SCALEMODE_BEST : SDL_SCALEMODE_NEAREST);
	// SDL_RenderSetIntegerScale(g_prender, SDL_TRUE);
	// auto surface480 = SDL_CreateRGBSurfaceWithFormat(0, g_width, 480, 32, SDL_PIXELFORMAT_RGBA8888);
	// auto texture480 = SDL_CreateTexture(screen_render, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, g640, k480);

	SDL_Rect rect480 = { 0, 0, g_width, g_height };
	SDL_Rect rect240 = { 0, 0, g_width / 2, g_height / 2 };

	// The surface contained by the window
	SDL_Surface* screen_surface = SDL_GetWindowSurface(window);
	SDL_Texture* screen_texture = SDL_CreateTextureFromSurface(g_prender, screen_surface);
	SDL_DestroySurface(screen_surface);
	// auto screen_texture = SDL_CreateTexture(screen_render, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, rect480.w, rect480.h);


	// auto ui_texture = SDL_CreateTexture(screen_render, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, rect240.w * 2, rect240.h * 2);
// 	auto ui_texture = SDL_CreateTextureFromSurface(g_prender, screen_surface);
// 	SDL_SetTextureBlendMode(ui_texture, SDL_BLENDMODE_BLEND);
// 
// 	RGB32* datas = new RGB32[rect480.w * rect480.h];
// 	memset(datas, 0, rect480.w * rect480.h * sizeof(RGB32));
// 
	SDL_Surface* bmp_surface = SDL_LoadBMP("xyq.bmp");
	SDL_Texture* bmp_texture = SDL_CreateTextureFromSurface(g_prender, bmp_surface);
	SDL_DestroySurface(bmp_surface);

	SDL_SetRenderTarget(g_prender, screen_texture);

    // SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "", "准备主循环", NULL);

	SDL_Event e;
	Uint64 tick = SDL_GetPerformanceCounter();
	Uint64 curr_tick, delta_tick;
	Uint64 freq = SDL_GetPerformanceFrequency();
	float ifreq = 1.0f / freq;
	int msec = freq / 618;
	freq /= 60;
	bool g_running = true;
	float g_dt;
	while (g_running) {
		curr_tick = SDL_GetPerformanceCounter();
		delta_tick = (curr_tick - tick);
		if (delta_tick < freq) {
			SDL_Delay((freq - delta_tick) / msec);
			continue;
		}
		tick = curr_tick;
		g_dt = (delta_tick % freq + freq) * ifreq;

		SDL_PollEvent(&e);
#if 0
		if (!g_input.acquire(&e)) {
			switch (e.type) {
			case SDL_EVENT_QUIT:
				break;
			}
		}
		scheduler->update(g_dt);
#endif
#ifdef CC_PLATFORM_WINDOWS
		// SDL_SetWindowTitle(window, std::to_string(g_dt).c_str());
#endif
		SDL_FRect r1 = { 0, 0, 312, 92 }, r2 = { 40, 20, 312, 92 };
		// SDL_RenderCopy(screen_render, bmp_texture, &r1, &r2);

		SDL_SetRenderTarget(g_prender, screen_texture);
		SDL_RenderClear(g_prender);

		SDL_RenderTexture(g_prender, bmp_texture, &r1, &r2);

		SDL_RenderPresent(g_prender);
	}
	// Mix_CloseAudio();
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}