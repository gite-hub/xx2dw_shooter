持续更新...

# engine/game

[SDL3demo(静态库裸奔)](libSDL3demo)

[easy2d(贪吃蛇)](easy2d)

[cc2djit(luajit绑定)](libcc2djit)

[cc2d(直读、双平台、GLSL)](libcc2d)



# tools

[大游戏/文件打包(x86)](lib3Apacker)

[lua翻译成C](liblua2c)

[udcim(相册显示/隐藏学习资料)](libudcim)

# 杂项

