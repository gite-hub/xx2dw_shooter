#include <stdlib.h>

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

int main(int argc, char* argv[]) {

	lua_State* L = NULL;
	L = luaL_newstate();
	luaL_openlibs(L);
	if (luaL_loadfile(L, "lua2c.lua") || lua_pcall(L, 0, 0, 0)) {
		printf("Error: %s\n", lua_tostring(L, -1)); 
		lua_pop(L, 1);
	}
	lua_close(L);
	for (;;);
	return 0;
}
