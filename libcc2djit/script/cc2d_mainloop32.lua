local ffi = require("ffi")

ffi.cdef([[
typedef __int64 LONGLONG;
typedef union _LARGE_INTEGER {
    struct {
        DWORD LowPart;
        LONG HighPart;
    } DUMMYSTRUCTNAME;
    struct {
        DWORD LowPart;
        LONG HighPart;
    } u;
    LONGLONG QuadPart;
} LARGE_INTEGER;

BOOL __stdcall QueryPerformanceCounter(LARGE_INTEGER* lpPerformanceCount);
BOOL __stdcall QueryPerformanceFrequency(LARGE_INTEGER* lpFrequency);
]])

ffi.cdef([[
typedef struct tagMSG {
    HWND        hwnd;
    UINT        message;
    WPARAM      wParam;
    LPARAM      lParam;
    DWORD       time;
    POINT       pt;
// #ifdef _MAC
//     DWORD       lPrivate;
// #endif
} MSG/*, *PMSG, NEAR *NPMSG, FAR *LPMSG*/;
]])

local setMainLoopFunc = function(func) do
	local lib = ffi.C
	local plargei = ffi.new("LARGE_INTEGER[1]");
	lib.QueryPerformanceFrequency(plargei[0])
	local freq = large.QuadPart;
	local ifreq = 1 / freq;
	lib.QueryPerformanceCounter(plargei[0])
	local counter = large.QuadPart, temp;
end





	// MSG msg;
	// ZeroMemory(&msg, sizeof(MSG));
	while (!glfwWindowShouldClose(hwindow)) {
#if 0
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				break;
			}
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			continue;
		}
#endif
		QueryPerformanceCounter(&large);
		temp = large.QuadPart - counter;
		if (temp >= freq / g_FPS) {
			counter = large.QuadPart;
			glfwPollEvents();
			glClear(GL_COLOR_BUFFER_BIT);
			cc2d_mainLoop(temp * ifreq);
			glfwSwapBuffers(hwindow);
		} else {
			Sleep(temp * 1000 / freq);
		}
	}
#endif

--print(plarge_integer[0].QuadPart)



ffi.cdef([[
long long QueryPerformanceFrequencyQuadPart();
long long QueryPerformanceCounterQuadPart();
void QueryPerformanceSleep(long long);

bool createWindow32(int width, int height, const char* caption);
void glfwPollEvents32();
void glfwSwapBuffers32();

]])



local lib = ffi.C;
local FPS = 60
local freq = lib.QueryPerformanceFrequencyQuadPart()
local freqFPS = tonumber(freq / FPS)
local counter = lib.QueryPerformanceCounterQuadPart()
local counter_now
local temp

lib.createWindow32(960, 600, "Open Graphics Library ����ʽͼ�ο�")

print(_CC.width, _CC.height, _CC.android)
print(package.path)



while true do
    counter_now = lib.QueryPerformanceCounterQuadPart()
    temp = counter_now - counter
    if temp < freqFPS then
        lib.QueryPerformanceSleep(freqFPS - temp); 
    else
        counter = counter_now
        lib.glfwPollEvents32()
        lib.glfwSwapBuffers32()
    end

end