local ffi = require("ffi")

require("khrplatform")
-- require("EGL")
-- require("GLES3")

ffi.cdef([[

long long QueryPerformanceFrequencyQuadPart();
long long QueryPerformanceCounterQuadPart();
void QueryPerformanceSleep(long long);

bool createWindow32(int width, int height, const char* caption);
void glfwPollEvents32();
void glfwSwapBuffers32();

void setFPS(int);
]])



local lib = ffi.C;
local FPS = 60

print(_CC.width, _CC.height, _CC.android)

if true then
    lib.setFPS(FPS)
    function mainloop(dt)
        print(dt)
    end
    registerMainLoop(mainloop)
    lib.createWindow32(960, 600, "Open Graphics Library 开放式图形库")
else
    lib.createWindow32(960, 600, "Open Graphics Library 开放式图形库")
    local freq = lib.QueryPerformanceFrequencyQuadPart()
    local freqFPS = freq / FPS
    local counter = lib.QueryPerformanceCounterQuadPart()
    local counter_now
    local temp
    while true do
        lib.glfwPollEvents32()
        counter_now = lib.QueryPerformanceCounterQuadPart()
        temp = counter_now - counter
        if temp < freqFPS then
            lib.QueryPerformanceSleep(freqFPS - temp)
        else
            --print(tonumber(temp) / tonumber(freq))
            counter = counter_now
            lib.glfwSwapBuffers32()
        end
    end
end