#include "win32.h"
#include "declspec.h"

static long long ifreq_quad_part = 1;
CC_DECLSPEC long long CC_CALL QueryPerformanceFrequencyQuadPart() {
    LARGE_INTEGER ifreq;
    QueryPerformanceFrequency(&ifreq);
    // micro微秒1000000
    return ifreq_quad_part = ifreq.QuadPart;
}

// 微秒
CC_DECLSPEC long long CC_CALL QueryPerformanceCounterQuadPart() {
    static LARGE_INTEGER itime;
    QueryPerformanceCounter(&itime);
    return itime.QuadPart;
}

// 毫秒1000
CC_DECLSPEC void CC_CALL QueryPerformanceSleep(long long quad_part) {
    Sleep(quad_part * 1000 / ifreq_quad_part);
}