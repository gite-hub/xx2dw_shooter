#ifndef _CC2D_EXTERN_C_
#define _CC2D_EXTERN_C_

#ifdef __cplusplus
#define extern_C  extern "C" {
#else
#define extern_C
#endif


#ifdef __cplusplus
#define extern_Cend  }
#else
#define extern_Cend
#endif


#endif
