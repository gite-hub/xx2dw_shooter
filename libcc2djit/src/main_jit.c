#include "main_jit.h"
#include "platform.h"

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

static lua_State* lua_state = nullptr;
static int lua_mainloop_func_ref = LUA_NOREF;

static lua_State* openJit() {
    lua_State* L = luaL_newstate();
    if (L != nullptr) {
        /* Stop collector during library initialization. */
        lua_gc(L, LUA_GCSTOP, 0);
        luaL_openlibs(L);
        lua_gc(L, LUA_GCRESTART, -1);
    }
    return L;
}

static void setGlobal(lua_State* L, const char* table_name, int width, int height) {
	lua_newtable(L);
	lua_setglobal(L, table_name);
	// 将 CC 放到栈顶
	lua_getglobal(L, table_name);
	lua_pushinteger(L, width);
	lua_setfield(L, -2, "width");
	lua_pushinteger(L, height);
	lua_setfield(L, -2, "height");
#if CC_WIN32
	lua_pushstring(L, "win32");
	lua_setfield(L, -2, "win32");
// #if defined(_WIN64)
// 	lua_pushstring(L, "win64");
// 	lua_setfield(L, -2, "win64");
// #endif
#elif CC_ANDROID
    lua_pushstring(L, "android");
	lua_setfield(L, -2, "android");
#endif
	// 移除栈顶的 CC
	lua_pop(L, 1);
}

static void registerMainLoop(lua_State* L) {
	if (lua_isfunction(L, -1)) {
		lua_mainloop_func_ref = luaL_ref(L, LUA_REGISTRYINDEX);
	}
}



static int traceback(lua_State* L) {
	if (!lua_isstring(L, 1)) { /* Non-string error object? Try metamethod. */
		if (lua_isnoneornil(L, 1) ||
			!luaL_callmeta(L, 1, "__tostring") ||
			!lua_isstring(L, -1))
			return 1;  /* Return non-string error object. */
		lua_remove(L, 1);  /* Replace object by result of __tostring metamethod. */
	}
	luaL_traceback(L, L, lua_tostring(L, 1), 1);
	return 1;
}

static int docall(lua_State* L, int narg, int nret) {
	int base = lua_gettop(L) - narg;  /* function index */
	lua_pushcfunction(L, traceback);  /* push traceback function */
	lua_insert(L, base);  /* put it under chunk and args */
#if 0
	signal(SIGINT, laction);
#endif
	int ret = lua_pcall(L, narg, nret/*(clear ? 0 : LUA_MULTRET)*/, base);
#if 0
	signal(SIGINT, SIG_DFL);
#endif
	lua_remove(L, base);  /* remove traceback function */
	/* force a complete garbage collection in case of errors */
	if (ret != LUA_OK) {
		lua_gc(L, LUA_GCCOLLECT, 0);
	}
	return ret;
}

static void report(lua_State* L, const char* module) {
	fputs("module fail: ", stderr);
	fputs(module, stderr);
	fputc('\n', stderr);
	if (!lua_isnil(L, -1)) {
		const char* msg = lua_tostring(L, -1);
		if (msg != NULL) {
			fputs(msg, stderr);
			fputc('\n', stderr);
		}
		fflush(stderr);
		lua_pop(L, 1);
	} else {
		fflush(stderr);
	}
}


bool doScript(lua_State* L, const char* model, const char* content) {

	int ret = luaL_loadstring(L, content);
	if (ret != LUA_OK) {
		fputs("loadstring fail: ", stderr);
		fputs(model, stderr);
		fputc('\n', stderr);
		fflush(stderr);
		return false;
	}
	ret = docall(L, 0, 0);
	if (ret != LUA_OK) {
		report(L, model);

		lua_close(L);
		return false;
	}
	return true;
}


static void addPaths(lua_State* L, const char* path) {
	lua_getglobal(L, "package");
	lua_getfield(L, -1, "path");

	const char* old_paths = lua_tostring(L, -1);
	int size = strlen(old_paths) + strlen(path);
	const char* new_paths = (char*)malloc(size + 8);
	memset(new_paths + size, 0, 8);
	sprintf(new_paths, "%s%s", old_paths, path);

	lua_pushstring(L, new_paths);
	lua_setfield(L, -3, "path");

	free(new_paths);
	// 弹出 package 和 path
	lua_pop(L, 2);
}


bool cc2d_mainJit(int width, int height) {
    lua_State* L = openJit();
    if (L == nullptr) {
        return false;
    }
    setGlobal(L, "_CC", width, height);
    lua_register(L, "registerMainLoop", registerMainLoop);
    addPaths(L, "./script/?.lua;");
	// doScript(L, "addPaths", "package.path = package.path .. './script/?.lua;'");
    lua_state = L;
    return true;
}




bool cc2d_mainScript(const char* filename) {
    lua_State* L = lua_state;
    int ret = luaL_loadfile(L, filename);
    if (ret != LUA_OK) {
		report(L, filename);

        lua_close(L);
        return false;
    }
    ret = docall(L, 0, 0);
    if (ret != LUA_OK) {
        report(L, filename);

        lua_close(L);
        return false;
    }
    return true;
}


static void mainLoop(lua_State* L, float dt) {
	lua_rawgeti(L, LUA_REGISTRYINDEX, lua_mainloop_func_ref);
	lua_pushnumber(L, dt);
	int ret = docall(L, 1, 0);
	if (ret != LUA_OK) {
        static bool once = true;
        if (once) {
            once = false;
            report(L, "docall_mainLoop");
        }
	}
}


void cc2d_mainLoop(float dt) {
	mainLoop(lua_state, dt);
}