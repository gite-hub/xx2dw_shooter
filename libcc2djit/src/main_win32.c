#include "win32.h"
#include <stdio.h>
#include "main.h"
#include "main_jit.h"
#include "declspec.h"


// #include "GLES2/gl2.h"
// #include "EGL/egl.h"
// 
// 
// #pragma comment(lib, "3rd/EGL/libEGL.dll.lib")
// #pragma comment(lib, "3rd/GLES2/libGLESv2.dll.lib")
// #pragma comment(lib, "3rd/bass/win32/bass")
// #pragma comment(lib, "3rd/turbojpeg/turbojpeg-static")

#pragma comment(lib, "luajit/lua-static")

static int g_width, g_height;
// static GLFWwindow* glfwindow = nullptr;
static HINSTANCE hinstance = nullptr;
static HWND hwnd = nullptr;



// static EGLDisplay display = nullptr;
// static EGLSurface surface = nullptr;
// static EGLContext context = nullptr;

#if 0
static GLFWwindow* createWindow(int width, int height, const char* caption) {
	if (!glfwInit()) {
		return nullptr;
	}
	// gettimeofday()
	glfwDefaultWindowHints();
	glfwWindowHint(GLFW_DEPTH_BITS, 0);
	glfwWindowHint(GLFW_STENCIL_BITS, 0);

	GLFWwindow* hwindow = glfwCreateWindow(width, height, caption, nullptr, nullptr);
	if (!hwindow) {
		return nullptr;
	}
	glfwMakeContextCurrent(hwindow);
	// GLFW_LOCK_KEY_MODS
	glfwSetInputMode(hwindow, GLFW_CURSOR_NORMAL, GLFW_TRUE);
	// 	while (auto e = glGetError()) {
	// 		CC_LOG_ERROR("glGetError() == %d", (int)e);
	// 	};
	if (!gladLoadGL((GLADloadfunc)glfwGetProcAddress)) {
		glfwDestroyWindow(hwindow);
		return nullptr;
	}

	// no v-sync by default
	glfwSwapInterval(0);

	glEnable(GL_PRIMITIVE_RESTART);
	glPrimitiveRestartIndex(65535);
	glPointSize(5);
	glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

	return hwindow;
}
#endif

static void CreateConsoleAndRedirectIO() {
	if (!AttachConsole(ATTACH_PARENT_PROCESS)) {
		AllocConsole();
	}

	// 重定向标准输出、标准输入和标准错误到控制台
	FILE* fp;

	freopen_s(&fp, "CONOUT$", "w", stdout);
	setvbuf(stdout, NULL, _IONBF, 0);

	freopen_s(&fp, "CONIN$", "r", stdin);
	setvbuf(stdin, NULL, _IONBF, 0);

	freopen_s(&fp, "CONOUT$", "w", stderr);
	setvbuf(stderr, NULL, _IONBF, 0);

	// 设置控制台模式为原始输入模式，以避免处理输入时的特殊行为
	HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
	SetConsoleMode(hStdin, ENABLE_PROCESSED_INPUT | ENABLE_LINE_INPUT | ENABLE_ECHO_INPUT);

	// 确保控制台窗口的光标处于正确位置
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	if (GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi)) {
		COORD cursorPos = csbi.dwCursorPosition;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), cursorPos);
	}

	SetConsoleOutputCP(CP_UTF8);
	SetConsoleCP(CP_UTF8);
}


int WINAPI WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nShowCmd) {
	hinstance = hInstance;
	g_width = GetSystemMetrics(SM_CXSCREEN);
	g_height = GetSystemMetrics(SM_CYSCREEN);

	CreateConsoleAndRedirectIO();

	if (!cc2d_mainJit(g_width, g_height) || !cc2d_mainScript("script/cc2d_win32.lua")) {
		_getch();
		return EXIT_FAILURE;
	}

	LARGE_INTEGER large;
	QueryPerformanceFrequency(&large);
	LONGLONG freq = large.QuadPart;
	int freqFPS = freq / g_FPS;
	double ifreq = 1.0 / freq;

	QueryPerformanceCounter(&large);
	LONGLONG counter = large.QuadPart, temp;

	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));
	while (true) {
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				return EXIT_SUCCESS;
			}
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			continue;
		}
		QueryPerformanceCounter(&large);
		temp = large.QuadPart - counter;
		if (temp < freqFPS) {
			Sleep((freqFPS - temp) * 1000 / freq);
			continue;
		}
		counter = large.QuadPart;
		// glfwPollEvents();
		// glClear(GL_COLOR_BUFFER_BIT);
		cc2d_mainLoop(temp * ifreq);
		// eglSwapBuffers(display, surface);
	}
	
	// glfwDestroyWindow(glfwindow);
	// eglDestroyContext(display, context);
	// eglDestroySurface(display, surface);
	// eglTerminate(display);
	DestroyWindow(hwnd);
	return EXIT_SUCCESS;
}

static void resizeWindow(HWND hwnd, int width, int height) {

	RECT wind_rect, client_rect;
	GetWindowRect(hwnd, &wind_rect);
	GetClientRect(hwnd, &client_rect);

	width = (wind_rect.right - (client_rect.right - width)) - wind_rect.left;
	height = (wind_rect.bottom - (client_rect.bottom - height)) - wind_rect.top;

	MoveWindow(hwnd, wind_rect.left, wind_rect.top, width, height, true);
}

CC_DECLSPEC bool CC_CALL createWindow32(int width, int height, const char* caption) {

	WNDCLASSEX wclass;
	wclass.style = CS_DBLCLKS | CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	wclass.cbClsExtra = 0;
	wclass.cbSize = sizeof(wclass);
	wclass.cbWndExtra = 0;
	wclass.hbrBackground = 0;
	wclass.hCursor = LoadCursor(0, IDC_ARROW);
	wclass.hIcon = LoadIcon(hinstance, MAKEINTRESOURCE(101/*IDI_ICON1*/));
	wclass.hIconSm = wclass.hIcon;
	wclass.lpszClassName = L"cc2d";
	wclass.hInstance = hinstance;
	wclass.lpfnWndProc = DefWindowProc;
	wclass.lpszMenuName = 0;
	wclass.style = CS_CLASSDC;
	//注册窗口
	if (!RegisterClassEx(&wclass)) {
		return false;
	}

	long style = WS_BORDER | WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_VISIBLE;
	hwnd = CreateWindow(wclass.lpszClassName, L"标题", style,
		(g_width - width) / 2, (g_height - height) / 2, width, height,
		nullptr, nullptr, hinstance, nullptr);

	if (hwnd == nullptr) {
		return false;
	}

	resizeWindow(hwnd, width, height);
	ShowWindow(hwnd, SW_SHOW);
	UpdateWindow(hwnd);
	CoInitialize(nullptr);

#if 0
	// 2. 初始化 EGL
	display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
	eglInitialize(display, NULL, NULL);

	EGLint configAttributes[] = {
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_ALPHA_SIZE, 8,
		EGL_DEPTH_SIZE, 8,
		EGL_STENCIL_SIZE, 8,
		EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
		EGL_NONE
	};

	EGLConfig config;
	EGLint numConfigs;
	eglChooseConfig(display, configAttributes, &config, 1, &numConfigs);

	surface = eglCreateWindowSurface(display, config, hwnd, NULL);

	EGLint contextAttributes[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};

	context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttributes);

	eglMakeCurrent(display, surface, surface, context);

#else
// 	GLFWwindow* hwindow = createWindow(width, height, caption);
// 	if (hwindow == nullptr) {
// 		return false;
// 	}
// 	glfwindow = hwindow;
#endif
	return true;
}

static MSG msg;
CC_DECLSPEC void CC_CALL glfwPollEvents32() {
	// glfwPollEvents();
	while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
		if (msg.message == WM_QUIT) {
			// running = false;
		}
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	// glClear(GL_COLOR_BUFFER_BIT);
}

CC_DECLSPEC void CC_CALL glfwSwapBuffers32() {
	// glfwSwapBuffers(glfwindow);
	// eglSwapBuffers(display, surface);
}