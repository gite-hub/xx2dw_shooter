#ifndef _CC2D_C_
#define _CC2D_C_

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include "extern_c.h"

/* Apparently this is needed by several Windows compilers */
#if !defined(__MACH__)
#ifndef NULL
#define NULL ((void *)0)
#endif
#endif /* ! Mac OS X - breaks precompiled headers */

#define nullptr NULL

extern_C
typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned int uint;

static const int k_1 = -1;
extern_Cend
#endif




