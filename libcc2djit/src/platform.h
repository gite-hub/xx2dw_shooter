#ifndef _CC2D_PLATFORM_
#define _CC2D_PLATFORM_

#if defined(_WIN32)
#define CC_WIN32 1
#define CC_ANDROID 0
#define CC_MOBILE 0
#elif defined(__ANDROID__)
#define CC_WIN32 0
#define CC_ANDROID 1
#define CC_MOBILE 1
#else
#define CC_WIN32 0
#define CC_ANDROID 0
#define CC_MOBILE 0
#endif

#endif
