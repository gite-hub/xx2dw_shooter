#ifndef _CC2D_DECLSPEC_
#define _CC2D_DECLSPEC_

#ifndef CC_DEPRECATED
#  if defined(__GNUC__) && (__GNUC__ >= 4)  /* technically, this arrived in gcc 3.1, but oh well. */
#    define CC_DEPRECATED __attribute__((deprecated))
#  elif defined(_MSC_VER)
#    define CC_DEPRECATED __declspec(deprecated)
#  else
#    define CC_DEPRECATED
#  endif
#endif

#ifndef CC_UNUSED
#  ifdef __GNUC__
#    define CC_UNUSED __attribute__((unused))
#  else
#    define CC_UNUSED
#  endif
#endif

/* Some compilers use a special export keyword */
#ifndef CC_DECLSPEC
# if defined(_WIN32) || defined(__WINRT__) || defined(__CYGWIN__) || defined(__GDK__)
#define CC_DECLSPEC __declspec(dllexport)
# elif defined(__OS2__)
#define CC_DECLSPEC __declspec(dllexport)
# else
#  if defined(__GNUC__) && __GNUC__ >= 4
#   define CC_DECLSPEC __attribute__ ((visibility("default")))
#  else
#   define CC_DECLSPEC
#  endif
# endif
#endif

/* By default SDL uses the C calling convention */
#ifndef CC_CALL
#if (defined(_WIN32) || defined(__WINRT__) || defined(__GDK__)) && !defined(__GNUC__)
#define CC_CALL __cdecl
#elif defined(__OS2__) || defined(__EMX__)
#define CC_CALL _System
# if defined (__GNUC__) && !defined(_System)
#  define _System /* for old EMX/GCC compat.  */
# endif
#else
#define CC_CALL
#endif
#endif /* SDLCALL */

/* Removed DECLSPEC on Symbian OS because SDL cannot be a DLL in EPOC */
#ifdef __SYMBIAN32__
#undef DECLSPEC
#define DECLSPEC
#endif /* __SYMBIAN32__ */

/* Force structure packing at 4 byte alignment.
   This is necessary if the header is included in code which has structure
   packing set to an alternate value, say for loading structures from disk.
   The packing is reset to the previous value in close_code.h
 */
#if defined(_MSC_VER) || defined(__MWERKS__) || defined(__BORLANDC__)
#ifdef _MSC_VER
#pragma warning(disable: 4103)
#endif
#ifdef __clang__
#pragma clang diagnostic ignored "-Wpragma-pack"
#endif
#ifdef __BORLANDC__
#pragma nopackwarning
#endif
#ifdef _WIN64
/* Use 8-byte alignment on 64-bit architectures, so pointers are aligned */
#pragma pack(push,8)
#else
#pragma pack(push,4)
#endif
#endif /* Compiler needs structure packing set */

#ifndef CC_INLINE
#if defined(__GNUC__)
#define CC_INLINE __inline__
#elif defined(_MSC_VER) || defined(__BORLANDC__) || \
      defined(__DMC__) || defined(__SC__) || \
      defined(__WATCOMC__) || defined(__LCC__) || \
      defined(__DECC) || defined(__CC_ARM)
#define CC_INLINE __inline
#ifndef __inline__
#define __inline__ __inline
#endif
#else
#define CC_INLINE inline
#ifndef __inline__
#define __inline__ inline
#endif
#endif
#endif /* CC_INLINE not defined */

#ifndef CC_FORCE_INLINE
#if defined(_MSC_VER)
#define CC_FORCE_INLINE __forceinline
#elif ( (defined(__GNUC__) && (__GNUC__ >= 4)) || defined(__clang__) )
#define CC_FORCE_INLINE __attribute__((always_inline)) static __inline__
#else
#define CC_FORCE_INLINE static CC_INLINE
#endif
#endif /* CC_FORCE_INLINE not defined */

#ifndef CC_NORETURN
#if defined(__GNUC__)
#define CC_NORETURN __attribute__((noreturn))
#elif defined(_MSC_VER)
#define CC_NORETURN __declspec(noreturn)
#else
#define CC_NORETURN
#endif
#endif /* CC_NORETURN not defined */


#endif
