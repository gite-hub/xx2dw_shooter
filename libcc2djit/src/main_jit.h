#ifndef _CC2D_MAIN_JIT_
#define _CC2D_MAIN_JIT_

#include "std_c.h"

extern_C

bool cc2d_mainJit(int width, int height);

bool cc2d_mainScript(const char* filename);

void cc2d_mainLoop(float dt);

extern_Cend
#endif
